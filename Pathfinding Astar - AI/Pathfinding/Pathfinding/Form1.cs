﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathfinding
{
    public partial class Form1 : Form
    {
        Graphics UI;
        Boolean Started=false;
        List<Point> path;
        readonly int UI_SizeX=19, UI_SizeY=19;
        int i = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            SetStyle(ControlStyles.ResizeRedraw, true);
            SetStyle(ControlStyles.UserPaint, true);
            SetStyle(ControlStyles.AllPaintingInWmPaint, true);
            SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            UI = new Graphics(this);
            UI.Generate(UI_SizeX, UI_SizeY);
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Escape)
            {
                if (Started == true)
                {
                    Started = false;
                    timer1.Stop();
                    UI.GetMatrix();
                }
                else
                    Application.Exit();
            }
            if (keyData == Keys.Enter)
            {
                SearchParameters searchParameters;
                int[,] matrix = UI.GetMatrix();
                searchParameters = new SearchParameters(Parser.GetStartLoc(matrix, UI_SizeX, UI_SizeY), Parser.GetEndLoc(matrix, UI_SizeX, UI_SizeY), Parser.GetMap(matrix, UI_SizeX, UI_SizeY));
                PathFinder pathFinder = new PathFinder(searchParameters);
                pathFinder = new PathFinder(searchParameters);
                path = pathFinder.FindPath();
                Started = true;
                timer1.Start();
            }
            if (keyData == Keys.D1)
                UI.PlaceActor(1);
            if (keyData == Keys.D2)
                UI.PlaceActor(2);
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (path.Count != 0)
            {
                Point p = path.ElementAt(i);
                int result = UI.MoveMainActor(p.X, p.Y);
                i++;
                if (i >= path.Count)
                {
                    timer1.Stop();
                    Started = false;
                    MessageBox.Show("Done");
                }
            }
            else
            {
                timer1.Stop();
                Started = false;
                MessageBox.Show("No path found");
            }
        }
    }
}
