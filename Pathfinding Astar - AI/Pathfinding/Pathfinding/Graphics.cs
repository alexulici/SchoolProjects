﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pathfinding
{
    class Graphics
    {
        class Graphical_Element
        {
            public PictureBox image;
            public int x;
            public int y;
            public int value=0;

            public void ReplaceWith(Graphical_Element g)
            {
                this.image = g.image;
                this.x = g.x;
                this.y = g.y;
                this.value = g.value;
            }
        }

        Form Graphics_Form;
        List<Graphical_Element> Graphical_elements = new List<Graphical_Element>();
        public int Matrix_SizeX, Matrix_SizeY;
        Graphical_Element Main_Actor;
        int PlaceActorStatus = -1;

        public Graphics(Form form)
        {
            this.Graphics_Form = form;
        }

        public void Generate(int x, int y)
        {
            foreach (Graphical_Element elem in Graphical_elements)
            {
                Graphics_Form.Controls.Remove(elem.image);
            }
            Matrix_SizeX = x;
            Matrix_SizeY = y;
            Graphical_elements.Clear();
            Point p = new Point(0, 0);
            for (int i = 1; i <= x; i++)
            {
                for (int j = 1; j <= y; j++)
                {
                    Graphical_Element elem = new Graphical_Element();
                    elem.x = j;
                    elem.y = i;
                    p.X = j * 50 + 100;
                    PictureBox picture = new PictureBox
                    {
                        Name = "pictureBox" + i,
                        Size = new Size(50, 50),
                        Location = p,
                        BorderStyle = BorderStyle.None,
                        SizeMode = PictureBoxSizeMode.Zoom
                    };
                    picture.Image = Pathfinding.Properties.Resources.Ground;
                    elem.image = picture;
                    elem.image.Click += new System.EventHandler(this.OnElemClick);
                    Graphical_elements.Add(elem);
                    Graphics_Form.Controls.Add(picture);
                }
                p.X = 0;
                p.Y = i * 40;
            }
        }

        public void OnElemClick(object sender, EventArgs e)
        {
            PictureBox picturebox = (PictureBox)sender;
            foreach (Graphical_Element g in Graphical_elements)
            {
                if (PlaceActorStatus == 1 && g.value == 1 || PlaceActorStatus == 2 && g.value == 2)
                {
                    g.image.Image = Pathfinding.Properties.Resources.Ground;
                    g.value = 0;
                }
            }
            foreach (Graphical_Element g in Graphical_elements)
            {
                if (picturebox.Equals(g.image))
                {
                    if (PlaceActorStatus == 1 && g.value == 0)
                    {
                        g.image.Image = Pathfinding.Properties.Resources.Actor1;
                        g.value = 1;
                        Main_Actor = g;
                        PlaceActorStatus = -1;
                        return;
                    }
                    if (PlaceActorStatus == 2 && g.value == 0)
                    {
                        g.image.Image = Pathfinding.Properties.Resources.Actor2;
                        g.value = 2;
                        PlaceActorStatus = -1;
                        return;
                    }
                    if (g.value == 0)
                    {
                        g.image.Image = Pathfinding.Properties.Resources.Block;
                        g.value = -1;
                    }
                    else if(g.value == -1)
                    {
                        g.image.Image = Pathfinding.Properties.Resources.Ground;
                        g.value = 0;
                    }
                    return;
                }
            }
        }

        internal void PlaceActor(int actor_type)
        {
            if (actor_type == 1)
                PlaceActorStatus = 1;
            else if (actor_type == 2)
                PlaceActorStatus = 2;
        }

        public int MoveMainActor(int y, int x)
        {
            if (y > Matrix_SizeY || y<0 || x > Matrix_SizeX || x<0)
                return 0;
            Graphical_Element newMainActor = Graphical_elements.ElementAt(y * Matrix_SizeY + x);
            if (newMainActor.value != -1)
            {
                Main_Actor.image.Image = Pathfinding.Properties.Resources.Ground;
                Main_Actor.value = 0;
                Main_Actor.ReplaceWith(newMainActor);
                Main_Actor.value = 1;
                Main_Actor.image.Image = Pathfinding.Properties.Resources.Actor1;
            }
            else
                return -1;
            return 1;

        }

        public int[,] GetMatrix()
        {
            int[,] matrix = new int[Matrix_SizeX,Matrix_SizeY];
            int count=0;
            for (int i = 0; i < Matrix_SizeX; i++)
            {
                for (int j = 0; j < Matrix_SizeY; j++)
                {
                    matrix[i, j] = Graphical_elements.ElementAt(count).value;
                    count++;
                }
            }
            return matrix;
        }
    }
}
