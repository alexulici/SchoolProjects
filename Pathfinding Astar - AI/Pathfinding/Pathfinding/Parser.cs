﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pathfinding
{
    static class Parser
    {

        internal static Point GetStartLoc(int[,] matrix, int x, int y)
        {
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    if (matrix[i, j] == 1)
                        return new Point(i, j);
                }
            return new Point(-1, -1);
        }

        internal static Point GetEndLoc(int[,] matrix, int x, int y)
        {
            for (int i = 0; i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    if (matrix[i, j] == 2)
                        return new Point(i, j);
                }
            return new Point(-1,-1);
        }

        internal static bool[,] GetMap(int[,] matrix,int x,int y)
        {
            bool[,] map = new bool[x,y];
            for(int i = 0;i < x; i++)
                for (int j = 0; j < y; j++)
                {
                    if (matrix[i, j] == -1)
                        map[i, j] = false;
                    if (matrix[i, j] == 0 || matrix[i, j] == 1 || matrix[i, j] == 2)
                        map[i, j] = true;
                }
            return map;
        }
    }
}
