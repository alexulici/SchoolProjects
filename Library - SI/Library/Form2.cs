﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concert
{
    public partial class Form2 : Form, IObserver<Object>
    {
        Domain.Subscriber Sub;
        int subindex;
        private IDisposable unsub1,unsub2;
        private Controller controller;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            unsub1=controller.SubscribeBook(this);
            unsub2=controller.SubscribeSubscribers(this);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = Sub.Name;
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }
        public void OnNext(Object value)
        {
            this.RefreshList();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            Domain.Book book = controller.FindBookById(controller.GetIdFromListBox(listBox1.Text));
            if (book != null)
            {
                Sub.AddBook(book);
                controller.UpdateSubscriber(Sub, subindex);
                controller.RemoveBook(book);
            }
        }

        public void RefreshList()
        {
            listBox1.Items.Clear();
            Domain.Book[] clist = new Domain.Book[controller.GetBooksize()];
            clist = controller.GetAllBooks();
            foreach (Domain.Book Book in clist)
            {
                listBox1.Items.Add(Book.ID + " " + Book.Title + " " + Book.Author);
            }

            listBox2.Items.Clear();
            for (int i = 0; i < Sub.BookCount(); i++)
                listBox2.Items.Add(Sub.GetBook(i).BookID + " " + Sub.GetBook(i).Title + " " + Sub.GetBook(i).Author);
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            unsub1.Dispose();
            unsub2.Dispose();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            int getid = controller.GetIdFromListBox(textBox1.Text);
            if (getid < 0)
                return;
            subindex = controller.FindSubscriberById(getid);
            if (subindex < 0)
                return;
            Domain.Subscriber subscriber = controller.GetSubscriberAt(subindex);
            
            if (subscriber != null)
            {
                Sub = subscriber;
                RefreshList();
                button3.Enabled = false;
                textBox1.Enabled = false;
            }
            else
                MessageBox.Show("Invalid id");
        }
    }
}
