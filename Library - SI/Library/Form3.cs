﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concert
{
    public partial class Form3 : Form
    {
        private Controller controller;
        private Domain.Subscriber Sub;
        public Form3(Controller controller)
        {
            InitializeComponent();
            // TODO: Complete member initialization
            this.controller = controller;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Sub = controller.GetSubscriberAt(controller.FindSubscriberById(Convert.ToInt32(textBox1.Text)));
            if (Sub == null)
            {
                MessageBox.Show("Subscriber not found");
                return;
            }
            Domain.Book book = Sub.FindBookById(Convert.ToInt32(textBox2.Text));
            if (book == null)
            {
                MessageBox.Show("Book not found");
                return;
            }
            Sub.RemoveBook(book);
            controller.InsertBook(book);
        }
    }
}
