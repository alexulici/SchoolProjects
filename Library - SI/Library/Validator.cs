﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    class ValidatorBook : IValidator<Domain.Book>
    {
        public bool validate(Domain.Book a)
        {
            StringBuilder Message = new StringBuilder("");
            if (a.Author == "")
                Message.Append("The book doesn't have an author");
            if (a.Title == "")
                Message.Append("The title cannot be empty");
            if (a.BookID < 0)
                Message.Append("Book id must be a positive value");
            if (Message.ToString() == "")
                return true;
            else
                throw new Exception(Message.ToString());
        }
        
    }
    class ValidatorSubscriber : IValidator<Domain.Subscriber>
    {
        public bool validate(Domain.Subscriber a)
        {
            StringBuilder Message = new StringBuilder("");
            if (a.Name == "")
                Message.Append("Subscriber name cannot be empty");
            if (a.Forename == "")
                Message.Append("Subscriber forename cannot be empty");
            if (a.Id < 0)
                Message.Append("Subscriber id cannot be negative");
            if (Message.ToString() == "")
                return true;
            else
                throw new Exception(Message.ToString());
        }
    }
}
