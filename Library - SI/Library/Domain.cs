﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    public class Domain
    {
        public class Book : IDObject , IComparable
        {
            private string title,author;
            private int bookID;

            public Book(string title, string author, int bookID):base (bookID)
            {
                this.title = title;
                this.author = author;
                this.bookID = bookID;
            }

            public string Title
            {
                get { return title; }
                set
                {
                    this.title = value;
                }
            }
            public string Author
            {
                get { return author; }
                set
                {
                    this.author = value;
                }
            }
            public int BookID
            {
                get { return bookID; }
                set
                {
                    this.bookID = value;
                }
            }
            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                Book book = obj as Book;
                if (book != null)
                    return this.bookID.CompareTo(book.bookID);
                else
                    throw new ArgumentException("Object is not a Book");
            }
        }
        public class Subscriber : IDObject , IComparable
        {
            private string name,forename;
            private List<Book> Books = new List<Book>();
            private int id;

            public Subscriber(string name,string forename,int id):base(id)
            {
                this.name = name;
                this.forename = forename;
                this.id = id;
            }
            public string Name
            {
                get { return name; }
                set
                {
                    this.name = value;
                }
            }
            public string Forename
            {
                get { return forename; }
                set
                {
                    this.forename = value;
                }
            }
            public int Id
            {
                get { return id; }
                set
                {
                    this.id = value;
                }
            }
            public Domain.Book GetBook(int position)
            {
                return Books.ElementAt(position);
            }
            public Book FindBookById(int id)
            {
                foreach (Book b in Books)
                {
                    if (b.BookID.CompareTo(id) == 0)
                        return b;
                }
                return null;
            }
            public int BookCount()
            {
                return Books.Count;
            }
            public void AddBook(Book a)
            {
                Books.Add(a);
            }
            public void RemoveBook(Book a)
            {
                Books.Remove(a);
            }
            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                Subscriber subscriber = obj as Subscriber;
                if (subscriber != null)
                    return this.name.CompareTo(subscriber.name);
                else
                    throw new ArgumentException("Object is not a Subscriber");
            }
        }
    }
}
