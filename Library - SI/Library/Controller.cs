﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    public class Controller
    {
        private Repository<Domain.Book> repositoryBooks = new Repository<Domain.Book>();
        private Repository<Domain.Subscriber> repositorySubscribers = new Repository<Domain.Subscriber>();
        System.Data.SqlClient.SqlConnection connection = new System.Data.SqlClient.SqlConnection(@"Data Source=.\SQLEXPRESS;Initial Catalog=Library;Integrated Security=True");

        public IDisposable SubscribeBook(IObserver<Domain.Book> o)
        {
            return repositoryBooks.Subscribe(o);

        }
        public IDisposable SubscribeSubscribers(IObserver<Domain.Subscriber> o)
        {
            return repositorySubscribers.Subscribe(o);

        }
        public Boolean InsertBook(Domain.Book Book)
        {
            ValidatorBook validator = new ValidatorBook();
            try
            {
                validator.validate(Book);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            repositoryBooks.InsertElement(Book);
            return true;
        }
        public Boolean InsertSubscriber(Domain.Subscriber subscriber)
        {
            ValidatorSubscriber validator = new ValidatorSubscriber();
            try
            {
                validator.validate(subscriber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            repositorySubscribers.InsertElement(subscriber);
            return true;
        }
        public Boolean UpdateSubscriber(Domain.Subscriber subscriber, int position)
        {
            ValidatorSubscriber validator = new ValidatorSubscriber();
            try
            {
                validator.validate(subscriber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            repositorySubscribers.RemoveElementAt(position);
            repositorySubscribers.InsertElementAt(subscriber,position);
            return true;
        }
        public void RemoveBook(Domain.Book Book)
        {
            repositoryBooks.RemoveElement(Book);
        }
        public void RemoveSubscriber(Domain.Subscriber subscriber)
        {
            repositorySubscribers.RemoveElement(subscriber);   
        }
        public void RemoveAllBooks()
        {
            repositoryBooks.RemoveAllElements();
        }
        public void RemoveAllSubscribers()
        {
            repositorySubscribers.RemoveAllElements();
        }
        public int GetBooksize()
        {
            return repositoryBooks.ElementLength();
        }
        public int GetSubscriberSize()
        {
            return repositorySubscribers.ElementLength();
        }
        public Domain.Book GetBookAt(int position)
        {
            return repositoryBooks.GetElement(position);
        }
        public Domain.Subscriber GetSubscriberAt(int position)
        {
            return repositorySubscribers.GetElement(position);
        }
        public Domain.Book FindBookById(int id)
        {
            Domain.Book[] list = new Domain.Book[repositoryBooks.ElementLength()];
            list = GetAllBooks();
            for (int i = 0; i < repositoryBooks.ElementLength(); i++)
            {
                if (id.Equals(list[i].BookID))
                    return list[i];
            }
            return null;
        }
        public Boolean HasBook(Domain.Subscriber a,int id)
        {
            for(int i=0 ; i<a.BookCount();i++)
            {
                if (id.Equals(a.GetBook(i).BookID))
                    return true;
            }
            return false;
        }
        public int FindSubscriberById(int id)
        {
            Domain.Subscriber[] list = new Domain.Subscriber[repositorySubscribers.ElementLength()];
            list = GetAllSubscribers();
            for (int i = 0; i < repositorySubscribers.ElementLength(); i++)
            {
                if (id.Equals(list[i].ID))
                    return i;
            }
            return -1;
        }
        public Domain.Book[] GetAllBooks()
        {
            Domain.Book[] list = new Domain.Book[repositoryBooks.ElementLength()];
            for (int i = 0; i < repositoryBooks.ElementLength(); i++)
            {
                list[i] = repositoryBooks.GetElement(i);
            }
            return list;
        }
        public Domain.Subscriber[] GetAllSubscribers()
        {
            Domain.Subscriber[] list = new Domain.Subscriber[repositorySubscribers.ElementLength()];
            for (int i = 0; i < repositorySubscribers.ElementLength(); i++)
            {
                list[i] = repositorySubscribers.GetElement(i);
            }
            return list;
        }

        public int GetIdFromListBox(string text)
        {
            try
            {
                string[] newtext = text.Split(' ');
                return Convert.ToInt32(newtext[0]);
            }
            catch
            {
                return -1;
            }
        }
        public void LoadFromDB()
        {
            string[] bookslist;
            connection.Open();
            SqlCommand command1 = new SqlCommand("SELECT * FROM Books", connection);
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Domain.Book book = new Domain.Book(reader1.GetString(1), reader1.GetString(2), Convert.ToInt32(reader1[0]));
                    repositoryBooks.InsertElement(book);
                }
            }
            reader1.Close();

            command1 = new SqlCommand("SELECT * FROM Subscriber", connection);
            reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    Domain.Subscriber subscriber = new Domain.Subscriber(reader1.GetString(1), reader1.GetString(2), Convert.ToInt32(reader1[0]));
                    bookslist = reader1.GetString(3).Split(' ');
                    foreach (string bookid in bookslist)
                    {
                        Domain.Book book = FindBookById(GetIdFromListBox(bookid));
                        if (book != null)
                        {
                            subscriber.AddBook(book);
                            RemoveBook(book);
                        }
                    }
                    repositorySubscribers.InsertElement(subscriber);
                }
            }
            reader1.Close();
            connection.Close();
        }

        public void SaveToDB()
        {
            string bookslist = "";
            Domain.Subscriber subscriber;
            connection.Open();
            for(int i=0;i<repositorySubscribers.ElementLength();i++)
            {
                bookslist = "";
                subscriber = repositorySubscribers.GetElement(i);
                for (int j = 0; j < subscriber.BookCount(); j++)
                    bookslist = bookslist + " " + subscriber.GetBook(j).BookID;
                SqlCommand command1 = new SqlCommand("UPDATE Subscriber SET books='" + bookslist + "' WHERE name='" +subscriber.Name + "'", connection);
                command1.ExecuteNonQuery();
            }
            connection.Close();
        }

    }
}
