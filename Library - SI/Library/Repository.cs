﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concert
{
    public class Repository<T> : IObservable<T> where T : IDObject , IComparable
    {
        private List<T> list = new List<T>();
        private List<IObserver<T>> observers = new List<IObserver<T>>();

        public IDisposable Subscribe(IObserver<T> observer)
        {
            if (!observers.Contains(observer))
                observers.Add(observer);
            return new Unsubscriber(observers, observer);
        }
        private void Notify(T next)
        {
            foreach (IObserver<T> obs in this.observers)
            {
                obs.OnNext(next);
            }
        }
        private class Unsubscriber : IDisposable
        {
            private List<IObserver<T>> _observers;
            private IObserver<T> _observer;
            public Unsubscriber(List<IObserver<T>> observers, IObserver<T> observer)
            {
                this._observers = observers;
                this._observer = observer;
            }
            public void Dispose()
            {
                if (_observer != null && _observers.Contains(_observer))
                    _observers.Remove(_observer);
            }
        }
        public void InsertElement(T Element)
        {
            list.Insert(list.Count, Element);
            this.Notify(Element);
        }
        public void InsertElementAt(T Element,int position)
        {
            list.Insert(position, Element);
            this.Notify(Element);
        }
        public void RemoveElement(IComparable id)
        {
            for (int i = 0; i < list.Count; i++)
                if (id.Equals(list[i]) == true)
                {
                    list.RemoveAt(i);
                    break;
                }
               this.Notify(null);
        }
        public void RemoveElementAt(int position)
        {
            for (int i = 0; i < list.Count; i++)
                if (i==position)
                    list.RemoveAt(i);
            this.Notify(null);
        }
        public void RemoveAllElements()
        {
            int count = list.Count;
            for (int i = 0; i < count; i++)
            {
                this.Notify(list.ElementAt(0));
                list.RemoveAt(0);
            }
        }
        public T GetElement(int position)
        {
            return list[position];
        }
        public int ElementLength()
        {
           return list.Count;
        }
        public int FindElementbyName(T Element)
        {
           for (int i = 0; i < list.Count; i++)
           {
               if (Element.CompareTo(list[i])>0)
                   return i;
           }
          return -1;
        }
        public int Find(IComparable id)
        {
            int i = 0;
            foreach (T elem in list)
            {
                if (elem.ID == id)
                    return i;
                i++;
            }
            return -1;
        }
    }
}
