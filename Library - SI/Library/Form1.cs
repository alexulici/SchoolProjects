﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concert
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public Controller controller = new Controller();
        Form2 form2;
        Form3 form3;

        private void button2_Click(object sender, EventArgs e)
        {
            controller.LoadFromDB();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            form2 = new Form2(controller);
            form2.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            form3 = new Form3(controller);
            form3.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            controller.SaveToDB();
        }
    }
}
