﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab2C.Exceptions
{
    public class RepositoryException : System.ApplicationException
    {
        public RepositoryException(string message)
            : base(message)
        {
        }
    }
}
