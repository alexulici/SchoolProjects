﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Tara {
	private String nume;
	private int suprafata;
	private String capitala;
	public Tara() {nume = null; suprafata = 0; capitala = null;}
	public Tara(String nume, String capitala, int suprafata) {
		this.nume = nume;
		this.suprafata = suprafata;
		this.capitala = capitala;
	}
	public void setNume(String nume)
	{
		this.nume=nume;
	}
	public void setSuprafata(int suprafata)
	{
		this.suprafata=suprafata;
	}
	public void setCapitala(String capitala)
	{
		this.capitala=capitala;
	}
	public String getNume()
	{
		return this.nume;
	}
	public int getSuprafata()
	{
		return this.suprafata;
	}
	public String getCapitala()
	{
		return this.capitala;
	}
	public String toString(){
		return this.getNume() + " " + this.getCapitala() + " " + this.getSuprafata();
	}
}
