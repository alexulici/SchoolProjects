﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2C.tad;

public interface Iterator<T>
{
    void first();
    bool next();
    T Element{ get; }
}