﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab2C.tad;

public interface IMap<K, T> where K : IComparable<K>
    {
        bool add(K Cheie, T o);
        bool remove(K Cheie);
        T search(K Cheie);
        bool setvalue(K Cheie, T val);
        T[] getAll();
        IMapIterator<K, T> getIterator();
    }