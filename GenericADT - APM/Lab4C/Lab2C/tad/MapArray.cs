﻿using Lab2C.Exceptions;
using Lab2C.tad;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class MapArray<K,T> : IMap<K, T> where K: IComparable<K> where T: class
{
    private List<pair<K,T>> Map = new List<pair<K,T>>();
    public bool add(K Cheie, T o)
    {
        pair<K,T> a = new pair<K,T>(Cheie, o),b;
        if (Map.Count == 0)
        {
            Map.Insert(0, a);
        }
        else
        {
            int mapsize = Map.Count;
            for (int i = 1; i < mapsize; i++)
            {
                b = (pair<K, T>)Map.ElementAt(i);
                if (Cheie.CompareTo(b.getCheie()) < 0)
                {
                    Map.Insert(i-1, a);
                    return true;
                }
            }
            Map.Insert(mapsize,a);
        }
        return true;
    }

    public bool remove(K Cheie)
    {
        pair<K,T> a;
        int mapsize = Map.Count;
        for (int i = 0; i < mapsize; i++)
        {
            a = (pair<K, T>)Map.ElementAt(i);
            if (Cheie.CompareTo(a.getCheie()) == 0)
                Map.RemoveAt(i);
        }
        return true;
    }

    public T search(K Cheie)
    {
        pair<K,T> a;
        int init = 0, sf = Map.Count, mid;
        if (sf == 0)
            return null;
        do
        {
            mid = (sf + init) / 2;
            a = (pair<K, T>)Map.ElementAt(mid);
            if (Cheie.CompareTo(a.getCheie()) == 0)
                return a.getVal();
            else if (Cheie.CompareTo(a.getCheie()) < 0)
                sf = mid;
            else
                init = mid + 1;
        }
        while (init < sf);
        return null;
    }

    public bool setvalue(K Cheie, T val)
    {
        pair<K,T> a = new pair<K,T>(Cheie, val);
        for (int i = 0; i < Map.Count; i++)
        {
            a = (pair<K, T>)Map.ElementAt(i);
            if (Cheie.CompareTo(a.getCheie()) == 0)
            {
                a.setVal(val);
            }
        }
        return false;
    }

    public int Count()
    {
        return Map.Capacity;
    }

    public T[] getAll()
    {
            T[] values = new T[100];
            int index = 0;
            DictionaryIterator<K,T> it = (DictionaryIterator<K,T>)this.getIterator();
            do
            {
                values[index++] = (it.Element).getVal();
            }
            while (it.next());
            if (it.Element != null) values[index++] = (it.Element).getVal();

            return values;
    }

    public IMapIterator<K, T> getIterator()
    {
        return new DictionaryIterator<K,T>(this);
    }


    public class DictionaryIterator<S, Q> : IMapIterator<S, Q> where S : IComparable<S> where Q : class
    {
        private MapArray<S,Q> d;
        private List<pair<S, Q>>.Enumerator listIterator;
        private pair<S,Q> crt;

        public DictionaryIterator(MapArray<S,Q> d)
        {
            this.d = d;
            listIterator = (List<pair<S,Q>>.Enumerator)d.Map.GetEnumerator();
            first();
        }

        public void first()
        {
            listIterator.MoveNext();
            crt = (pair<S, Q>)listIterator.Current;
        }

        public bool next()
        {
            if (crt == null)
                throw new RepositoryException("There are no more entries...");
            bool res;
            res = listIterator.MoveNext();
            if (res)
            {
                crt = (pair<S, Q>)listIterator.Current;
                return true;
            }
            else
                return false;
        }

        public pair<S, Q> Element
        {
            get { return listIterator.Current; }
        }
    }
}