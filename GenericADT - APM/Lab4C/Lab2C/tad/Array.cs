﻿using Lab2C.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*public class Array<T> where T : IComparable
{
    private T[] v;
    private int size;
    private int capacity;

    public Array()
    {
        capacity = 100;
        v = new T[capacity];
        size = 0;
    }

    public Boolean insert(T p,int poz)
    {
        if (poz < 0)
            poz = 0;
        else
            if (poz > size)
                poz = size;
        for (int i = size; i > poz; i--)
            v[i + 1] = v[i];
        v[poz] = p;
        size +=1;
        return true;
    }

    public Boolean remove(int poz)
    {
        for (int i = poz; i < size; i++)
            v[i] = v[i + 1];
        size -= 1;
        return false;
    }

    public T getElement(int poz)
    {
        return this.v[poz];
    }

    public int getsize()
    {
        return this.size;
    }

    public T this[int key]
    {
        get
        {
            return v[key];
        }
        set
        {
            v[key] = value;
        }
    }
    public Iterator<T> getIterator()
    {
        return new ListIterator<T>(this);
    }

    public class ListIterator<T> : Iterator<T> where T: IComparable
    {
        internal Array<T> outerList;
        internal T crt;
        internal int poz;

        internal ListIterator(Array<T> list)
        {
            outerList = list;
            first();
        }

        public void first()
        {
            poz = 0;
            crt = (T)outerList.v[poz];
        }

        public void next()
        {
            if (crt == null) throw new RepositoryException("[ListIterator]: no more elements...");
            poz++;
            crt = (T)outerList.v[poz];
        }

        public bool hasNext()
        {
            poz++;
            return outerList.v[poz] != null;
        }

        public T Element
        {
            get { return outerList.v[poz]; }
        }
    }
}*/