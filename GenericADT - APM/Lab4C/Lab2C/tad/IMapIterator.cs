﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab2C.tad
{
    public interface IMapIterator<K, T> where K : IComparable<K>
    {
        void first();
        bool next();
        pair<K, T> Element
        { get; }
    }
}
