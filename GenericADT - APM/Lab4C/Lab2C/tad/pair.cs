﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Lab2C.tad
{
    public class pair<K, T> : IComparable where K : IComparable<K>
    {

        private K Cheie;
        private T Val;

        public pair(K Cheie, T Val)
        {
            this.Cheie = Cheie;
            this.Val = Val;
        }
        public K getCheie()
        {
            return Cheie;
        }
        public void setCheie(K cheie)
        {
            Cheie = cheie;
        }
        public T getVal()
        {
            return Val;
        }
        public void setVal(T val)
        {
            Val = val;
        }
        public int CompareTo(object o)
        {
            pair<K, T> p = o as pair<K, T>;
            return p == null ? -1 : Cheie.CompareTo(p.Cheie);
        }

        public int CompareTo(pair<K, T> p)
        {
            return p == null ? -1 : Cheie.CompareTo(p.Cheie);
        }
    }
}
