﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public class Lista {
	private Node head;
	private int size;

	public Lista() {
		this.head = null;
		this.size = 0;
	}
	
	public Boolean add(Object p) {
		Node node = new Node(p);
		node.setNext(head);
		head = node;
		this.size += 1;
		return true;
	}
	
	public Boolean remove(Object p) {
		Node node = new Node(p);
		node = head;
		if(node.getInfo() == p)
		{
			head=node.getNext();
			this.size-=1;
			return true;
		}
		while(node.getNext() != null)
		{
			if(node.getNext().getInfo() == p)
			{
				
				if(node.getNext().getNext() != null)
					node.setNext(node.getNext().getNext());
				else
					node.setNext(null);
				this.size-=1;
				return true;
			}
			node=node.getNext();
		}
		return false;
	}
	
	public Object search(Object p) {
		Node crt = head;
		while(crt.getNext() != null)
		{
			if(crt.getInfo() == p)
				return p;
			crt=crt.getNext();
		}
		return null;
	}
	public Object getindex(int n){
		int i=0;
		Node crt = head;
		if(crt.getNext()==null)
			return crt.getInfo();
		while(crt.getNext() != null)
		{
			if(i == n)
				return crt.getInfo();
			crt=crt.getNext();
			i++;
		}
		if(i == n)
			return crt.getInfo();
		return null;
	}
	public int getsize(){
		return this.size;
	}
	
public class Node {
		private Object info;
		private Node next;
        private object p;

		public Node(Object info) {
			this.info = info;
			next = null;
		}

		public Object getInfo() {
			return info;
		}

		public void setInfo(Object info) {
			this.info = info;
		}

		public Node getNext() {
			return next;
		}

		public void setNext(Node next) {
			this.next = next;
		}
	}
}
