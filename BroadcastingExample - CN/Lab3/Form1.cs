﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab3
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        IPAddress Ip;
        IPAddress SubnetMask;
        IPAddress BroadcastIp;
        Thread RecieveThread;
        Boolean ServerOpen=false;
        const int port = 1234;
        List<User> Clients = new List<User>();

        private void button1_Click(object sender, EventArgs e)
        {
            SendMessage("MESSAGE:" + textBox3.Text + "\0");
            textBox3.Clear();
        }

        #region Network info
        /*get the IPaddress and the subnetmask*/
        public IPAddress GetBroadcastAddress(IPAddress address, IPAddress subnetMask)
        {
            byte[] ipAdressBytes = address.GetAddressBytes();
            byte[] subnetMaskBytes = subnetMask.GetAddressBytes();

            if (ipAdressBytes.Length != subnetMaskBytes.Length)
                throw new ArgumentException("Lengths of IP address and subnet mask do not match.");

            byte[] broadcastAddress = new byte[ipAdressBytes.Length];
            for (int i = 0; i < broadcastAddress.Length; i++)
            {
                broadcastAddress[i] = (byte)(ipAdressBytes[i] | (subnetMaskBytes[i] ^ 255));
            }
            return new IPAddress(broadcastAddress);
        }

        private void getNetworkInfo()
        {
            IPHostEntry host;
            host = Dns.GetHostEntry(Dns.GetHostName());

            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    Ip = ip;
                    SubnetMask = IPAddress.Parse(getSubnetMask(ip));
                }
            }
        }
        private string getSubnetMask(IPAddress address)
        {
            foreach (NetworkInterface adapter in NetworkInterface.GetAllNetworkInterfaces())
            {
                foreach (UnicastIPAddressInformation unicastIPAddressInformation in adapter.GetIPProperties().UnicastAddresses)
                {
                    if (unicastIPAddressInformation.Address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        if (address.Equals(unicastIPAddressInformation.Address))
                        {
                            return unicastIPAddressInformation.IPv4Mask.ToString();
                        }
                    }
                }
            }
            throw new ArgumentException(string.Format("Can't find subnetmask for IP address '{0}'", address));
        }
        #endregion
        #region Client
        public void SendMessage(string data)
        {
            byte[] sendBytes = new Byte[1024];
            byte[] rcvPacket = new Byte[1024];
            UdpClient client = new UdpClient();
            IPAddress address = BroadcastIp;
            client.Connect(address, port);
            sendBytes = Encoding.ASCII.GetBytes(data);
            client.Send(sendBytes, sendBytes.GetLength(0));
            client.Close();  //close connection
        }
        #endregion
        #region Server
        public void Server()
        {
            string data = "";

            UdpClient server = new UdpClient(port);

            string result;
            IPEndPoint remoteIPEndPoint = new IPEndPoint(IPAddress.Any, 0);
            while (ServerOpen == true)
            {
                byte[] receivedBytes = server.Receive(ref remoteIPEndPoint);
                data = Encoding.ASCII.GetString(receivedBytes);
                result = ParseMessage(data.TrimEnd(),remoteIPEndPoint);
                if(!result.Equals("NORETURN"))
                    textBox2.AppendText(Environment.NewLine + result);
            }
            server.Close();  //close the connection
        }

        private string ParseMessage(string p, IPEndPoint endpoint)
        {
            if (p.Substring(0, 4).Equals("JOIN"))
            {
                User newuser = new User();
                newuser.Ip = IPAddress.Parse(endpoint.Address.ToString());
                newuser.Name = p.Substring(6, p.Length - 7);
                newuser.Timer = 60;
                foreach (User x in Clients)
                {
                    if (x.Ip.Equals(newuser.Ip))
                    {
                        x.Timer = 60;
                        return "NORETURN";
                    }
                }
                Clients.Add(newuser);
                RefreshClientsList();
            }
            else
                if (p.Substring(0, 5).Equals("LEAVE"))
                {
                    foreach (User x in Clients)
                    {
                        if (x.Ip.Equals(IPAddress.Parse(endpoint.Address.ToString())))
                        {
                            Clients.Remove(x);
                            RefreshClientsList();
                            string temp = x.Name + " left the chat";
                            return temp;
                        }
                    }
                }
                else
                if (p.Substring(0, 7).Equals("MESSAGE"))
                {
                    string temp;
                    foreach (User x in Clients)
                    {
                        if (x.Ip.Equals(IPAddress.Parse(endpoint.Address.ToString())))
                        {
                            temp = x.Name + ":" + p.Substring(8, p.Length - 9);
                            return temp;
                        }
                    }
                    temp = "UNKNOWN:" + p.Substring(8, p.Length - 8);
                    return temp;
                }
            return p;
        }

        public void RefreshClientsList()
        {
            listBox1.Items.Clear();
            foreach(User x in Clients)
            {
                listBox1.Items.Add(x.Name + "(" + x.Ip + ")");
            }
        }
        #endregion

        private void Form1_Load(object sender, EventArgs e)
        {
            TextBox.CheckForIllegalCrossThreadCalls = false;
            getNetworkInfo();
            BroadcastIp = GetBroadcastAddress(Ip, SubnetMask);
            textBox2.Text = "Your IP adress is : " + Ip + Environment.NewLine;
            textBox2.Text = textBox2.Text + "Your SubnetMast is : " + SubnetMask + Environment.NewLine;
            textBox2.Text = textBox2.Text + "Your Broadcast Ip is : " + BroadcastIp + Environment.NewLine;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (ServerOpen == false)
            {
                ServerOpen = true;
                RecieveThread = new Thread(Server);
                RecieveThread.IsBackground = true;
                RecieveThread.Start();
                Thread.Sleep(100);
                SendMessage("JOIN: " + textBox1.Text + "\0");
                button2.Text = "Close";
                button1.Enabled = true;
                textBox1.Enabled = false;
                timer1.Start();
            }
            else
            {
                Clients.Clear();
                RefreshClientsList();
                ServerOpen = false;
                SendMessage("LEAVE");
                textBox1.Enabled = true;
                button2.Text = "Start";
                button1.Enabled = false;
                timer1.Stop();
            }
        }

        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter && ServerOpen == true)
            {
                SendMessage("MESSAGE:" + textBox3.Text + "\0");
                textBox3.Clear();
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        class User
        {
            IPAddress ip;
            string name;
            int timer = 0;

            public IPAddress Ip
            {
                get { return ip; }
                set { this.ip = value; }
            }

            public string Name
            {
                get { return name; }
                set { this.name = value; }
            }

            public int Timer
            {
                get { return timer; }
                set { this.timer = value; }
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            Boolean Modify = false;
            SendMessage("JOIN: " + textBox1.Text + "\0");
            User x = new User();
            for (int i = 0; i < Clients.Count; i++)
            {
                x = Clients.ElementAt(i);
                if (x.Timer <= 0)
                {
                    Clients.RemoveAt(i);
                    Modify = true;
                }
                x.Timer = x.Timer - 1;
            }
            if (Modify == true)
                RefreshClientsList();
        }

        private void textBox3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }
    }
}
