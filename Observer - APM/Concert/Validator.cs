﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    class ValidatorConcert : IValidator<Domain.Concert>
    {
        public bool validate(Domain.Concert a)
        {
            StringBuilder Message = new StringBuilder("");
            if (a.Name == "")
                Message.Append("Concert name cannot be empty");
            if (a.MaxSlots < a.Slots)
                Message.Append("You can't have more slots than max slots");
            if (Message.ToString() == "")
                return true;
            else
                throw new Exception(Message.ToString());
        }
        
    }
    class ValidatorSubscriber : IValidator<Domain.Subscriber>
    {
        public bool validate(Domain.Subscriber a)
        {
            StringBuilder Message = new StringBuilder("");
            if (a.Name == "")
                Message.Append("Subscriber name cannot be empty");
            if (a.Tickets > 10)
                Message.Append("You can't have more than 10 tickets");
                        if (Message.ToString() == "")
                return true;
            else
                throw new Exception(Message.ToString());
        }
    }
}
