﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concert
{
    public partial class Form1 : Form , IObserver<Object>
    {
        public Form1()
        {
            InitializeComponent();
        }
        public Controller controller = new Controller();
        Form2 form2;

        private void Form1_Load(object sender, EventArgs e)
        {
            controller.SubscribeConcert(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //clear old data
            listBox1.Items.Clear();
            comboBox1.Items.Clear();
            controller.RemoveAllConcerts();
            controller.RemoveAllSubscribers();
            controller.Load();
            //////////////////////

            
            Domain.Subscriber[] slist = new Domain.Subscriber[controller.GetSubscriberSize()];
            slist = controller.GetAllSubscribers();
            foreach (Domain.Subscriber subscriber in slist)
            {
                comboBox1.Items.Add(subscriber.Name);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
                int index = controller.FindSubscriberByName(comboBox1.Text);
            if(index>=0)
            {
                Domain.Subscriber subscriber = controller.GetSubscriberAt(controller.FindSubscriberByName(comboBox1.Text));
                if (subscriber != null)
                {
                    form2 = new Form2(this, subscriber, index);
                    form2.Show();
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            controller.Save();
        }

        private void RefreshList()
        {
            listBox1.Items.Clear();
            Domain.Concert[] clist = new Domain.Concert[controller.GetConcertSize()];
            clist = controller.GetAllConcerts();
            foreach (Domain.Concert concert in clist)
            {
                listBox1.Items.Add(concert.Name + " " + concert.Slots.ToString() + "/" + concert.MaxSlots.ToString());
            }
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }

        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }

        public void OnNext(object value)
        {
            this.RefreshList();
        }
    }
}
