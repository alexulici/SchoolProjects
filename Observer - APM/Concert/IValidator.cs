﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    public interface IValidator<T>
    {
        bool validate(T elem);
    }
}
