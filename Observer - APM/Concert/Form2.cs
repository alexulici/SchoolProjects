﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Concert
{
    public partial class Form2 : Form, IObserver<Object>
    {
        Form1 form1;
        Domain.Subscriber Sub;
        int subindex;
        private IDisposable unsub1,unsub2;
        public Form2()
        {
            InitializeComponent();
        }
        public Form2(Form1 form1,Domain.Subscriber s,int subindex)
        {
            InitializeComponent();
            this.form1 = form1;
            this.Sub = s;
            this.subindex = subindex;
            unsub1=form1.controller.SubscribeConcert(this);
            unsub2=form1.controller.SubscribeSubscribers(this);
            RefreshList();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = Sub.Name;
        }

        public void OnCompleted()
        {
            throw new NotImplementedException();
        }
        public void OnError(Exception error)
        {
            throw new NotImplementedException();
        }
        public void OnNext(Object value)
        {
            this.RefreshList();
        }


        private void button1_Click(object sender, EventArgs e)
        {
            int index = listBox1.SelectedIndex;
            if (index >= 0)
                if (form1.controller.HasConcert(Sub, form1.controller.GetConcertAt(index).Name) == false)
                    if (Sub.Tickets > 0)
                        if (form1.controller.TakeSlot(form1.controller.GetConcertAt(index)))
                        {
                            Sub.AddConcert(form1.controller.GetConcertAt(index));
                            Sub.BuyTicket();
                            form1.controller.UpdateSubscriber(Sub, subindex);
                        }
                        else
                            MessageBox.Show("Nu mai sunt locuri disponibile pentru acest concert.");
                    else
                        MessageBox.Show("Nu mai ai abonamente.");
                else
                    MessageBox.Show("Te-ai inscris la acest concert deja.");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int listboxpos = listBox2.SelectedIndex;
            int index = form1.controller.FindConcertByName(listBox2.Text);
            if (listboxpos>=0)
            {
                form1.controller.FreeSlot(form1.controller.GetConcertAt(index));
                Sub.RemoveConcert(Sub.GetConcert(listboxpos));
                Sub.SellTicket();
                form1.controller.UpdateSubscriber(Sub, subindex);
            }
        }

        public void RefreshList()
        {
            listBox1.Items.Clear();
            Domain.Concert[] clist = new Domain.Concert[form1.controller.GetConcertSize()];
            clist = form1.controller.GetAllConcerts();
            foreach (Domain.Concert concert in clist)
            {
                listBox1.Items.Add(concert.Name + " " + concert.Slots.ToString() + "/" + concert.MaxSlots.ToString());
            }

            listBox2.Items.Clear();
            label2.Text = "Abonamente : " + Sub.Tickets.ToString();
            for (int i = 0; i < Sub.ConcertCount(); i++)
                listBox2.Items.Add(Sub.GetConcert(i).Name);
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            unsub1.Dispose();
            unsub2.Dispose();
        }
    }
}
