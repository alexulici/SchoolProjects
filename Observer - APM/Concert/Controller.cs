﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    public class Controller
    {
        private Repository<Domain.Concert> repositoryConcert = new Repository<Domain.Concert>();
        private Repository<Domain.Subscriber> repositorySubscribers = new Repository<Domain.Subscriber>();

        public IDisposable SubscribeConcert(IObserver<Domain.Concert> o)
        {
            return repositoryConcert.Subscribe(o);

        }
        public IDisposable SubscribeSubscribers(IObserver<Domain.Subscriber> o)
        {
            return repositorySubscribers.Subscribe(o);

        }
        public Boolean InsertConcert(Domain.Concert concert)
        {
            ValidatorConcert validator = new ValidatorConcert();
            try
            {
                validator.validate(concert);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            repositoryConcert.InsertElement(concert);
            return true;
        }
        public Boolean InsertSubscriber(Domain.Subscriber subscriber)
        {
            ValidatorSubscriber validator = new ValidatorSubscriber();
            try
            {
                validator.validate(subscriber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            repositorySubscribers.InsertElement(subscriber);
            return true;
        }
        public Boolean UpdateSubscriber(Domain.Subscriber subscriber, int position)
        {
            ValidatorSubscriber validator = new ValidatorSubscriber();
            try
            {
                validator.validate(subscriber);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            repositorySubscribers.RemoveElementAt(position);
            repositorySubscribers.InsertElementAt(subscriber,position);
            return true;
        }
        public void RemoveConcert(Domain.Concert concert)
        {
            repositoryConcert.RemoveElement(concert);
        }
        public void RemoveSubscriber(Domain.Subscriber subscriber)
        {
            repositorySubscribers.RemoveElement(subscriber);   
        }
        public void RemoveAllConcerts()
        {
            repositoryConcert.RemoveAllElements();
        }
        public void RemoveAllSubscribers()
        {
            repositorySubscribers.RemoveAllElements();
        }
        public int GetConcertSize()
        {
            return repositoryConcert.ElementLength();
        }
        public int GetSubscriberSize()
        {
            return repositorySubscribers.ElementLength();
        }
        public Domain.Concert GetConcertAt(int position)
        {
            return repositoryConcert.GetElement(position);
        }
        public Boolean TakeSlot(Domain.Concert a)
        {
            int pos = repositoryConcert.Find(a.ID);
            Domain.Concert nou = repositoryConcert.GetElement(pos);
            if (nou.Slots >= nou.MaxSlots)
            {
                return false;
            }
            else
            {
                nou.TakeSlot();
                repositoryConcert.RemoveElementAt(pos);
                repositoryConcert.InsertElementAt(nou,pos);
                return true;
            }
        }
        public void FreeSlot(Domain.Concert a)
        {
            int pos = repositoryConcert.Find(a.ID);
            Domain.Concert nou = repositoryConcert.GetElement(pos);
                nou.FreeSlot();
                repositoryConcert.RemoveElementAt(pos);
                repositoryConcert.InsertElementAt(nou, pos);
        }
        public Domain.Subscriber GetSubscriberAt(int position)
        {
            return repositorySubscribers.GetElement(position);
        }
        public int FindConcertByName(string name)
        {
            Domain.Concert[] list = new Domain.Concert[repositoryConcert.ElementLength()];
            list = GetAllConcerts();
            for (int i = 0; i < repositoryConcert.ElementLength(); i++)
            {
                if (name.Equals(list[i].Name))
                    return i;
            }
            return -1;
        }
        public Boolean HasConcert(Domain.Subscriber a,string name)
        {
            for(int i=0 ; i<a.ConcertCount();i++)
            {
                if (name.Equals(a.GetConcert(i).Name))
                    return true;
            }
            return false;
        }
        public int FindSubscriberByName(string name)
        {
            Domain.Subscriber[] list = new Domain.Subscriber[repositorySubscribers.ElementLength()];
            list = GetAllSubscribers();
            for (int i = 0; i < repositorySubscribers.ElementLength(); i++)
            {
                if (name.Equals(list[i].Name))
                    return i;
            }
            return -1;
        }
        public Domain.Concert[] GetAllConcerts()
        {
            Domain.Concert[] list = new Domain.Concert[repositoryConcert.ElementLength()];
            for (int i = 0; i < repositoryConcert.ElementLength(); i++)
            {
                list[i] = repositoryConcert.GetElement(i);
            }
            return list;
        }
        public Domain.Subscriber[] GetAllSubscribers()
        {
            Domain.Subscriber[] list = new Domain.Subscriber[repositorySubscribers.ElementLength()];
            for (int i = 0; i < repositorySubscribers.ElementLength(); i++)
            {
                list[i] = repositorySubscribers.GetElement(i);
            }
            return list;
        }
        public void Load()
        {
            string[] load = new string[100];
            load = repositoryConcert.loadfromfile("Concerts.txt");
            foreach (string line in load)
            {
                if (line != null)
                {
                    string[] list = line.Split(';');
                    Domain.Concert concert = new Domain.Concert(list[0], Convert.ToInt32(list[1]), Convert.ToInt32(list[2]));
                    InsertConcert(concert);
                }
            }
            load = repositorySubscribers.loadfromfile("Subscribers.txt");
            foreach (string line in load)
            {
                if (line != null)
                {
                    string[] list = line.Split(';');
                    Domain.Subscriber subscriber = new Domain.Subscriber(list[0]);
                    for (int i = 0; i < Convert.ToInt32(list[1]); i++)
                    {
                        Domain.Concert concert = new Domain.Concert(list[i+2],0,0);
                        subscriber.AddConcert(concert);
                        subscriber.BuyTicket();
                    }
                    InsertSubscriber(subscriber);
                }
            }
        }
        public void Save()
        {
            Domain.Concert[] clist = GetAllConcerts();
            string[] cresult = new string[repositoryConcert.ElementLength()];
            int i=0;
            foreach(Domain.Concert concert in clist)
            {
                cresult[i]=concert.Name + ";" + concert.Slots + ";" + concert.MaxSlots;
                i++;
            }
            repositoryConcert.savetofile("Concerts.txt", cresult);

            Domain.Subscriber[] slist = GetAllSubscribers();
            string[] sresult = new string[repositorySubscribers.ElementLength()];
            i = 0;
            foreach (Domain.Subscriber subscriber in slist)
            {
                sresult[i] = subscriber.Name + ";" + subscriber.ConcertCount();
                for (int j = 0; j < subscriber.ConcertCount(); j++)
                    sresult[i] = sresult[i] + ";" + subscriber.GetConcert(j).Name;
                i++;
            }
            repositorySubscribers.savetofile("Subscribers.txt", sresult);
        }

    }
}
