﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concert
{
    public class Domain
    {
        public class Concert : IDObject , IComparable
        {
            private string name;
            private int current_slots;
            private int max_slots;

            public Concert(string name, int current_slots,int max_slots):base (name)
            {
                this.name = name;
                this.current_slots = current_slots;
                this.max_slots = max_slots;
            }

            public string Name
            {
                get { return name; }
                set
                {
                    this.name = value;
                }
            }
            public int Slots
            {
                get { return current_slots; }
            }
            public int MaxSlots
            {
                get { return max_slots; }
            }
            public void TakeSlot()
            {
                this.current_slots++;
            }
            public void FreeSlot()
            {
                this.current_slots--;
            }
            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                Concert concert = obj as Concert;
                if (concert != null)
                    return this.name.CompareTo(concert.name);
                else
                    throw new ArgumentException("Object is not a Concert");
            }
        }
        public class Subscriber : IDObject , IComparable
        {
            private string name;
            private List<Concert> concerte = new List<Concert>();
            private int tickets;

            public Subscriber(string name):base(name)
            {
                this.name = name;
                this.tickets = 10;
            }
            public string Name
            {
                get { return name; }
                set
                {
                    this.name = value;
                }
            }
            public int Tickets
            {
                get { return tickets; }
            }
            public Domain.Concert GetConcert(int position)
            {
                return concerte.ElementAt(position);
            }
            public int ConcertCount()
            {
                return concerte.Count;
            }
            public void AddConcert(Concert a)
            {
                concerte.Add(a);
            }
            public void RemoveConcert(Concert a)
            {
                concerte.Remove(a);
            }
            public void BuyTicket()
            {
                this.tickets--;
            }
            public void SellTicket()
            {
                this.tickets++;
            }
            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                Subscriber subscriber = obj as Subscriber;
                if (subscriber != null)
                    return this.name.CompareTo(subscriber.name);
                else
                    throw new ArgumentException("Object is not a Subscriber");
            }
        }
    }
}
