﻿using LoSDispatch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace LoSDispatch
{
    class Commands
    {
        public delegate string MethodDelegate(Command newCommand, Player player);

        public static Command ParseCommand(string command)
        {
            Command newCommand = new Command();

            string parameters = "";
            string[] words = command.Split(' ');
            try
            {
                for (int i = 1; i <= words.Length - 1; i++)
                    parameters = parameters + " " + words[i];
                newCommand = new Command(Convert.ToInt32(words[0]), parameters.Substring(1, parameters.Length - 1));
            }
            catch
            {
                return null;
            }
            return newCommand;
        }

        public static string ExecuteCommand(string command, Player player)
        {
            Command newCommand = ParseCommand(command);
            if (newCommand == null)
                return "noreturn";
            var deList = new List<MethodDelegate> { _0Register, _1Login, _02FindMatch, null, null, null, null, null, null,
                _09CheckToken, _10ServerConnects, _11GetMatchInfo, null, _13MatchOver, _14ConfirmReady,null, _16LadderResponse,
            _17CurrentMatches,_18ShowOnlinePlayers};
            if (newCommand.GetCommandID() < deList.Count)
            {
                string result = deList[newCommand.GetCommandID()](newCommand, player);
                if (result.Equals("noreturn"))
                    return "noreturn";
                else
                    return newCommand.GetCommandID() + " " + result;
            }
            else
                return "noreturn";
        }

        //Interpreting commands received from client

        private static string _0Register(Command newCommand, Player player)
        {
            string[] userinfo = newCommand.parameters.Split(' '), result;
            int id;
            result = Database.ExecuteReader("SELECT ID FROM Users WHERE Username='" + userinfo[0] + "'");
            if (result != null)
            {
                return "1";
            }
            result = Database.ExecuteReader("SELECT ID FROM Users WHERE Nickname='" + userinfo[1] + "'");
            if (result != null)
            {
                return "8";
            }
            result = Database.ExecuteReader("SELECT MAX(ID) FROM Users");
            if (result[0].Equals(""))
                id = 0;
            else
                id = Convert.ToInt16(result[0]) + 1;
            Database.ExecuteNonQuery("INSERT INTO Users (ID,Username,Nickname,Password,Email,Role,Mmr) VALUES (" + id + ",'" + userinfo[0] + "','" + userinfo[1] + "','" + userinfo[2] + "','" + userinfo[3] + "',0,1000)");
            return "0";
        }

        private static string _1Login(Command newCommand, Player player)
        {
            Player x;
            string[] userinfo = newCommand.parameters.Split(' ');
            string[] result = Database.ExecuteReader("SELECT ID,Nickname,Role,Mmr FROM Users WHERE Username='" + userinfo[0] + "' AND Password='" + userinfo[1] + "'");
            if (result != null)
            {
                player.Id = Convert.ToInt16(result[0]);
                player.Nickname = result[1];
                player.Role = Convert.ToInt16(result[2]);
                player.Mmr = Convert.ToInt16(result[3]);
                player.State = -1;
                for (int i = 0; i < Controller.PlayersList.Count; i++)
                {
                    x = Controller.PlayersList.ElementAt(i);
                    if (x.Id == player.Id)
                    {
                        player.Token = x.Token;
                        player.State = x.State;
                        if (player.State == 1)
                            TCPFunctions.SendString("12 ", player.Socket);
                        Controller.PlayersList.Remove(x);
                        break;
                    }
                }
                if (player.State == -1)
                    player.State = 0;
                if (player.Token == null)
                    player.Token = Utilities.GenerateToken();
                Controller.PlayersList.Add(player);
                Controller.AwaitingList.Remove(player);
                return player.Token;
            }
            else
                return "1";
        }

        private static string _02FindMatch(Command newCommand, Player player)
        {
            string[] param = newCommand.parameters.Split(' ');
            GameCore.PlayerQueue(param[0], player);
            return "noreturn";
        }

        private static string _09CheckToken(Command newCommand, Player player)
        {
            Player x;
            for (int i = 0; i < Controller.PlayersList.Count; i++)
            {
                x = Controller.PlayersList.ElementAt(i);
                if (x.Token.Equals(newCommand.parameters))
                {
                    x.Socket = player.Socket;
                    Controller.AwaitingList.Remove(player);
                    return "0";
                }
            }
            return "1";
        }

        private static string _10ServerConnects(Command newCommand, Player player)
        {
            IPEndPoint remoteIpEndPoint = player.Socket.RemoteEndPoint as IPEndPoint;
            Server newServer = new Server();
            newServer.Ip = remoteIpEndPoint.Address.ToString();
            newServer.Port = Convert.ToInt16(newCommand.parameters);
            newServer.Socket = player.Socket;
            Controller.ServersList.Add(newServer);
            Controller.AwaitingList.Remove(player);
            return "1";
        }

        private static string _11GetMatchInfo(Command newCommand, Player player)
        {
            Match pickedMatch;
            for (int i = 0; i < Controller.MatchesList.Count; i++)
            {
                pickedMatch = Controller.MatchesList.ElementAt(i);
                if (pickedMatch.Id.Equals(Convert.ToInt16(newCommand.parameters)))
                {
                    pickedMatch.State = 3;
                    break;
                }
            }
            return "noreturn";
        }

        private static string _13MatchOver(Command newCommand, Player player)
        {
            string[] param = newCommand.parameters.Split(' ');
            int matchid;
            Match pickedmatch;
            Player winner = null, loser = null;
            for (int i = 0; i < Controller.MatchesList.Count; i++)
            {
                pickedmatch = Controller.MatchesList.ElementAt(i);
                if (pickedmatch.Id == Convert.ToInt16(param[0]))
                {
                    pickedmatch.WinnerID = Convert.ToInt16(param[1]);
                    pickedmatch.Duration = Convert.ToInt16(param[2]);
                    Player pickedplayer;
                    Player ret;
                    Database.ExecuteNonQuery("INSERT INTO Match (ID,Winner,MatchType,Duration) VALUES (" + pickedmatch.Id + "," + pickedmatch.WinnerID + "," + pickedmatch.Type + "," + pickedmatch.Duration + ")");
                    for (int j = 0; j < pickedmatch.Players.Count; j++)
                    {
                        pickedplayer = pickedmatch.Players.ElementAt(j);
                        ret = Controller.PlayersList.Where(p => p.Id == pickedplayer.Id).FirstOrDefault();
                        if (pickedplayer.Id == pickedmatch.WinnerID)
                        {
                            winner = ret;
                        }
                        else
                            loser = ret;
                        ret.State = 0;
                        Database.ExecuteNonQuery("INSERT INTO MatchPlayers (MatchID,PlayerID) VALUES (" + pickedmatch.Id + "," + pickedplayer.Id + ")");
                    }
                    int mmrdiff = 0;
                    if (pickedmatch.Type == 1)
                    {
                        mmrdiff = GameCore.RecalcMMR(winner, loser);
                        for (int j = 0; j < pickedmatch.Players.Count; j++)
                        {
                            pickedplayer = pickedmatch.Players.ElementAt(j);
                            ret = Controller.PlayersList.Where(p => p.Id == pickedplayer.Id).FirstOrDefault();
                            if (pickedplayer.Id == pickedmatch.WinnerID)
                            {
                                ret.Mmr += mmrdiff;
                            }
                            else
                            {
                                ret.Mmr -= mmrdiff;
                            }
                            Database.ExecuteNonQuery("UPDATE Users SET Mmr=" + ret.Mmr + " WHERE ID=" + ret.Id);
                        }
                    }
                    Database.ExecuteNonQuery("UPDATE Match SET Duration=" + pickedmatch.Duration + " WHERE ID=" + pickedmatch.Id);
                    Database.ExecuteNonQuery("UPDATE Match SET Mmrdiff=" + mmrdiff + " WHERE ID=" + pickedmatch.Id);
                    matchid = pickedmatch.Id;
                    Controller.MatchesList.Remove(pickedmatch);
                    return "0 " + matchid;
                }
            }
            return "1";
        }

        private static string _14ConfirmReady(Command newCommand, Player player)
        {
            Match pickedmatch;
            Player pickedplayer;
            if (newCommand.parameters.Equals("1"))
            {
                pickedmatch = PlayerFunctions.FindPlayerMatch(player);
                for (int j = 0; j < pickedmatch.Players.Count; j++)
                {
                    pickedplayer = pickedmatch.Players.ElementAt(j);
                    if (player.Id == pickedplayer.Id)
                        pickedplayer.State = 2;
                }
                for (int j = 0; j < pickedmatch.Players.Count; j++)
                {
                    pickedplayer = pickedmatch.Players.ElementAt(j);
                    if (pickedplayer.State != 2)
                    {
                        return "noreturn";
                    }
                }
                pickedmatch.State = 1;
                GameCore.ManageMatches();
            }
            else if (newCommand.parameters.Equals("0"))
            {
                pickedmatch = PlayerFunctions.FindPlayerMatch(player);
                for (int j = 0; j < pickedmatch.Players.Count; j++)
                {
                    pickedplayer = pickedmatch.Players.ElementAt(j);
                    pickedplayer.State = 0;
                    if (player.Id != pickedplayer.Id)
                    {
                        TCPFunctions.SendString("2 0", pickedplayer.Socket);
                    }
                }
                Controller.MatchesList.Remove(pickedmatch);
            }
            return "noreturn";
        }

        private static string _16LadderResponse(Command newCommand, Player player)
        {
            for (int i = 1; i <= 10; i++)
            {
                TCPFunctions.SendString("16 " + PlayerFunctions.GetTopPlayers(i), player.Socket);
            }
            return "noreturn";
        }

        private static string _17CurrentMatches(Command newCommand, Player player)
        {
            Match pickedmatch;
            Player pickedplayer;
            string playersname = "";
            if (newCommand.parameters.Equals("0"))
            {
                for (int i = 0; i < Controller.MatchesList.Count; i++)
                {
                    pickedmatch = Controller.MatchesList.ElementAt(i);
                    if (pickedmatch.State == 3)
                    {
                        for (int j = 0; j < pickedmatch.Players.Count; j++)
                        {
                            pickedplayer = pickedmatch.Players.ElementAt(j);
                            playersname += " " + pickedplayer.Nickname;
                        }
                        TCPFunctions.SendString("17 " + pickedmatch.Id + " " + pickedmatch.Type + " " + "-1 -1 -1" + playersname, player.Socket);
                    }
                }
            }
            else if (newCommand.parameters.Equals("1"))
            {
            }
            return "noreturn";
        }

        private static string _18ShowOnlinePlayers(Command newCommand, Player player)
        {
            Player pickedplayer;
            if (newCommand.parameters.Equals("1"))
            {
                for (int i = 0; i < Controller.PlayersList.Count; i++)
                {
                    pickedplayer = Controller.PlayersList.ElementAt(i);
                    if (pickedplayer.State == 0)
                        TCPFunctions.SendString("18 " + pickedplayer.Nickname, player.Socket);
                }
                Controller.OnlinePlayersObserver.Add(player);
            }
            else if(newCommand.parameters.Equals("0"))
            {
                Controller.OnlinePlayersObserver.Remove(player);
            }
            return "noreturn";
        }
    }
}