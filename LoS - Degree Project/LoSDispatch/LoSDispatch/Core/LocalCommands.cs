﻿using LoSDispatch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class LocalCommands
    {
        public delegate string LocalDelegate(string parameter);

        public static string ExecuteLocalCommand(string command)
        {
            int i = 0;
            var deList = new List<LocalDelegate> { LocalCommand0, LocalCommand1, LocalCommand2, LocalCommand3, LocalCommand4,
                LocalCommand5, LocalCommand6 };
            List<string> commands = new List<string> { "showclients", "tracknetwork", "serverslist", "stoptracking", "x"
            , "matches", "players"};
            string[] words = Utilities.StringToArray(command);
            command = "";
            int wordsLength = words.Length;
            foreach (string word in words)
            {
                if (i != 0 && i < wordsLength - 1)
                    command = command + word + " ";
                if (i == wordsLength - 1)
                    command = command + word;
                i++;
            }
            i = 0;
            foreach (string com in commands)
            {
                if (com == words[0])
                    return deList[i](command);
                i++;
            }
            return "-1";
        }

        private static string LocalCommand0(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Shows online players
            Console.WriteLine("[SERVER]: ======= Online Users =======");
            foreach (Player player in Controller.PlayersList) //unsafe
            {
                if(player.Socket.Connected==true)
                {
                    Console.WriteLine("Client with id " + player.Id + "; IP " + player.Socket.RemoteEndPoint);
                }
                else
                    Console.WriteLine("Client with id " + player.Id + " might reconnect");
            }
            foreach (Player player in Controller.AwaitingList) //unsafe
            {
                if (player.Socket.Connected == true)
                    Console.WriteLine("Client with ip " + player.Socket.RemoteEndPoint + " not authenticated yet");
            }
            Console.WriteLine("[SERVER]: ======= ============ =======");
            Console.ResetColor();
            return "1";
        }

        private static string LocalCommand1(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Disconnects all online players and closes the sockets
            Console.WriteLine("[SERVER]: Network Tracking started");
            Console.ResetColor();
            NetworkTraffic.StartTracking();
            return "1";
        }

        private static string LocalCommand2(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            int counter = 0;
            Server server = new Server();
            Console.WriteLine("[SERVER]: ======= Servers List =======");
            for (int i=0; i<Controller.ServersList.Count; i++)
            {
                server = Controller.ServersList.ElementAt(i);
                Console.WriteLine("Server [" + counter + "] " + server.Ip + ":" + server.Port);
                counter++;
            }
            Console.WriteLine("[SERVER]: ======= ============ =======");
            Console.ResetColor();
            return "1";
        }

        private static string LocalCommand3(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("[SERVER]: Network Tracking stopped");
            Console.ResetColor();
            NetworkTraffic.StopTracking();
            return "1";
        }

        private static string LocalCommand4(string parameters)
        {
            int playergames = 0, playerwins = 0;
            string[] result;
            result = Database.ExecuteReader("SELECT * FROM Match INNER JOIN MatchPlayers ON Match.ID = MatchPlayers.MatchID WHERE PlayerID=" + 0);
            return "1";
        }

        private static string LocalCommand5(string parameters)
        {
            Match pickedmatch;
            Console.WriteLine("[SERVER]: ======= Matches List =======");
            for (int i = 0; i < Controller.MatchesList.Count; i++)
            {
                pickedmatch = Controller.MatchesList.ElementAt(i);
                Console.WriteLine("ID=" + pickedmatch.Id + " state=" +pickedmatch.State + " player1:" + pickedmatch.Players.ElementAt(0) + " player2:" +pickedmatch.Players.ElementAt(1));
            }
            Console.WriteLine("[SERVER]: ======= ============ =======");
            return "1";
        }

        private static string LocalCommand6(string parameters)
        {
            Player pickedplayer;
            Console.WriteLine("[SERVER]: ======= Players List =======");
            for (int i = 0; i < Controller.PlayersList.Count; i++)
            {
                pickedplayer = Controller.PlayersList.ElementAt(i);
                Console.WriteLine("ID=" + pickedplayer.Id + " state=" + pickedplayer.State + " Token:" + pickedplayer.Token);
            }
            Console.WriteLine("[SERVER]: ======= ============ =======");
            return "1";
        }
    }
}
