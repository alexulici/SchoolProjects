﻿using LoSDispatch.Domain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class Controller
    {
        #region ServerDefaultConfig
        public const int _PORT = 8000; //The port on which server will be hosted
        #endregion

        #region GlobalVariables
        public static readonly List<Server> ServersList = new List<Server>();
        public static readonly List<Match> MatchesList = new List<Match>();
        public static readonly List<Player> RankedPlayersQueue = new List<Player>();
        public static readonly List<Player> CasualPlayersQueue = new List<Player>();
        public static readonly List<Player> DisconnectedPlayers = new List<Player>();
        public static readonly List<Player> AwaitingList = new List<Player>(); //The list of players who are connected to server but not authenticated
        public static readonly List<Player> PlayersList = new List<Player>(); //The list of authenticated players
        public static readonly List<Player> OnlinePlayersObserver = new List<Player>(); //The list of authenticated players

        public static int MatchCount=0;
        #endregion
        public static void SetupServer()
        {
            Initialization();
            IPHostEntry IPHost = System.Net.Dns.GetHostByName(System.Net.Dns.GetHostName()); //Getting our external id
            Console.WriteLine("Setting up server...");
            Console.WriteLine("Server Local IP : " + IPHost.AddressList[0].ToString() + ":" + _PORT); //Show our local ip & port
            Console.WriteLine("Server External IP : " + Utilities.getExternalIp()); //Show the external IP
            //Creating the socket & bind
            TCPFunctions._serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            TCPFunctions._serverSocket.Bind(new IPEndPoint(IPAddress.Any, _PORT));
            TCPFunctions._serverSocket.Listen(5);
            TCPFunctions._serverSocket.BeginAccept(TCPFunctions.AcceptCallback, null);
            //Ready to accept connections
            Console.WriteLine("Server setup complete");
        }

        public static void MessageArrived(Socket sock, string text)
        {
            Server serv;
            string[] splitcommands;
            for (int j=0;j<ServersList.Count;j++)
            {
                serv = ServersList.ElementAt(j);
                if (serv.Socket == sock)
                {
                    Console.WriteLine("Server [" + serv.Ip + "] is writing : " + text);
                    splitcommands = text.Split('|');
                    Player x;
                    for (int i = 0; i < splitcommands.Length - 1; i++)
                    {
                        x = new Player();
                        x.Socket = sock;
                        string commandresult = Commands.ExecuteCommand(splitcommands[i], x); //We execute the received command
                        if (!commandresult.Equals("noreturn"))
                            TCPFunctions.SendString(commandresult, sock);
                    }
                    TCPFunctions.BeginReceive(sock);
                    return;
                }
            }
            Tuple<Player, List<Player>> PlayerList = PlayerFunctions.FindPlayerBySocket(sock, new List<List<Player>> { Controller.PlayersList, Controller.AwaitingList });
            Console.WriteLine("Received Text: " + text);
            if (text == "" || PlayerList.Item1 == null) //If the message that we got from the player is empty then he probably got disconnected so we disconnect him
            {
                PlayerFunctions.Player_Disconnect(sock);
                return;
            }
            Console.WriteLine("[" + PlayerList.Item1.Id + "] is writing something");

            splitcommands = text.Split('|');
            for (int i = 0; i < splitcommands.Length - 1; i++)
            {
                string commandresult = Commands.ExecuteCommand(splitcommands[i], PlayerList.Item1); //We execute the received command
                if (!commandresult.Equals("noreturn"))
                    TCPFunctions.SendString(commandresult, sock);
            }
            TCPFunctions.BeginReceive(sock);
        }

        public static void NewPlayerConnect(Socket sock)
        {
            Player player = new Player(); //We create a new player and we save the socket on it
            player.Id = -1;
            player.Socket = sock;
            Controller.AwaitingList.Add(player); //Adding the player to the awaiting list
            Console.WriteLine(player.Socket.RemoteEndPoint + " connected");
        }

        internal static void PlayerChangedState(Player player)
        {
            Player pickedplayer;
            if (player.State == 0)
            {
                for (int i = 0; i < Controller.OnlinePlayersObserver.Count; i++)
                {
                    pickedplayer = Controller.OnlinePlayersObserver.ElementAt(i);
                    TCPFunctions.SendString("18 " + player.Nickname, pickedplayer.Socket);
                }
            }
        }

        public static string LoadFromFile()
        {
            string path = AppDomain.CurrentDomain.BaseDirectory, ip;
            int port;
            if (File.Exists(path + "/Config.txt"))
            {
                using (StreamReader reader = new StreamReader("Config.txt"))
                {
                    string line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        if (line == null)
                            return "Serverslist file is empty";
                        string[] splitline = line.Split(':');
                        if (splitline.Length >= 2 && splitline[0] != null && splitline[1] != null)
                        {
                            ip = splitline[0];
                            port = Convert.ToInt32(splitline[1]);
                            //ServersList.Add(new Server(ip, port));
                        }
                        else
                            return "Incorrect ip format. Make sure you are using the following format IP:Port. For instance 127.0.0.1:8000.";
                    }
                }
            }
            else
                return "Configuration file not found";
            return "0";
        }

        public static void Initialization()
        {
            string[] result = Database.ExecuteReader("SELECT MAX(ID) FROM Match");
            if (result[0].Equals(""))
                MatchCount = 0;
            else
                MatchCount = Convert.ToInt16(result[0]);
        }
    }
}
