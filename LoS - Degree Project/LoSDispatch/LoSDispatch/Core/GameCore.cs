﻿using LoSDispatch.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LoSDispatch
{
    class GameCore
    {
        static System.Timers.Timer QueueTimer;
        static List<QueueRange> range = new List<QueueRange>();
        public static void PlayerQueue(string queuetype,Player p)
        {
            Match newMatch;
            if (QueueTimer == null)
            {
                QueueTimer = new System.Timers.Timer();
                QueueTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                QueueTimer.Interval = 5000;
                QueueTimer.Enabled = true;
            }
            else if(QueueTimer.Enabled == false)
            {
                QueueTimer.Enabled = true;
            }
            if (queuetype == "0")
            {
                Controller.CasualPlayersQueue.Add(p);
            }
            else if (queuetype == "1")
            {
                Controller.RankedPlayersQueue.Add(p);
                range.Add(new QueueRange(p.Mmr, p.Mmr, p));
                range.Sort((s1, s2) => s1.Player.Mmr.CompareTo(s2.Player.Mmr));
                Console.WriteLine("ADDED");
            }
            else if (queuetype == "3")
            {
                for (int i = 0; i < Controller.MatchesList.Count; i++)
                {
                    newMatch = Controller.MatchesList.ElementAt(i);
                    if (newMatch.State == 3)
                    {
                         TCPFunctions.SendString("14 " + newMatch.Server.Ip + ":" + newMatch.Server.Port, p.Socket);
                    }
                }
            }
            else if (queuetype == "4")
            {
                Controller.CasualPlayersQueue.Remove(p);
                Controller.RankedPlayersQueue.Remove(p);
                range.Remove(range.Find(x => x.Player.Equals(p)));
            }
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (Controller.CasualPlayersQueue.Count < 2 && Controller.RankedPlayersQueue.Count < 1 && Controller.MatchesList.Count == 0)
            {
                QueueTimer.Enabled = false;
                return;
            }
            if(Controller.CasualPlayersQueue.Count >= 2)
            {
                for (int i=0;i<Controller.CasualPlayersQueue.Count;i+=2)
                {
                    PrepareMatch(new List<Player>() { Controller.CasualPlayersQueue.ElementAt(i), Controller.CasualPlayersQueue.ElementAt(i + 1) },0);
                }
            }
            if (Controller.RankedPlayersQueue.Count >= 1)
            {
                QueueRange playerrange,opponent;
                Boolean opponentfound = false;
                for (int i = 0; i < range.Count && opponentfound == false; i++)
                {
                    playerrange = range.ElementAt(i);
                    for (int j=0;j<range.Count && opponentfound == false;j++)
                    {
                        opponent = range.ElementAt(j);
                        if (playerrange.Player != opponent.Player)
                        {
                            if (playerrange.Lower <= opponent.Higher && playerrange.Lower >= opponent.Lower || playerrange.Higher >= opponent.Lower && playerrange.Higher <= opponent.Higher)
                            {
                                Console.WriteLine("Opponent found");
                                PrepareMatch(new List<Player>() { range.ElementAt(i).Player, range.ElementAt(j).Player },1);
                                range.Remove(playerrange);
                                range.Remove(opponent);
                                opponentfound = true;
                            }
                        }
                    }
                }
                for (int i=0;i<range.Count;i++)
                {
                    playerrange = range.ElementAt(i);
                    playerrange.Lower -= 50;
                    playerrange.Higher += 50;
                    Console.WriteLine(playerrange.Player.Nickname + " " + playerrange.Lower + " " + playerrange.Higher);
                }
            }
            ManageMatches();
        }

        private static Server FindAvailableServer()
        {
            Server server;
            return Controller.ServersList.ElementAt(0);

            //needsfix
            for(int i=0;i<Controller.ServersList.Count; i++)
            {
                server = Controller.ServersList.ElementAt(i);
                if(server!=null)
                {
                    TCPFunctions.SendString("", server.Socket);
                }
            }
            return null;
        }

        public static void ManageMatches()
        {
            Player pickedplayer;
            Match pickedmatch;
            for (int i = 0; i < Controller.MatchesList.Count; i++)
            {
                pickedmatch = Controller.MatchesList.ElementAt(i);
                if (pickedmatch.State == 1)
                {
                    pickedmatch.State = 2;
                    Controller.MatchCount++;
                    pickedmatch.Id = Controller.MatchCount;
                    TCPFunctions.SendString("11 " + pickedmatch.ToString(), pickedmatch.Server.Socket);
                    for (int j = 0; j < pickedmatch.Players.Count; j++)
                    {
                        pickedplayer = pickedmatch.Players.ElementAt(j);
                        pickedplayer.State = 1;
                        TCPFunctions.SendString("14 " + pickedmatch.Server.Ip + ":" + pickedmatch.Server.Port, pickedplayer.Socket);
                    }
                }
            }
        }

        public static int RecalcMMR(Player winner,Player loser)
        {
            int playergames=0, playerwins=0;
            string[] result;
            result = Database.ExecuteReader("SELECT COUNT(ID) FROM Match INNER JOIN MatchPlayers ON Match.ID = MatchPlayers.MatchID WHERE Winner=" + winner.Id);
            playerwins = Convert.ToInt16(result[0]) / 2;
            result = Database.ExecuteReader("SELECT COUNT(*) FROM MatchPlayers WHERE PlayerID=" + winner.Id);
            playergames = Convert.ToInt16(result[0]);
            return Utilities.MMRFormula(loser.Mmr, playerwins, playergames - playerwins);
        }

        public static Match PrepareMatch(List<Player> PlayersList,int matchtype)
        {
            Player pickedplayer;
            Match newMatch = new Match();
            newMatch.Server = FindAvailableServer();
            newMatch.Players = PlayersList;
            newMatch.State = 0;
            newMatch.Type = matchtype;
            Controller.MatchesList.Add(newMatch);
            for (int j = 0; j < newMatch.Players.Count; j++)
            {
                pickedplayer = newMatch.Players.ElementAt(j);
                pickedplayer.State = 1;
                TCPFunctions.SendString("2 1", pickedplayer.Socket);
                Controller.CasualPlayersQueue.Remove(pickedplayer);
                Controller.RankedPlayersQueue.Remove(pickedplayer);
            }
            return newMatch;
        }
    }
}
