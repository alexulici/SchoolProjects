﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main function
            Console.Title = "Legends of Sinjar Dispatch";
            Database.Initialization();
            Controller.LoadFromFile();
            Controller.SetupServer();
            while (true)
            {
                string command = Console.ReadLine();
                LocalCommands.ExecuteLocalCommand(command);
            }
        }
    }
}
