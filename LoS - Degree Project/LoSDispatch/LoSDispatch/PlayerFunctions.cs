﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class PlayerFunctions
    {
        public static void Player_Disconnect(Socket sock)
        {
            //Occurs when a player disconnects from the server
            Tuple<Player, List<Player>> PlayerList = FindPlayerBySocket(sock, new List<List<Player>> { Controller.PlayersList, Controller.AwaitingList });
            if (PlayerList == null)
            {
                return;
            }
            Player pickedplayer;
            for (int i = 0; i < Controller.OnlinePlayersObserver.Count; i++)
            {
                pickedplayer = Controller.OnlinePlayersObserver.ElementAt(i);
                TCPFunctions.SendString("18 " + PlayerList.Item1.Nickname, pickedplayer.Socket);
            }
            Controller.CasualPlayersQueue.Remove(PlayerList.Item1);
            Controller.RankedPlayersQueue.Remove(PlayerList.Item1);
            if (PlayerList.Item1.Socket.Connected==true)
                Console.WriteLine("Client [" + PlayerList.Item1.Id + "] " + PlayerList.Item1.Socket.RemoteEndPoint + " disconnected");
            else
                Console.WriteLine("Client [" + PlayerList.Item1.Id + "] disconnected");
            try
            {
                sock.Shutdown(SocketShutdown.Both);
                sock.Close(); // Dont shutdown because the socket may be disposed and its disconnected anyway
            }
            catch
            {
                return;
            }
        }

        public static Tuple<Player, List<Player>> FindPlayerBySocket(Socket sock, List<List<Player>> mainlist)
        {
            int MainListCount = mainlist.Count, listcount;
            Player player;
            for (int i = 0; i < MainListCount; i++)
            {
                List<Player> list = mainlist.ElementAt(i);
                listcount = list.Count;
                for (int j = 0; j < listcount; j++)
                {
                    player = list.ElementAt(j);
                    if (player != null && player.Socket.Equals(sock))
                        return Tuple.Create(player, list);
                }
            }
            return null;
        }

        public static Match FindPlayerMatch(Player x)
        {
            Match match;
            for (int i = 0; i < Controller.MatchesList.Count; i++)
            {
                match = Controller.MatchesList.ElementAt(i);
                if (match.Players.Contains(x) == true)
                    return match;
            }
            return null;
        }

        public static string GetTopPlayers(int position)
        {
            string[] result;
            string message="";
            string playerid;
            result = Database.ExecuteReader("SELECT ID,Nickname,Mmr FROM (SELECT ROW_NUMBER() OVER(ORDER BY Mmr DESC) AS RowNum, * FROM Users) sub WHERE RowNum = " + position);
            message = result[0] + " " + result[1] + " " + result[2] + " ";
            playerid = result[0];
            result = Database.ExecuteReader("SELECT COUNT(ID) FROM Match INNER JOIN MatchPlayers ON Match.ID = MatchPlayers.MatchID WHERE Winner=" + playerid);
            message += (Convert.ToInt16(result[0]) / 2).ToString() + " ";
            result = Database.ExecuteReader("SELECT COUNT(*) FROM MatchPlayers WHERE PlayerID=" + playerid);
            message += result[0];
            return message;
        }
    }
}
