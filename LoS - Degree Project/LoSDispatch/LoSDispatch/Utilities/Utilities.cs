﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Reflection;

namespace LoSDispatch
{
    public class Utilities
    {
        public static string[] StringToArray(string parameters)
        {
            //Splits the words into array
            string[] words = parameters.Split(' ');
            for (int i = 0; i < words.Length; i++)
                if (words[i] == "")
                    words = words.Where(w => w != words[i]).ToArray();
            return words;
        }

        public static string getExternalIp()
        {
            try
            {
                string externalip = new WebClient().DownloadString("http://icanhazip.com");
                return externalip;
            }
            catch { return null; }
        }

        public static Boolean StringContainsSubstring(string fullstring, string tofindstring)
        {
            //Checks if a string contains a substring
            string[] string_array = StringToArray(fullstring);
            for (int i = 0; i < string_array.Length; i++)
                if (string_array[i].Equals(tofindstring))
                    return true;
            return false;
        }

        public static string[] ParametersToArray(string parameters)
        {
            //Splits the words into array
            string[] words = parameters.Split(' ');
            for (int i = 0; i < words.Length; i++)
                if (words[i] == "")
                    words = words.Where(w => w != words[i]).ToArray();
            return words;
        }

        public static bool IsAnumber(char a)
        {
            if (a > '0' && a < '9')
                return true;
            else
                return false;
        }

        public static string GenerateToken()
        {
            byte[] time = BitConverter.GetBytes(DateTime.UtcNow.ToBinary());
            byte[] key = Guid.NewGuid().ToByteArray();
            return Convert.ToBase64String(time.Concat(key).ToArray());
        }

        public static int MMRFormula(int opponentrating, int wins, int losses)
        {
            return (opponentrating + 400 * (wins - losses)) / (wins + losses);
        }

        public static string CurrentMethod()
        {
            StackTrace stackTrace = new StackTrace();           // get call stack
            StackFrame[] stackFrames = stackTrace.GetFrames();  // get method calls (frames)

            StackFrame callingFrame = stackFrames[1];
            MethodInfo method = (MethodInfo)callingFrame.GetMethod();
            return method.Name;
        }
    }
}
