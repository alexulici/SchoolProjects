﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class TCPFunctions
    {
        public static Socket _serverSocket; //Socket of the server
        public const int _BUFFER_SIZE = 2048; //Size of a received message
        public static readonly byte[] _buffer = new byte[_BUFFER_SIZE]; //The buffer of received message


        public static Boolean ConnectToServer(Socket socket,string ip,int port)
        {
            //Tries to connect to the server with given parameters. Returns true if it succeedes else returns false
            socket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            while (socket.Connected == false)
            {
                try
                {
                    socket.Connect(ip, port);
                }
                catch (SocketException)
                {
                    return false;
                }
            }
            return true;
        }
        public static bool SocketConnected(Socket s)
        {
            try
            {
                if (s.Poll(0, SelectMode.SelectRead) && s.Available == 0)
                    return false;
                else
                    return true;
            }
            catch (ObjectDisposedException)
            {
                return false;
            }
        }
        public static Boolean BeginReceive(Socket s)
        {
            if (SocketConnected(s) == true)
            {
                try
                {
                    s.BeginReceive(_buffer, 0, _BUFFER_SIZE, SocketFlags.None, ReceiveCallback, s);
                }
                catch
                {
                    return false;
                }
                return true;
            }
            else
                return false;
        }
        public static Boolean SendString(string message, Socket sender)
        {
            if (sender.Connected == true)
            {
                try
                {
                    message = message + "|";
                    byte[] data = Encoding.ASCII.GetBytes(message);
                    Console.WriteLine("Responding with : " + message);
                    sender.Send(data);
                }
                catch
                {
                    Console.WriteLine("Failed to deliver:" + message);
                }
                return true;
            }
            else
                return false;
        }
        public static void ReceiveCallback(IAsyncResult AR) //Getting a new message
        {
            Socket sock = (Socket)AR.AsyncState;
            int received;

            try
            {
                if (sock.Connected)
                    received = sock.EndReceive(AR);
                else
                    return;
            }
            catch (SocketException)
            {
                PlayerFunctions.Player_Disconnect(sock); //We disconnect the player form the server
                return;
            }
            byte[] recBuf = new byte[received];
            NetworkTraffic.bytecount += recBuf.Length;
            Array.Copy(_buffer, recBuf, received);
            string text = Encoding.ASCII.GetString(recBuf);
            Controller.MessageArrived(sock, text);
            BeginReceive(sock);
        }
        public static void AcceptCallback(IAsyncResult AR)
        {
            //Occurs when a new clients connects
            Socket socket;

            try
            {
                socket = _serverSocket.EndAccept(AR);
            }
            catch (ObjectDisposedException) // I cannot seem to avoid this (on exit when properly closing sockets)
            {
                return;
            }
            Controller.NewPlayerConnect(socket);
            socket.BeginReceive(_buffer, 0, _BUFFER_SIZE, SocketFlags.None, ReceiveCallback, socket);
            _serverSocket.BeginAccept(AcceptCallback, null);
        }
    }
}
