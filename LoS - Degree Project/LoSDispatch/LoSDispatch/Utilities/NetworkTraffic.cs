﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Timers;

namespace LoSDispatch
{
    class NetworkTraffic
    {
        public static long bytecount;
        static long speed;
        static Thread trackingthread;
        static Boolean track=false;
        public static void StartTracking()
        {
            if (trackingthread == null)
            {
                bytecount = 0;
                speed = SpeedTest();
                Thread trackingthread = new Thread(TrackThread);
                track = true;
                trackingthread.Start();
            }
        }

        public static void StopTracking()
        {
            if (track == true)
            {
                track = false;
            }
        }

        private static void TrackThread()
        {
            while (track == true)
            {
                Console.WriteLine(bytecount + " speed:" + speed + " " + ((bytecount*100) / (speed/8)) + "% usage");
                bytecount = 0;
                Thread.Sleep(1000);
            }
        }

        public static long SpeedTest()
        {
            //from http://stackoverflow.com/questions/1084199/how-to-create-a-download-speed-test-with-net-c

            const string tempfile = "tempfile.tmp";
            System.Net.WebClient webClient = new System.Net.WebClient();

            Console.WriteLine("Downloading file....");

            System.Diagnostics.Stopwatch sw = System.Diagnostics.Stopwatch.StartNew();
            webClient.DownloadFile("http://download.thinkbroadband.com/50MB.zip", tempfile);
            sw.Stop();

            FileInfo fileInfo = new FileInfo(tempfile);
            long speed = fileInfo.Length / sw.Elapsed.Seconds;

            Console.WriteLine("Download duration: {0}", sw.Elapsed);
            Console.WriteLine("File size: {0}", fileInfo.Length.ToString("N0"));
            Console.WriteLine("Speed: {0} bps ", speed.ToString("N0"));

            File.Delete(tempfile);
            return speed;
        }
    }
}
