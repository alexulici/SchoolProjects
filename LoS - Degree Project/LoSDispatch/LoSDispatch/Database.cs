﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class Database
    {
        private static System.Data.SqlClient.SqlConnection connection;

        public static void Initialization()
        {
            //Database constructor
            connection = new System.Data.SqlClient.SqlConnection(); //Connection to database
            connection.ConnectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=LoS;Integrated Security=True";
            connection.Open();
        }

        public static string[] ExecuteReader(string parameters)
        {
            string[] result=null;
            int counter = 0;
            SqlCommand command = new SqlCommand(parameters, connection);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                result = new string[reader.FieldCount];
                reader.Read();
                for(int i=0; i<reader.FieldCount;i++)
                {
                    result[counter] = reader[counter].ToString();
                    counter++;
                }
            }
            reader.Close();
            return result;
        }
        public static void ExecuteNonQuery(string parameters)
        {
            SqlCommand command = new SqlCommand(parameters, connection);
            command.ExecuteNonQuery();
        }
    }
}
