﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    public class Player
    {
        Socket socket;
        int id,mmr,role,state;
        string nickname,token;

        public Player()
        {
            state = -1;
            nickname = "unknown";
        }

        public Socket Socket
        {
            get
            {
                return socket;
            }

            set
            {
                socket = value;
            }
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public int Mmr
        {
            get
            {
                return mmr;
            }

            set
            {
                mmr = value;
            }
        }

        public int Role
        {
            get
            {
                return role;
            }

            set
            {
                role = value;
            }
        }

        public string Nickname
        {
            get
            {
                return nickname;
            }

            set
            {
                nickname = value;
            }
        }

        public string Token
        {
            get
            {
                return token;
            }

            set
            {
                token = value;
            }
        }

        public int State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
                Console.WriteLine("STATE:" + state);
                Controller.PlayerChangedState(this);
            }
        }

        public override string ToString()
        {
            return id + " " + mmr + " " + role + " " + nickname + " " + token;
        }
    }
}
