﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch.Domain
{
    public class QueueRange
    {
        Player player;
        int lower, higher;

        public QueueRange(int low,int high,Player p)
        {
            lower = low;
            higher = high;
            player = p;
        }

        public Player Player
        {
            get
            {
                return player;
            }

            set
            {
                player = value;
            }
        }

        public int Lower
        {
            get
            {
                return lower;
            }

            set
            {
                lower = value;
            }
        }

        public int Higher
        {
            get
            {
                return higher;
            }

            set
            {
                higher = value;
            }
        }
    }
}
