﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch.Domain
{
    class Command
    {
        public int commandID;
        public string parameters;

        public Command(int id, string p)
        {
            this.commandID = id;
            this.parameters = p;
        }

        public Command()
        {
        }

        internal void SetCommandID(int p)
        {
            this.commandID = p;
        }

        internal void SetParameters(string p)
        {
            this.parameters = p;
        }

        internal int GetCommandID()
        {
            return this.commandID;
        }
    }
}
