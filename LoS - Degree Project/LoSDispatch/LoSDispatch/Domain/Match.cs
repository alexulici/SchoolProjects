﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSDispatch
{
    class Match
    {
        int id, state, type, winnerID,duration,mmrdiff;
        List<Player> players;
        Server server;

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public List<Player> Players
        {
            get
            {
                return players;
            }

            set
            {
                players = value;
            }
        }

        public Server Server
        {
            get
            {
                return server;
            }

            set
            {
                server = value;
            }
        }

        public int State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
            }
        }

        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public int WinnerID
        {
            get
            {
                return winnerID;
            }

            set
            {
                winnerID = value;
            }
        }

        public int Duration
        {
            get
            {
                return duration;
            }

            set
            {
                duration = value;
            }
        }

        public int Mmrdiff
        {
            get
            {
                return mmrdiff;
            }

            set
            {
                mmrdiff = value;
            }
        }

        public override string ToString()
        {
            Player x;
            string message;
            message = id + " " + type + " " + winnerID;
            for(int i=0;i<players.Count;i++)
            {
                x = players.ElementAt(i);
                message = message + " " + x.ToString();
            }
            return message;
        }
    }
}
