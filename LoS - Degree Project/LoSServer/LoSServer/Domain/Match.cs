﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSServer
{
    class Match
    {
        int id, state, type, winnerID, current_turn, duration;
        List<Player> players;
        Server server;

        public Match()
        {
            players = new List<Player>();
            current_turn = 0;
        }

        public int Id
        {
            get
            {
                return id;
            }

            set
            {
                id = value;
            }
        }

        public List<Player> Players
        {
            get
            {
                return players;
            }

            set
            {
                players = value;
            }
        }

        public Server Server
        {
            get
            {
                return server;
            }

            set
            {
                server = value;
            }
        }

        public int State
        {
            get
            {
                return state;
            }

            set
            {
                state = value;
            }
        }

        public int Type
        {
            get
            {
                return type;
            }

            set
            {
                type = value;
            }
        }

        public int WinnerID
        {
            get
            {
                return winnerID;
            }

            set
            {
                winnerID = value;
            }
        }

        public int CurrentTurn
        {
            get
            {
                return current_turn;
            }

            set
            {
                current_turn = value;
            }
        }

        public int Duration
        {
            get
            {
                return duration;
            }

            set
            {
                duration = value;
            }
        }
    }
}
