﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSServer
{
    class Program
    {
        static void Main(string[] args)
        {
            //Main function
            Console.Title = "Legends of Sinjar Server";
            Controller.LoadFromFile();
            Console.WriteLine("Choose your port: ");
            Controller._PORT = Convert.ToInt16(Console.ReadLine());
            Controller.SetupServer();
            while (true)
            {
                string command = Console.ReadLine();
                LocalCommands.ExecuteLocalCommand(command);
            }
        }
    }
}
