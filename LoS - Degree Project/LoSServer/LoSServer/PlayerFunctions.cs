﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LoSServer
{
    class PlayerFunctions
    {
        public static void Player_Disconnect(Socket sock)
        {
            //Occurs when a player disconnects from the server
            Tuple<Player, List<Player>> PlayerList = FindPlayerBySocket(sock, new List<List<Player>> { Controller.PlayersList, Controller.AwaitingList });
            if (PlayerList == null)
            {
                return;
            }
            if (PlayerList.Item1.State == 1)
            {
                Match pickedmatch = FindPlayerMatch(PlayerList.Item1);
                Player pickedplayer;
                for (int i = 0; i < pickedmatch.Players.Count; i++)
                {
                    pickedplayer = pickedmatch.Players.ElementAt(i);
                    if (pickedplayer.Id != PlayerList.Item1.Id)
                        TCPFunctions.SendString("4 system Disconnected", pickedplayer.Socket);
                }
            }
            try
            {
                if (PlayerList.Item1.Socket.Connected == true)
                    Console.WriteLine("Client [" + PlayerList.Item1.Id + "] " + PlayerList.Item1.Socket.RemoteEndPoint + " disconnected");
                else
                    Console.WriteLine("Client [" + PlayerList.Item1.Id + "] disconnected");
                sock.Shutdown(SocketShutdown.Both);
                sock.Close(); // Dont shutdown because the socket may be disposed and its disconnected anyway
            }
            catch
            {
                return;
            }
        }

        public static Tuple<Player, List<Player>> FindPlayerBySocket(Socket sock, List<List<Player>> mainlist)
        {
            int MainListCount = mainlist.Count, listcount;
            Player player;
            for (int i = 0; i < MainListCount; i++)
            {
                List<Player> list = mainlist.ElementAt(i);
                listcount = list.Count;
                for (int j = 0; j < listcount; j++)
                {
                    player = list.ElementAt(j);
                    if (player != null && player.Socket != null && player.Socket.Equals(sock))
                        return Tuple.Create(player, list);
                }
            }
            return null;
        }

        public static Match FindPlayerMatch(Player x)
        {
            Match match;
            for (int i = 0; i < Controller.MatchesList.Count; i++)
            {
                match = Controller.MatchesList.ElementAt(i);
                if (match.Players.Contains(x) == true)
                    return match;
            }
            return null;
        }
    }
}
