﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace LoSServer
{
    public class Utilities
    {
        public static string[] StringToArray(string parameters)
        {
            //Splits the words into array
            string[] words = parameters.Split(' ');
            for (int i = 0; i < words.Length; i++)
                if (words[i] == "")
                    words = words.Where(w => w != words[i]).ToArray();
            return words;
        }

        public static string getExternalIp()
        {
            try
            {
                string externalip = new WebClient().DownloadString("http://icanhazip.com");
                return externalip;
            }
            catch { return null; }
        }

        public static Boolean StringContainsSubstring(string fullstring, string tofindstring)
        {
            //Checks if a string contains a substring
            string[] string_array = StringToArray(fullstring);
            for (int i = 0; i < string_array.Length; i++)
                if (string_array[i].Equals(tofindstring))
                    return true;
            return false;
        }

        public static string[] ParametersToArray(string parameters)
        {
            //Splits the words into array
            string[] words = parameters.Split(' ');
            for (int i = 0; i < words.Length; i++)
                if (words[i] == "")
                    words = words.Where(w => w != words[i]).ToArray();
            return words;
        }

        public static bool IsAnumber(char a)
        {
            if (a > '0' && a < '9')
                return true;
            else
                return false;
        }

        public static T RandomElementFromList<T>(List<T> list)
        {
            Random rnd = new Random();
            int rand = rnd.Next(0, list.Count-1);
            return (T)Convert.ChangeType(list.ElementAt(rand), typeof(T));
        }
    }
}
