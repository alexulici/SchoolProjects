﻿using LoSServer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LoSServer
{
    class LocalCommands
    {
        public delegate string LocalDelegate(string parameter);

        public static string ExecuteLocalCommand(string command)
        {
            int i = 0;
            var deList = new List<LocalDelegate> { LocalCommand0, LocalCommand1, LocalCommand2, LocalCommand3 };
            List<string> commands = new List<string> { "showclients", "exit", "serverslist", "connecttodispatch" };
            string[] words = Utilities.StringToArray(command);
            command = "";
            int wordsLength = words.Length;
            foreach (string word in words)
            {
                if (i != 0 && i < wordsLength - 1)
                    command = command + word + " ";
                if (i == wordsLength - 1)
                    command = command + word;
                i++;
            }
            i = 0;
            foreach (string com in commands)
            {
                if (com == words[0])
                    return deList[i](command);
                i++;
            }
            return "-1";
        }

        private static string LocalCommand0(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Shows online players
            Console.WriteLine("[SERVER]: ======= Online Users =======");
            foreach (Player player in Controller.PlayersList) //unsafe
            {
                if(player.Socket != null && player.Socket.Connected==true)
                {
                    Console.WriteLine("Client with id " + player.Id + "; IP " + player.Socket.RemoteEndPoint);
                }
                else
                    Console.WriteLine("Client with id " + player.Id + " might reconnect " + player.Nickname);
            }
            foreach (Player player in Controller.AwaitingList) //unsafe
            {
                if (player.Socket.Connected == true)
                    Console.WriteLine("Client with ip " + player.Socket.RemoteEndPoint + " not authenticated yet");
            }
            Console.WriteLine("[SERVER]: ======= ============ =======");
            Console.ResetColor();
            return "1";
        }

        private static string LocalCommand1(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            //Disconnects all online players and closes the sockets
            Player newPlayer = new Player();
            for(int i=0;i< Controller.PlayersList.Count; i++)
            {
                newPlayer = Controller.PlayersList.ElementAt(i);
                PlayerFunctions.Player_Disconnect(newPlayer.Socket);
            }
            for (int i = 0; i < Controller.AwaitingList.Count; i++)
            {
                newPlayer = Controller.AwaitingList.ElementAt(i);
                PlayerFunctions.Player_Disconnect(newPlayer.Socket);
            }
            Console.WriteLine("[SERVER]: All online users have been disconnected");
            TCPFunctions._serverSocket.Close();
            Console.WriteLine("[SERVER]: Server socked has been closed");
            Console.ResetColor();
            return "1";
        }

        private static string LocalCommand2(string parameters)
        {
            return "1";
        }

        private static string LocalCommand3(string parameters)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("[SERVER]: ======= ============ =======");
            Socket s = Controller.dispatch.Socket;
            Boolean result = TCPFunctions.ConnectToServer(ref s, Controller.dispatch.Ip, Controller.dispatch.Port);
            Controller.dispatch.Socket = s;
            if (result == true)
            {
                Console.WriteLine("Successfully connected to dispatch [" + Controller.dispatch.Ip + ":" + Controller.dispatch.Port + "]");
                TCPFunctions.SendString("10 " + Controller._PORT, Controller.dispatch.Socket);
                TCPFunctions.BeginReceive(Controller.dispatch.Socket);
            }
            else
                Console.WriteLine("Failed to connect to dispatch");
            Console.WriteLine("[SERVER]: ======= ============ =======");
            Console.ResetColor();
            return "1";
        }
    }
}
