﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace LoSServer
{
    class GameCore
    {
        static System.Timers.Timer QueueTimer;
        public static Player NextPlayer(Player p, Match match)
        {
            Player pickedplayer;
            for(int i=0;i<match.Players.Count;i++)
            {
                pickedplayer = match.Players.ElementAt(i);
                if(p==pickedplayer)
                {
                    if (i+1 < match.Players.Count)
                        return match.Players.ElementAt(i + 1);
                    else
                        return match.Players.ElementAt(0);
                }
            }
            return null;
        }

        public static void StartTimer()
        {
            if (QueueTimer == null)
            {
                QueueTimer = new System.Timers.Timer();
                QueueTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);
                QueueTimer.Interval = 1000;
                QueueTimer.Enabled = true;
            }
            else if (QueueTimer.Enabled == false)
            {
                QueueTimer.Enabled = true;
            }
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            Match pickedmatch;
            if (Controller.MatchesList.Count == 0)
            {
                QueueTimer.Enabled = false;
                return;
            }
            for(int i=0;i<Controller.MatchesList.Count;i++)
            {
                pickedmatch = Controller.MatchesList.ElementAt(i);
                if(pickedmatch.State == 1)
                    pickedmatch.Duration++;
            }
        }
        public static void MatchOver(Match match,Player winner)
        {
            Player p;
            match.WinnerID = winner.Id;
            for(int i=0; i<match.Players.Count;i++)
            {
                p = match.Players.ElementAt(i);
                TCPFunctions.SendString("13 " + winner.Id, p.Socket);
            }
            TCPFunctions.SendString("13 " + match.Id + " " + winner.Id + " " + match.Duration, Controller.dispatch.Socket);
        }
    }
}
