﻿using LoSServer.Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LoSServer
{
    class Commands
    {
        public delegate string MethodDelegate(Command newCommand, Player player);

        public static Command ParseCommand(string command)
        {
            Command newCommand = new Command();

            string parameters = "";
            string[] words = command.Split(' ');
            try
            {
                for (int i = 1; i <= words.Length - 1; i++)
                    parameters = parameters + " " + words[i];
                newCommand = new Command(Convert.ToInt32(words[0]), parameters.Substring(1, parameters.Length - 1));
            }
            catch
            {
                return null;
            }
            return newCommand;
        }

        public static string ExecuteCommand(string command, Player player)
        {
            Command newCommand = ParseCommand(command);
            if (newCommand == null)
                return "noreturn";
            var deList = new List<MethodDelegate> { null,null,null, _03GameState, _04GetMsg, _05PlayerInfo, _06PlayerTurn, _07GetRoll, null,
                _09CheckToken,null,_11GetMatchInfo,_12Reconnect,_13MatchOverResponse,null,_15AbandonMatch };
            if (newCommand.GetCommandID() < deList.Count)
            {
                string result;
                try
                {
                    result = deList[newCommand.GetCommandID()](newCommand, player);
                }
                catch
                {
                    result = "noreturn";
                }
                if (result.Equals("noreturn"))
                    return "noreturn";
                else
                    return newCommand.GetCommandID() + " " + result;
            }
            else
                return "noreturn";
        }

        //Interpreting commands received from client

        private static string _03GameState(Command newCommand, Player player)
        {
            Player pickedplayer;
            player.State = 1;
            Match match = PlayerFunctions.FindPlayerMatch(player);
            for (int i = 0; i < match.Players.Count; i++)
            {
                pickedplayer = match.Players.ElementAt(i);
                if (pickedplayer.State == 0)
                {
                    return "-1";
                }
            }
            for (int i = 0; i < match.Players.Count; i++)
            {
                pickedplayer = match.Players.ElementAt(i);
                TCPFunctions.SendString("3 0", pickedplayer.Socket);
                _05PlayerInfo(null, pickedplayer);
            }
            match.State = 1;
            GameCore.StartTimer();
            return "noreturn";
        }

        private static string _04GetMsg(Command newCommand, Player player)
        {
            Match pickedmatch = PlayerFunctions.FindPlayerMatch(player);
            Player pickedplayer;
            for (int i = 0; i < pickedmatch.Players.Count; i++)
            {
                pickedplayer = pickedmatch.Players.ElementAt(i);
                TCPFunctions.SendString("4 " + player.Nickname + " " + newCommand.parameters, pickedplayer.Socket);
            }
            return "noreturn";
        }

        private static string _05PlayerInfo(Command newCommand, Player player)
        {
            Match match = PlayerFunctions.FindPlayerMatch(player);
            Player p;
            TCPFunctions.SendString("5 " + player.Id + " " + player.Nickname + " " + player.Mmr + " " + player.Position, player.Socket);
            for (int i = 0; i < match.Players.Count; i++)
            {
                p = match.Players.ElementAt(i);
                if (p.Socket != player.Socket)
                {
                    TCPFunctions.SendString("5 " + p.Id + " " + p.Nickname + " " + p.Mmr + " " + p.Position, player.Socket);
                }
            }
            TCPFunctions.SendString("6 " + match.CurrentTurn, player.Socket);
            return "noreturn";
        }

        private static string _06PlayerTurn(Command newCommand, Player player)
        {
            Match match = PlayerFunctions.FindPlayerMatch(player);
            Player p;
            for (int i = 0; i < match.Players.Count; i++)
            {
                p = match.Players.ElementAt(i);
                TCPFunctions.SendString("6 " + match.CurrentTurn, p.Socket);
            }
            return "noreturn";
        }

        private static string _07GetRoll(Command newCommand, Player player)
        {
            Match match = PlayerFunctions.FindPlayerMatch(player);
            Player p, nextPlayer;
            Random rnd = new Random();
            int roll = rnd.Next(1, 6);
            if (player.Position + roll < 19)
            {
                player.Position = player.Position + roll;
            }
            else
            {
                player.Position = 19;
            }
            nextPlayer = GameCore.NextPlayer(player, match);
            match.CurrentTurn = nextPlayer.Id;
            for (int i = 0; i < match.Players.Count; i++)
            {
                p = match.Players.ElementAt(i);
                TCPFunctions.SendString("8 " + player.Id + " " + player.Position, p.Socket);
                TCPFunctions.SendString("6 " + match.CurrentTurn, p.Socket);
            }
            if (player.Position == 19)
            {
                GameCore.MatchOver(match, player);
            }
            return roll.ToString();
        }

        private static string _09CheckToken(Command newCommand, Player player)
        {
            Player x;
            for (int i = 0; i < Controller.PlayersList.Count; i++)
            {
                x = Controller.PlayersList.ElementAt(i);
                if (x.Token.Equals(newCommand.parameters))
                {
                    if (x.Socket != null && x.Socket.Connected == true)
                    {
                        PlayerFunctions.Player_Disconnect(player.Socket);
                        Console.WriteLine(x.Id + " " + x.Token + " ERROR");
                        return "0";
                    }
                    x.Socket = player.Socket;
                    Tuple<Player, List<Player>> PlayerList = PlayerFunctions.FindPlayerBySocket(player.Socket, new List<List<Player>> { Controller.AwaitingList });
                    if (PlayerList != null)
                        Controller.AwaitingList.Remove(PlayerList.Item1);
                    return "0";
                }
            }
            return "1";
        }

        private static string _11GetMatchInfo(Command newCommand, Player player)
        {
            Player newPlayer;
            string[] message = newCommand.parameters.Split(' ');
            Match newMatch = new Match();
            newMatch.Id = Convert.ToInt16(message[0]);
            newMatch.Type = Convert.ToInt16(message[1]);
            newMatch.WinnerID = Convert.ToInt16(message[2]);
            for (int i = 3; i < message.Length; i += 5)
            {
                newPlayer = new Player();
                newPlayer.Id = Convert.ToInt16(message[i]);
                newPlayer.Mmr = Convert.ToInt16(message[i + 1]);
                newPlayer.Role = Convert.ToInt16(message[i + 2]);
                newPlayer.Nickname = message[i + 3];
                newPlayer.Token = message[i + 4];
                newMatch.Players.Add(newPlayer);
                Controller.PlayersList.Add(newPlayer);
            }
            newMatch.CurrentTurn = Utilities.RandomElementFromList<Player>(newMatch.Players).Id;
            Controller.MatchesList.Add(newMatch);
            return newMatch.Id.ToString();
        }

        private static string _12Reconnect(Command newCommand, Player player)
        {
            Match match = PlayerFunctions.FindPlayerMatch(player);
            Player p;
            _05PlayerInfo(null, player);
            for (int i = 0; i < match.Players.Count; i++)
            {
                p = match.Players.ElementAt(i);
                TCPFunctions.SendString("8 " + p.Id + " " + p.Position, player.Socket);
            }
            return "noreturn";
        }

        private static string _13MatchOverResponse(Command newCommand, Player player)
        {
            string[] param = newCommand.parameters.Split(' ');
            if (param[0].Equals("0"))
            {
                Match pickedmatch;
                for (int i = 0; i < Controller.MatchesList.Count; i++)
                {
                    pickedmatch = Controller.MatchesList.ElementAt(i);
                    if (pickedmatch.Id == Convert.ToInt16(param[1]))
                    {
                        Player pickedplayer;
                        for (int j = 0; j < pickedmatch.Players.Count; j++)
                        {
                            pickedplayer = pickedmatch.Players.ElementAt(j);
                            Controller.PlayersList.Remove(pickedplayer);
                        }
                        Controller.MatchesList.Remove(pickedmatch);
                        return "noreturn";
                    }
                }
            }
            return "noreturn";
        }

        private static string _15AbandonMatch(Command newCommand, Player player)
        {
            Match pickedmatch = PlayerFunctions.FindPlayerMatch(player);
            Player winner = null;
            for (int i = 0; i < pickedmatch.Players.Count; i++)
            {
                winner = pickedmatch.Players.ElementAt(i);
                if (winner.Id != player.Id)
                {
                    TCPFunctions.SendString("4 system Abandoned the match", winner.Socket);
                    break;
                }
            }
            GameCore.MatchOver(pickedmatch, winner);
            return "noreturn";
        }
    }
}
