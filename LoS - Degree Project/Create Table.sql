use LoS
CREATE TABLE Users
(ID int PRIMARY KEY,
Username nvarchar(20),
Nickname nvarchar(20),
Password nvarchar(20),
Email nvarchar(40),
Role int,
Mmr int
);

CREATE TABLE Match
(ID int PRIMARY KEY,
Winner int,
MatchType int,
Duration int,
Mmrdiff int
);

CREATE TABLE MatchPlayers
(MatchID int NOT NULL REFERENCES Match(ID) ON DELETE CASCADE,
PlayerID int NOT NULL REFERENCES Users(ID) ON DELETE CASCADE,
PRIMARY KEY(MatchID, PlayerID)
);