﻿namespace LoSClient
{
    partial class MatchReady_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DeclineBtn = new System.Windows.Forms.PictureBox();
            this.AcceptBtn = new System.Windows.Forms.PictureBox();
            this.MessageLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DeclineBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // DeclineBtn
            // 
            this.DeclineBtn.BackColor = System.Drawing.Color.Transparent;
            this.DeclineBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.DeclineBtn.Image = global::LoSClient.Properties.Resources.DeclineBtn;
            this.DeclineBtn.Location = new System.Drawing.Point(271, 104);
            this.DeclineBtn.Name = "DeclineBtn";
            this.DeclineBtn.Size = new System.Drawing.Size(86, 29);
            this.DeclineBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.DeclineBtn.TabIndex = 4;
            this.DeclineBtn.TabStop = false;
            this.DeclineBtn.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // AcceptBtn
            // 
            this.AcceptBtn.BackColor = System.Drawing.Color.Transparent;
            this.AcceptBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.AcceptBtn.Image = global::LoSClient.Properties.Resources.AcceptBtn;
            this.AcceptBtn.Location = new System.Drawing.Point(110, 50);
            this.AcceptBtn.Name = "AcceptBtn";
            this.AcceptBtn.Size = new System.Drawing.Size(136, 36);
            this.AcceptBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.AcceptBtn.TabIndex = 3;
            this.AcceptBtn.TabStop = false;
            this.AcceptBtn.Click += new System.EventHandler(this.AcceptBtn_Click);
            // 
            // MessageLabel
            // 
            this.MessageLabel.AutoSize = true;
            this.MessageLabel.BackColor = System.Drawing.Color.Transparent;
            this.MessageLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MessageLabel.ForeColor = System.Drawing.Color.Black;
            this.MessageLabel.Location = new System.Drawing.Point(115, 19);
            this.MessageLabel.Name = "MessageLabel";
            this.MessageLabel.Size = new System.Drawing.Size(131, 17);
            this.MessageLabel.TabIndex = 11;
            this.MessageLabel.Text = "Your game is ready";
            // 
            // MatchReady_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(369, 138);
            this.Controls.Add(this.MessageLabel);
            this.Controls.Add(this.DeclineBtn);
            this.Controls.Add(this.AcceptBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MatchReady_Form";
            this.Text = "MatchReady";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MatchReady_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DeclineBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.AcceptBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox AcceptBtn;
        private System.Windows.Forms.PictureBox DeclineBtn;
        private System.Windows.Forms.Label MessageLabel;
    }
}