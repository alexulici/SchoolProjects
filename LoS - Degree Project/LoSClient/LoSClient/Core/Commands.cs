﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoSClient.Domain;

namespace LoSClient
{
    public class Commands
    {
        public delegate void MethodDelegate(string parameters);
        private Controller controller;

        public Commands(Controller controller)
        {
            this.controller = controller;
        }

        public void ExecuteCommand(Command command)
        {
            if (command != null)
            {
                var deList = new List<MethodDelegate> { controller._0RegisterResponse, controller._1LoginResponse,
                controller._2FindMatchResponse, controller.gamecore._3GameStateResponse, controller.gamecore._4GetChatMessage
            ,controller.gamecore._5GetPlayerInfo,controller.gamecore._6PlayerTurn,controller.gamecore._7GetRoll,
                controller.gamecore._8MovePlayer,controller._9TokenResponse,null,null,controller._12Reconnect
                ,controller.gamecore._13GameOver, controller._14ConfirmReady,null,controller._16LoadLadderResponse,
                controller._17LoadMatchesResponse,controller._18OnlinePlayersRecv};
                deList[command.GetCommandID()](command.GetParameters());
            }
        }

        public Command ParseCommand(string command)
        {
            Command newCommand = new Command();

            string parameters = "";
            string[] words = command.Split(' ');
            try
            {
                for (int i = 1; i <= words.Length - 1; i++)
                    parameters = parameters + " " + words[i];
                newCommand = new Command(Convert.ToInt32(words[0]), parameters.Substring(1, parameters.Length - 1));
            }
            catch
            {
                return null;
            }
            return newCommand;
        }
    }
}
