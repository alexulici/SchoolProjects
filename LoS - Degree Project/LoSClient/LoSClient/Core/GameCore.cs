﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoSClient.Domain;

namespace LoSClient
{
    public class GameCore
    {
        private Controller controller;
        public List<Player> playerslist;
        public Dictionary<Player, int> players_positions;
        public int playerid_turn;
        public int gamestate;

        public GameCore(Controller controller)
        {
            this.controller = controller;
            playerslist = new List<Player>();
            players_positions = new Dictionary<Player, int>();
        }

        public void Initialization()
        {
            controller.game_form.PlayerTurnManage(false);
        }

        public void _3GameStateResponse(string response)
        {
            if(response.Equals("-1"))
            {
                controller.Loading(true);
            }
            else
                if(response.Equals("0"))
            {
                controller.gamecore.gamestate = 1;
                controller.Loading(false);
            }
        }

        public void _4GetChatMessage(string message)
        {
            Tuple<string, string> result = GetNameFromMessage(message);
            controller.game_form.WriteInChat(result.Item1, result.Item2);
        }

        public void _5GetPlayerInfo(string message)
        {
            string[] msg = message.Split(' ');
            Player p = new Player();
            p.SetID(Convert.ToInt32(msg[0]));
            p.Setnickname(msg[1]);
            p.Setmmr(Convert.ToInt32(msg[2]));
            playerslist.Add(p);
            controller.game_form.NewPlayerFound(p.GetID());
            if (playerslist.Count == 1)
            {
                controller.game_form.ShowMyName(p.Getnickname());
            }
            players_positions.Add(p, Convert.ToInt32(msg[3]));
        }

        public void _6PlayerTurn(string message)
        {
            playerid_turn = Convert.ToInt32(message);
            Player p = FindPlayerByIDList(playerid_turn, playerslist);
            if (playerid_turn == playerslist.ElementAt(0).GetID())
            {
                controller.game_form.PlayerTurnManage(true);
            }
            else
                controller.game_form.PlayerTurnManage(false);
            controller.game_form.WriteInChat("system", p.Getnickname() + "'s turn");
        }

        public Player FindPlayerByIDList(int ID, List<Player> list)
        {
            Player p;
            for (int i = 0; i < list.Count; i++)
            {
                p = list.ElementAt(i);
                if (p.GetID() == ID)
                    return p;
            }
            return null;
        }

        public void Roll()
        {
            Command reg_cmd = new Command(7, "");
            controller.TCP_Fct.SendMessage(reg_cmd.ToString());
        }

        public void _7GetRoll(string message)
        {
            int roll = Convert.ToInt32(message);
            controller.game_form.WriteInChat("system", "You rolled " + roll);
            controller.game_form.UpdateDice(roll);
        }

        public void _8MovePlayer(string message)
        {
            string[] msg = message.Split(' ');
            Player p = FindPlayerByIDList(Convert.ToInt32(msg[0]), playerslist);
            MovePlayer(p, Convert.ToInt32(msg[1]));
        }

        public void MovePlayer(Player p, int position)
        {
            players_positions[p] = Convert.ToInt32(position);
            controller.game_form.MovePlayerTo(p.GetID(), position);
        }

        public void _13GameOver(string message)
        {
            Player pickedplayer = FindPlayerByIDList(Convert.ToInt16(message), playerslist);
            controller.BackToMainMenu = false;
            controller.TCP_Fct.CloseConnection();
            controller.TCP_Fct._SERVERIP = controller.Dispatch_IP;
            controller.TCP_Fct._SERVERPORT = controller.Dispatch_Port;
            string result = controller.TCP_Fct.ConnectToServer();
            if (!result.Equals("0"))
            {
                controller.CurrentForm.Close();
                controller.main_form.Show();
                Command sendtoken_cmd = new Command(9, controller.MyToken);
                controller.TCP_Fct.SendMessage(sendtoken_cmd.ToString());
                controller.BackToMainMenu = true;
            }
            else
            {
                controller.game_form.BackToMenu(pickedplayer.Getnickname());
                Command sendtoken_cmd = new Command(9, controller.MyToken);
                controller.TCP_Fct.SendMessage(sendtoken_cmd.ToString());
                controller.BackToMainMenu = true;
                return;
            }
        }

        public Tuple<string,string> GetNameFromMessage(string message)
        {
            string[] split = message.Split(' ');
            string name = split[0],newmsg="";
            for(int i=1;i<split.Length;i++)
            {
                newmsg += split[i] + " ";
            }
            return Tuple.Create(name, newmsg.Substring(0, newmsg.Length - 1));
        }
    }
}
