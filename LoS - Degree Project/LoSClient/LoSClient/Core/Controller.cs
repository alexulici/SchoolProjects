﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoSClient.Domain;
using System.Windows.Forms;
using System.IO;
using System.Threading;

namespace LoSClient
{
    public class Controller
    {
        #region FormVariable
        public Main_Form main_form;
        public Authentication_Form auth_form;
        public Register_Form reg_form;
        public Multiplayer_Form multi_form;
        public Game_Form game_form;
        public MatchReady_Form matchready_form;
        public Ladder_Form ladder_form;
        public GamesList_From gameslist_form;
        public OnlinePlayers onlineplayers_form;
        #endregion

        public TCPFunctions TCP_Fct;
        public Commands commands;

        public GameCore gamecore;
        public ErrorHandler errorhandler;
        private Thread RecieveThread;
        public Form CurrentForm;
        public Boolean BackToMainMenu;
        public String Dispatch_IP;
        public int Dispatch_Port;
        public string MyToken;
        public Boolean MustReconnect;
        private Boolean loading;

        public Boolean SearchingForOpponent;


        public Boolean Spectate;

        public Controller()
        {
            TCP_Fct = new TCPFunctions(this);
            errorhandler = new ErrorHandler();
            commands = new Commands(this);
            gamecore = new GameCore(this);
        }


        #region SetForms
        public void SetMainForm(Main_Form form)
        {
            this.main_form = form;
            CurrentForm = form;
        }

        public void SetAuthForm(Authentication_Form form)
        {
            this.auth_form = form;
            CurrentForm = form;
            MustReconnect = false;
            loading = false;
            BackToMainMenu = true;
            SearchingForOpponent = false;
            Spectate = false;
        }

        public void SetRegForm(Register_Form form)
        {
            this.reg_form = form;
            CurrentForm = form;
        }
        public void SetMultiForm(Multiplayer_Form form)
        {
            this.multi_form = form;
            CurrentForm = form;
        }
        public void SetGameForm(Game_Form form)
        {
            this.game_form = form;
            CurrentForm = form;
        }

        public void SetMatchReadyForm(MatchReady_Form form)
        {
            this.matchready_form = form;
        }

        public void SetLadderForm(Ladder_Form form)
        {
            this.ladder_form = form;
            CurrentForm = form;
        }

        public void SetGamesListForm(GamesList_From form)
        {
            this.gameslist_form = form;
            CurrentForm = form;
        }

        public void SetOnlinePlayersForm(OnlinePlayers form)
        {
            this.onlineplayers_form = form;
        }
        #endregion

        public void FindMatch(int matchtype)
        {
            Command findmatch = new Command(2, matchtype.ToString());
            TCP_Fct.SendMessage(findmatch.ToString());
        }

        public void _2FindMatchResponse(string message)
        {
            if(message.Equals("0"))
            {
                matchready_form.Close();
                UnlockCurrentForm();
                multi_form.OpponentDeclined();
            }
            else
                multi_form.OpponedFound();
        }

        public void _14ConfirmReady(string message)
        {
            string[] splitline = message.Split(':');
            if (splitline.Length >= 2 && splitline[0] != null && splitline[1] != null)
            {
                BackToMainMenu = false;
                TCP_Fct.CloseConnection();
                TCP_Fct._SERVERIP = splitline[0];
                TCP_Fct._SERVERPORT = Convert.ToInt32(splitline[1]);
                string result = TCP_Fct.ConnectToServer();
                if (result.Equals("0"))
                {
                    Loading(true);
                    SearchingForOpponent = false;
                    Command sendtoken_cmd = new Command(9, MyToken);
                    TCP_Fct.SendMessage(sendtoken_cmd.ToString());
                    multi_form.GameWindow();
                }
                else
                {
                    MessageBox.Show("Error: " + result);
                    TCP_Fct._SERVERIP = Dispatch_IP;
                    TCP_Fct._SERVERPORT = Dispatch_Port;
                    result = TCP_Fct.ConnectToServer();
                    if (!result.Equals("0"))
                    {
                        CurrentForm.Close();
                        main_form.Show();
                        MessageBox.Show("Server is offline");
                    }
                    else
                    {
                        Command sendtoken_cmd = new Command(9, MyToken);
                        TCP_Fct.SendMessage(sendtoken_cmd.ToString());
                        BackToMainMenu = true;
                    }
                }
            }
            else
                return;
        }

        public void ListeningThread()
        {
            while (true)
            {
                string message = TCP_Fct.GetMessage();
                if (message.Equals(""))
                {
                    break;
                }
                string[] splitcommands = message.Split('|');
                for (int i = 0; i < splitcommands.Length - 1; i++)
                {
                    Command command = commands.ParseCommand(splitcommands[i]);
                    commands.ExecuteCommand(command);
                }
            }
        }

        internal void LoadLadder()
        {
            TCP_Fct.SendMessage("16 ");
        }

        internal void OnlinePlayersObserver(int mode)
        {
            TCP_Fct.SendMessage("18 " + mode);
        }

        internal void _18OnlinePlayersRecv(string parameters)
        {
            onlineplayers_form.NewUser(parameters);
        }

        internal void LoadMatches(int mode)
        {
            TCP_Fct.SendMessage("17 " + mode);
        }

        internal void _16LoadLadderResponse(string response)
        {
            ladder_form.AddPlayerToLadder(response);
        }

        internal void _17LoadMatchesResponse(string response)
        {
            gameslist_form.AddMatch(response);
        }

        internal void AbandonMatch()
        {
            TCP_Fct.SendMessage("15 ");
        }

        internal void SendMsg(string text)
        {
            TCP_Fct.SendMessage("4 " + text);
        }

        public void StartListening()
        {
            RecieveThread = new Thread(ListeningThread);
            RecieveThread.IsBackground = true;
            RecieveThread.Start();
        }

        public void DisconnectedFromServer()
        {
            TCP_Fct.CloseConnection();
            if (BackToMainMenu == true)
            {
                CurrentForm.Close();
                main_form.Show();
                MessageBox.Show("You have been disconnected from the server");
            }
        }

        public void LockCurrentForm()
        {
            CurrentForm.Enabled = false;
        }

        public void UnlockCurrentForm()
        {
            CurrentForm.Enabled = true;
        }

        public void Login(string username, string password)
        {
            username = username.ToLower();
            password = password.ToLower();
            Command log_cmd = new Command(1, username + " " + password);
            TCP_Fct.SendMessage(log_cmd.ToString());
        }

        public void _1LoginResponse(string response)
        {
            auth_form.LogResponse(errorhandler.LoginHandle(response));
        }

        public void Register(string username, string nickname, string password, string confpassword, string email)
        {
            username = username.ToLower();
            nickname = nickname.ToLower();
            password = password.ToLower();
            confpassword = confpassword.ToLower();
            email = email.ToLower();
            string result = errorhandler.RegisterValidate(username, nickname, password, confpassword, email);
            if (!result.Equals("0"))
            {
                _0RegisterResponse(result);
                return;
            }
            Command reg_cmd = new Command(0, username + " " + nickname + " " + password + " " + email);
            TCP_Fct.SendMessage(reg_cmd.ToString());
        }

        public void _0RegisterResponse(string response)
        {
            reg_form.RegisterCheck(errorhandler.RegisterHandle(response));
        }

        public void _9TokenResponse(string response)
        {
            if (errorhandler.TokenHandle(response).Equals("OK"))
            {
                if (gamecore.gamestate == 0)
                {
                    if (MustReconnect == false)
                    {
                        Command reg_cmd = new Command(3, "");
                        TCP_Fct.SendMessage(reg_cmd.ToString());
                    }
                    else
                    {
                        Command reg_cmd = new Command(12, "");
                        TCP_Fct.SendMessage(reg_cmd.ToString());
                        gamecore.gamestate = 1;
                        MustReconnect = false;
                        Loading(false);
                    }
                    return;
                }
                else if(loading==true)
                {
                    Command reg_cmd = new Command(3, "");
                    TCP_Fct.SendMessage(reg_cmd.ToString());
                }
            }
            else if(errorhandler.TokenHandle(response).Equals("2"))
            {
                Command sendtoken_cmd = new Command(9, MyToken);
                TCP_Fct.SendMessage(sendtoken_cmd.ToString());
            }
            else
            {
                BackToMainMenu = true;
                DisconnectedFromServer();
            }
        }

        public void _12Reconnect(string response)
        {
            MustReconnect = true;
        }

        public void Loading(Boolean load)
        {
            loading = load;
            if(game_form!=null)
                game_form.LoadingCheck(loading);
        }

        public string LoadFromFile()
        {
            if (System.IO.File.Exists(Application.StartupPath + "/Config.txt"))
            {
                using (StreamReader reader = new StreamReader("Config.txt"))
                {
                    string line = reader.ReadLine();
                    if(line == null)
                        return "Configuration file is empty";
                    string[] splitline = line.Split(':');
                    if (splitline.Length >= 2 && splitline[0] != null && splitline[1] != null)
                    {
                        TCP_Fct._SERVERIP = splitline[0];
                        TCP_Fct._SERVERPORT = Convert.ToInt32(splitline[1]);
                        Dispatch_IP = TCP_Fct._SERVERIP;
                        Dispatch_Port = TCP_Fct._SERVERPORT;
                        return "0";
                    }
                    else
                        return "Incorrect ip format. Make sure you are using the following format IP:Port. For instance 127.0.0.1:8000.";
                }
            }
            else
                return "Configuration file not found";
        }
    }
}
