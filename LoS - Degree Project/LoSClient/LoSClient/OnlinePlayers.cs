﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class OnlinePlayers : Form
    {
        private Controller controller;

        public OnlinePlayers(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.SetOnlinePlayersForm(this);
        }

        private void OnlinePlayers_Load(object sender, EventArgs e)
        {
            controller.OnlinePlayersObserver(1);
        }

        public void NewUser(string playername)
        {
            if (listBox1.Items.Contains(playername))
                listBox1.Items.Remove(playername);
            else
                listBox1.Items.Add(playername);
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            controller.OnlinePlayersObserver(0);
            this.Close();
        }
    }
}
