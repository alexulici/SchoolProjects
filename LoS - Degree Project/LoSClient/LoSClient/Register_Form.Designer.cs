﻿namespace LoSClient
{
    partial class Register_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsernameTxt = new System.Windows.Forms.TextBox();
            this.NicknameTxt = new System.Windows.Forms.TextBox();
            this.PassTxt = new System.Windows.Forms.TextBox();
            this.ConfPassTxt = new System.Windows.Forms.TextBox();
            this.EmailTxt = new System.Windows.Forms.TextBox();
            this.RegisterBtn = new System.Windows.Forms.PictureBox();
            this.CancelBtn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RegisterBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // UsernameTxt
            // 
            this.UsernameTxt.BackColor = System.Drawing.SystemColors.Window;
            this.UsernameTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.UsernameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.UsernameTxt.Location = new System.Drawing.Point(318, 123);
            this.UsernameTxt.Name = "UsernameTxt";
            this.UsernameTxt.Size = new System.Drawing.Size(172, 16);
            this.UsernameTxt.TabIndex = 1;
            // 
            // NicknameTxt
            // 
            this.NicknameTxt.BackColor = System.Drawing.SystemColors.Window;
            this.NicknameTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.NicknameTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.NicknameTxt.Location = new System.Drawing.Point(318, 166);
            this.NicknameTxt.Name = "NicknameTxt";
            this.NicknameTxt.Size = new System.Drawing.Size(172, 16);
            this.NicknameTxt.TabIndex = 2;
            // 
            // PassTxt
            // 
            this.PassTxt.BackColor = System.Drawing.SystemColors.Window;
            this.PassTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.PassTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PassTxt.Location = new System.Drawing.Point(318, 209);
            this.PassTxt.Name = "PassTxt";
            this.PassTxt.Size = new System.Drawing.Size(172, 16);
            this.PassTxt.TabIndex = 3;
            this.PassTxt.UseSystemPasswordChar = true;
            // 
            // ConfPassTxt
            // 
            this.ConfPassTxt.BackColor = System.Drawing.SystemColors.Window;
            this.ConfPassTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ConfPassTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfPassTxt.Location = new System.Drawing.Point(318, 250);
            this.ConfPassTxt.Name = "ConfPassTxt";
            this.ConfPassTxt.Size = new System.Drawing.Size(172, 16);
            this.ConfPassTxt.TabIndex = 4;
            this.ConfPassTxt.UseSystemPasswordChar = true;
            // 
            // EmailTxt
            // 
            this.EmailTxt.BackColor = System.Drawing.SystemColors.Window;
            this.EmailTxt.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.EmailTxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EmailTxt.Location = new System.Drawing.Point(318, 296);
            this.EmailTxt.Name = "EmailTxt";
            this.EmailTxt.Size = new System.Drawing.Size(172, 16);
            this.EmailTxt.TabIndex = 5;
            // 
            // RegisterBtn
            // 
            this.RegisterBtn.BackColor = System.Drawing.Color.Transparent;
            this.RegisterBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RegisterBtn.Image = global::LoSClient.Properties.Resources.RegisterBtn;
            this.RegisterBtn.Location = new System.Drawing.Point(569, 356);
            this.RegisterBtn.Name = "RegisterBtn";
            this.RegisterBtn.Size = new System.Drawing.Size(119, 32);
            this.RegisterBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RegisterBtn.TabIndex = 6;
            this.RegisterBtn.TabStop = false;
            this.RegisterBtn.Click += new System.EventHandler(this.RegisterBtn_Click);
            // 
            // CancelBtn
            // 
            this.CancelBtn.BackColor = System.Drawing.Color.Transparent;
            this.CancelBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CancelBtn.Image = global::LoSClient.Properties.Resources.CancelBtn;
            this.CancelBtn.Location = new System.Drawing.Point(123, 356);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(119, 32);
            this.CancelBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CancelBtn.TabIndex = 7;
            this.CancelBtn.TabStop = false;
            this.CancelBtn.Click += new System.EventHandler(this.CancelBtn_Click);
            // 
            // Register_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LoSClient.Properties.Resources.RegisterBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.RegisterBtn);
            this.Controls.Add(this.EmailTxt);
            this.Controls.Add(this.ConfPassTxt);
            this.Controls.Add(this.PassTxt);
            this.Controls.Add(this.NicknameTxt);
            this.Controls.Add(this.UsernameTxt);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Register_Form";
            this.Text = "Register_Form";
            this.Load += new System.EventHandler(this.Register_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RegisterBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CancelBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox UsernameTxt;
        private System.Windows.Forms.TextBox NicknameTxt;
        private System.Windows.Forms.TextBox PassTxt;
        private System.Windows.Forms.TextBox ConfPassTxt;
        private System.Windows.Forms.TextBox EmailTxt;
        private System.Windows.Forms.PictureBox RegisterBtn;
        private System.Windows.Forms.PictureBox CancelBtn;
    }
}