﻿namespace LoSClient
{
    partial class Multiplayer_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.RankedBtn = new System.Windows.Forms.PictureBox();
            this.CasualBtn = new System.Windows.Forms.PictureBox();
            this.FindMatchBtn = new System.Windows.Forms.PictureBox();
            this.FriendsBtn = new System.Windows.Forms.PictureBox();
            this.WatchLiveBtn = new System.Windows.Forms.PictureBox();
            this.LadderBtn = new System.Windows.Forms.PictureBox();
            this.GamesListBtn = new System.Windows.Forms.PictureBox();
            this.BackBtn = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.RankedBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CasualBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FindMatchBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.WatchLiveBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LadderBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GamesListBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // RankedBtn
            // 
            this.RankedBtn.BackColor = System.Drawing.Color.Transparent;
            this.RankedBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RankedBtn.Image = global::LoSClient.Properties.Resources.RankedBtn;
            this.RankedBtn.Location = new System.Drawing.Point(173, 26);
            this.RankedBtn.Name = "RankedBtn";
            this.RankedBtn.Size = new System.Drawing.Size(127, 34);
            this.RankedBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RankedBtn.TabIndex = 7;
            this.RankedBtn.TabStop = false;
            this.RankedBtn.Visible = false;
            this.RankedBtn.Click += new System.EventHandler(this.RankedBtn_Click);
            // 
            // CasualBtn
            // 
            this.CasualBtn.BackColor = System.Drawing.Color.Transparent;
            this.CasualBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CasualBtn.Image = global::LoSClient.Properties.Resources.CasualBtn;
            this.CasualBtn.Location = new System.Drawing.Point(470, 26);
            this.CasualBtn.Name = "CasualBtn";
            this.CasualBtn.Size = new System.Drawing.Size(127, 34);
            this.CasualBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CasualBtn.TabIndex = 6;
            this.CasualBtn.TabStop = false;
            this.CasualBtn.Visible = false;
            this.CasualBtn.Click += new System.EventHandler(this.CasualBtn_Click);
            // 
            // FindMatchBtn
            // 
            this.FindMatchBtn.BackColor = System.Drawing.Color.Transparent;
            this.FindMatchBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.FindMatchBtn.Image = global::LoSClient.Properties.Resources.FindMatchBtn;
            this.FindMatchBtn.Location = new System.Drawing.Point(321, 26);
            this.FindMatchBtn.Name = "FindMatchBtn";
            this.FindMatchBtn.Size = new System.Drawing.Size(127, 34);
            this.FindMatchBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FindMatchBtn.TabIndex = 5;
            this.FindMatchBtn.TabStop = false;
            this.FindMatchBtn.Click += new System.EventHandler(this.FindMatchBtn_Click);
            // 
            // FriendsBtn
            // 
            this.FriendsBtn.BackColor = System.Drawing.Color.Transparent;
            this.FriendsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.FriendsBtn.Image = global::LoSClient.Properties.Resources.OnlinePlayers;
            this.FriendsBtn.Location = new System.Drawing.Point(629, 226);
            this.FriendsBtn.Name = "FriendsBtn";
            this.FriendsBtn.Size = new System.Drawing.Size(136, 36);
            this.FriendsBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.FriendsBtn.TabIndex = 4;
            this.FriendsBtn.TabStop = false;
            this.FriendsBtn.Click += new System.EventHandler(this.FriendsBtn_Click);
            // 
            // WatchLiveBtn
            // 
            this.WatchLiveBtn.BackColor = System.Drawing.Color.Transparent;
            this.WatchLiveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.WatchLiveBtn.Image = global::LoSClient.Properties.Resources.WatchLiveBtn;
            this.WatchLiveBtn.Location = new System.Drawing.Point(629, 268);
            this.WatchLiveBtn.Name = "WatchLiveBtn";
            this.WatchLiveBtn.Size = new System.Drawing.Size(136, 36);
            this.WatchLiveBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.WatchLiveBtn.TabIndex = 3;
            this.WatchLiveBtn.TabStop = false;
            this.WatchLiveBtn.Click += new System.EventHandler(this.WatchLiveBtn_Click);
            // 
            // LadderBtn
            // 
            this.LadderBtn.BackColor = System.Drawing.Color.Transparent;
            this.LadderBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.LadderBtn.Image = global::LoSClient.Properties.Resources.LadderBtn;
            this.LadderBtn.Location = new System.Drawing.Point(629, 310);
            this.LadderBtn.Name = "LadderBtn";
            this.LadderBtn.Size = new System.Drawing.Size(136, 36);
            this.LadderBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LadderBtn.TabIndex = 2;
            this.LadderBtn.TabStop = false;
            this.LadderBtn.Click += new System.EventHandler(this.LadderBtn_Click);
            // 
            // GamesListBtn
            // 
            this.GamesListBtn.BackColor = System.Drawing.Color.Transparent;
            this.GamesListBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.GamesListBtn.Image = global::LoSClient.Properties.Resources.GamesListBtn;
            this.GamesListBtn.Location = new System.Drawing.Point(629, 352);
            this.GamesListBtn.Name = "GamesListBtn";
            this.GamesListBtn.Size = new System.Drawing.Size(136, 36);
            this.GamesListBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.GamesListBtn.TabIndex = 1;
            this.GamesListBtn.TabStop = false;
            this.GamesListBtn.Click += new System.EventHandler(this.GamesListBtn_Click);
            // 
            // BackBtn
            // 
            this.BackBtn.BackColor = System.Drawing.Color.Transparent;
            this.BackBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.BackBtn.Image = global::LoSClient.Properties.Resources.BackBtn;
            this.BackBtn.Location = new System.Drawing.Point(12, 352);
            this.BackBtn.Name = "BackBtn";
            this.BackBtn.Size = new System.Drawing.Size(136, 36);
            this.BackBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.BackBtn.TabIndex = 10;
            this.BackBtn.TabStop = false;
            this.BackBtn.Click += new System.EventHandler(this.BackBtn_Click);
            // 
            // Multiplayer_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LoSClient.Properties.Resources.MultiplayerBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.BackBtn);
            this.Controls.Add(this.RankedBtn);
            this.Controls.Add(this.CasualBtn);
            this.Controls.Add(this.FindMatchBtn);
            this.Controls.Add(this.FriendsBtn);
            this.Controls.Add(this.WatchLiveBtn);
            this.Controls.Add(this.LadderBtn);
            this.Controls.Add(this.GamesListBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Multiplayer_Form";
            this.Text = "Multiplayer_Form";
            this.Load += new System.EventHandler(this.Multiplayer_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.RankedBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CasualBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FindMatchBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.FriendsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.WatchLiveBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LadderBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GamesListBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.BackBtn)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox GamesListBtn;
        private System.Windows.Forms.PictureBox LadderBtn;
        private System.Windows.Forms.PictureBox WatchLiveBtn;
        private System.Windows.Forms.PictureBox FriendsBtn;
        private System.Windows.Forms.PictureBox FindMatchBtn;
        private System.Windows.Forms.PictureBox CasualBtn;
        private System.Windows.Forms.PictureBox RankedBtn;
        private System.Windows.Forms.PictureBox BackBtn;
    }
}