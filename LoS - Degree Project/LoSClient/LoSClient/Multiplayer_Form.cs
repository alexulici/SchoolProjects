﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using LoSClient.Domain;

namespace LoSClient
{
    public partial class Multiplayer_Form : Form
    {
        private Controller controller;

        public Multiplayer_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.SetMultiForm(this);
        }

        private void Multiplayer_Form_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            if (controller.MustReconnect == true)
                FindMatchBtn.Image = LoSClient.Properties.Resources.ReconnectBtn;
            else
                if(controller.SearchingForOpponent==true)
            {
                FindMatchBtn.Image = LoSClient.Properties.Resources.SearchingOpponentBtn;
            }
        }

        private void CasualBtn_Click(object sender, EventArgs e)
        {
            FindMatch(0);
        }

        public void OpponedFound()
        {
            Invoke((MethodInvoker)delegate ()
            {
                MatchReady_Form matchready = new MatchReady_Form(controller);
                matchready.Show();
            });
        }

        public void GameWindow()
        {
            Invoke((MethodInvoker)delegate ()
            {
                Game_Form game_form = new Game_Form(controller);
                game_form.Show();
            });
            this.Close();
        }

        public void OpponentDeclined()
        {
            controller.SearchingForOpponent = false;
            FindMatchBtn.Visible = true;
            CasualBtn.Visible = false;
            RankedBtn.Visible = false;
            FindMatchBtn.Image = LoSClient.Properties.Resources.FindMatchBtn;
        }

        private void FindMatchBtn_Click(object sender, EventArgs e)
        {
            if (controller.SearchingForOpponent == false)
            {
                if (controller.MustReconnect == true)
                    controller.FindMatch(3);
                else
                {
                    FindMatchBtn.Visible = false;
                    CasualBtn.Visible = true;
                    RankedBtn.Visible = true;
                }
            }
            else
            {
                controller.FindMatch(4);
                FindMatchBtn.Image = LoSClient.Properties.Resources.FindMatchBtn;
                controller.SearchingForOpponent = false;
            }
        }

        private void FindMatch(int type)
        {
            FindMatchBtn.Image = LoSClient.Properties.Resources.SearchingOpponentBtn;
            CasualBtn.Visible = false;
            RankedBtn.Visible = false;
            FindMatchBtn.Visible = true;
            controller.SearchingForOpponent = true;
            controller.FindMatch(type);
        }

        private void LadderBtn_Click(object sender, EventArgs e)
        {
            Ladder_Form ladd_form = new Ladder_Form(controller);
            ladd_form.Show();
            this.Close();
        }

        private void RankedBtn_Click(object sender, EventArgs e)
        {
            FindMatch(1);
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            controller.TCP_Fct.CloseConnection();
        }

        private void WatchLiveBtn_Click(object sender, EventArgs e)
        {
            GamesList_From gameslist_form = new GamesList_From(controller,0);
            gameslist_form.Show();
            this.Close();
        }

        private void GamesListBtn_Click(object sender, EventArgs e)
        {
            GamesList_From gameslist_form = new GamesList_From(controller, 1);
            gameslist_form.Show();
            this.Close();
        }

        private void FriendsBtn_Click(object sender, EventArgs e)
        {
            OnlinePlayers onlineplayers_form = new OnlinePlayers(controller);
            onlineplayers_form.Show();
        }
    }
}
