﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class Register_Form : Form
    {
        private Controller controller;

        public Register_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.SetRegForm(this);
        }

        private void Register_Form_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void RegisterBtn_Click(object sender, EventArgs e)
        {
            controller.Register(UsernameTxt.Text, NicknameTxt.Text, PassTxt.Text, ConfPassTxt.Text, EmailTxt.Text);
        }

        public void RegisterCheck(string message)
        {
            if (message.Equals("OK"))
                MessageBox.Show("Registration Successful");
            else
                MessageBox.Show("Registration failed: " + message);
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            Authentication_Form auth_form = new Authentication_Form(controller);
            auth_form.Show();
            this.Close();
        }
    }
}
