﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class Game_Form : Form
    {
        private Controller controller;
        List<PictureBox> grid = new List<PictureBox>();
        Dictionary<int, PictureBox> players = new Dictionary<int, PictureBox>();
        public Bitmap[] dice_bmp;

        public Game_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.gamecore = new GameCore(controller);
            controller.SetGameForm(this);
        }

        private void Game_Form_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            Init();
            controller.gamecore.Initialization();
        }

        internal void Init()
        {
            #region GridList
            grid.Add(Title1);
            grid.Add(Title2);
            grid.Add(Title3);
            grid.Add(Title4);
            grid.Add(Title5);
            grid.Add(Title6);
            grid.Add(Title7);
            grid.Add(Title8);
            grid.Add(Title9);
            grid.Add(Title10);
            grid.Add(Title11);
            grid.Add(Title12);
            grid.Add(Title13);
            grid.Add(Title14);
            grid.Add(Title15);
            grid.Add(Title16);
            grid.Add(Title17);
            grid.Add(Title18);
            grid.Add(Title19);
            grid.Add(Title20);
            #endregion

            #region DiceImages
            dice_bmp = new Bitmap[7];
            dice_bmp[1] = LoSClient.Properties.Resources.Dice1;
            dice_bmp[2] = LoSClient.Properties.Resources.Dice2;
            dice_bmp[3] = LoSClient.Properties.Resources.Dice3;
            dice_bmp[4] = LoSClient.Properties.Resources.Dice4;
            dice_bmp[5] = LoSClient.Properties.Resources.Dice5;
            dice_bmp[6] = LoSClient.Properties.Resources.Dice6;
            #endregion

        }

        public void WriteInChat(string name, string message)
        {
            if(ChatScreen.Text.Equals(""))
                ChatScreen.Text = "[" + name + "]: " + message;
            else
                ChatScreen.Text = ChatScreen.Text + Environment.NewLine + "[" + name + "]: " + message;
        }

        public void PlayerTurnManage(Boolean myturn)
        {
            if (myturn == true)
            {
                PlayerTurn.Visible = true;
                Dice.Visible = true;
            }
            else
            {
                PlayerTurn.Visible = false;
                Dice.Visible = false;
            }
        }

        public void ShowMyName(string name)
        {
            PlayerNameLabel.Text = name;
        }

        public void NewPlayerFound(int playerID)
        {
            Invoke((MethodInvoker)delegate ()
            {
                PictureBox picture = new PictureBox
                {
                    Name = playerID.ToString(),
                    Size = new Size(47, 43),
                    Location = new Point(Title1.Location.X, Title1.Location.Y),
                    BorderStyle = BorderStyle.None,
                    SizeMode = PictureBoxSizeMode.Zoom,
                    BackColor = Color.Transparent,
                    Image = LoSClient.Properties.Resources.Character1,
                };
                Title1.Parent = picture;
                players.Add(playerID, picture);
                Controls.Add(picture);
            });
        }

        private void Dice_Click(object sender, EventArgs e)
        {
            controller.gamecore.Roll();
        }

        public void UpdateDice(int roll)
        {
            Dice.Image = dice_bmp[roll];
        }

        public void MovePlayerTo(int playerID,int position)
        {
            for (int i = 0; i < players.Count; i++)
            {
                if (players.ElementAt(i).Key == playerID)
                {
                    players.ElementAt(i).Value.Location = new Point(grid.ElementAt(position).Location.X, grid.ElementAt(position).Location.Y);
                    grid.ElementAt(position).Parent = players.ElementAt(i).Value;
                    break;
                }
            }
        }


        protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
        {
            if (keyData == Keys.Enter && ChatTxt.Text != "")
            {
                controller.SendMsg(ChatTxt.Text);
                ChatTxt.Text = "";
            }
            return base.ProcessCmdKey(ref msg, keyData);
        }

        private void LeaveBtn_Click(object sender, EventArgs e)
        {
            controller.AbandonMatch();
        }

        public void BackToMenu(string winner)
        {
            MessageBox.Show(winner + " won !");
            Invoke((MethodInvoker)delegate ()
            {
                Multiplayer_Form multiform = new Multiplayer_Form(controller);
                multiform.Show();
            });
            this.Close();
        }

        public void LoadingCheck(Boolean loading)
        {
            if (loading == true)
                Loadinglabel.Visible = true;
            else
                Loadinglabel.Visible = false;
        }

        private void ChatTxt_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                e.Handled = true;
                e.SuppressKeyPress = true;
            }
        }

        private void Game_Form_FormClosed(object sender, FormClosedEventArgs e)
        {
        }
    }
}
