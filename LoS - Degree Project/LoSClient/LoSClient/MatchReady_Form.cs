﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class MatchReady_Form : Form
    {
        private Controller controller;

        public MatchReady_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.SetMatchReadyForm(this);
        }

        private void AcceptBtn_Click(object sender, EventArgs e)
        {
            controller.TCP_Fct.SendMessage("14 1");
            this.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            controller.UnlockCurrentForm();
            controller.multi_form.OpponentDeclined();
            controller.TCP_Fct.SendMessage("14 0");
            this.Close();
        }

        private void MatchReady_Form_Load(object sender, EventArgs e)
        {
            controller.LockCurrentForm();
        }
    }
}
