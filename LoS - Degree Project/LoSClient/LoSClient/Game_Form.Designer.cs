﻿namespace LoSClient
{
    partial class Game_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChatScreen = new System.Windows.Forms.RichTextBox();
            this.ChatTxt = new System.Windows.Forms.TextBox();
            this.Title20 = new System.Windows.Forms.PictureBox();
            this.Title19 = new System.Windows.Forms.PictureBox();
            this.Title18 = new System.Windows.Forms.PictureBox();
            this.Title17 = new System.Windows.Forms.PictureBox();
            this.Title16 = new System.Windows.Forms.PictureBox();
            this.Title15 = new System.Windows.Forms.PictureBox();
            this.Title14 = new System.Windows.Forms.PictureBox();
            this.Title13 = new System.Windows.Forms.PictureBox();
            this.Title12 = new System.Windows.Forms.PictureBox();
            this.Title11 = new System.Windows.Forms.PictureBox();
            this.Title10 = new System.Windows.Forms.PictureBox();
            this.Title9 = new System.Windows.Forms.PictureBox();
            this.Title8 = new System.Windows.Forms.PictureBox();
            this.Title7 = new System.Windows.Forms.PictureBox();
            this.Title6 = new System.Windows.Forms.PictureBox();
            this.Title5 = new System.Windows.Forms.PictureBox();
            this.Title4 = new System.Windows.Forms.PictureBox();
            this.Title3 = new System.Windows.Forms.PictureBox();
            this.Title2 = new System.Windows.Forms.PictureBox();
            this.Title1 = new System.Windows.Forms.PictureBox();
            this.Dice = new System.Windows.Forms.PictureBox();
            this.PlayerTurn = new System.Windows.Forms.PictureBox();
            this.LeaveBtn = new System.Windows.Forms.PictureBox();
            this.PlayerNameLabel = new System.Windows.Forms.Label();
            this.Loadinglabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.Title20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerTurn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeaveBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // ChatScreen
            // 
            this.ChatScreen.Location = new System.Drawing.Point(498, 178);
            this.ChatScreen.Name = "ChatScreen";
            this.ChatScreen.Size = new System.Drawing.Size(275, 122);
            this.ChatScreen.TabIndex = 16;
            this.ChatScreen.Text = "";
            // 
            // ChatTxt
            // 
            this.ChatTxt.Location = new System.Drawing.Point(587, 306);
            this.ChatTxt.Name = "ChatTxt";
            this.ChatTxt.Size = new System.Drawing.Size(186, 20);
            this.ChatTxt.TabIndex = 17;
            this.ChatTxt.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChatTxt_KeyDown);
            // 
            // Title20
            // 
            this.Title20.BackColor = System.Drawing.Color.Transparent;
            this.Title20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title20.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title20.Location = new System.Drawing.Point(253, 156);
            this.Title20.Name = "Title20";
            this.Title20.Size = new System.Drawing.Size(69, 58);
            this.Title20.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title20.TabIndex = 25;
            this.Title20.TabStop = false;
            // 
            // Title19
            // 
            this.Title19.BackColor = System.Drawing.Color.Transparent;
            this.Title19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title19.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title19.Location = new System.Drawing.Point(185, 156);
            this.Title19.Name = "Title19";
            this.Title19.Size = new System.Drawing.Size(69, 58);
            this.Title19.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title19.TabIndex = 24;
            this.Title19.TabStop = false;
            // 
            // Title18
            // 
            this.Title18.BackColor = System.Drawing.Color.Transparent;
            this.Title18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title18.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title18.Location = new System.Drawing.Point(185, 212);
            this.Title18.Name = "Title18";
            this.Title18.Size = new System.Drawing.Size(69, 58);
            this.Title18.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title18.TabIndex = 23;
            this.Title18.TabStop = false;
            // 
            // Title17
            // 
            this.Title17.BackColor = System.Drawing.Color.Transparent;
            this.Title17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title17.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title17.Location = new System.Drawing.Point(185, 268);
            this.Title17.Name = "Title17";
            this.Title17.Size = new System.Drawing.Size(69, 58);
            this.Title17.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title17.TabIndex = 22;
            this.Title17.TabStop = false;
            // 
            // Title16
            // 
            this.Title16.BackColor = System.Drawing.Color.Transparent;
            this.Title16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title16.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title16.Location = new System.Drawing.Point(253, 268);
            this.Title16.Name = "Title16";
            this.Title16.Size = new System.Drawing.Size(69, 58);
            this.Title16.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title16.TabIndex = 21;
            this.Title16.TabStop = false;
            // 
            // Title15
            // 
            this.Title15.BackColor = System.Drawing.Color.Transparent;
            this.Title15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title15.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title15.Location = new System.Drawing.Point(322, 268);
            this.Title15.Name = "Title15";
            this.Title15.Size = new System.Drawing.Size(69, 58);
            this.Title15.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title15.TabIndex = 20;
            this.Title15.TabStop = false;
            // 
            // Title14
            // 
            this.Title14.BackColor = System.Drawing.Color.Transparent;
            this.Title14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title14.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title14.Location = new System.Drawing.Point(391, 268);
            this.Title14.Name = "Title14";
            this.Title14.Size = new System.Drawing.Size(69, 58);
            this.Title14.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title14.TabIndex = 19;
            this.Title14.TabStop = false;
            // 
            // Title13
            // 
            this.Title13.BackColor = System.Drawing.Color.Transparent;
            this.Title13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title13.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title13.Location = new System.Drawing.Point(391, 212);
            this.Title13.Name = "Title13";
            this.Title13.Size = new System.Drawing.Size(69, 58);
            this.Title13.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title13.TabIndex = 18;
            this.Title13.TabStop = false;
            // 
            // Title12
            // 
            this.Title12.BackColor = System.Drawing.Color.Transparent;
            this.Title12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title12.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title12.Location = new System.Drawing.Point(391, 156);
            this.Title12.Name = "Title12";
            this.Title12.Size = new System.Drawing.Size(69, 58);
            this.Title12.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title12.TabIndex = 15;
            this.Title12.TabStop = false;
            // 
            // Title11
            // 
            this.Title11.BackColor = System.Drawing.Color.Transparent;
            this.Title11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title11.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title11.Location = new System.Drawing.Point(391, 99);
            this.Title11.Name = "Title11";
            this.Title11.Size = new System.Drawing.Size(69, 58);
            this.Title11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title11.TabIndex = 14;
            this.Title11.TabStop = false;
            // 
            // Title10
            // 
            this.Title10.BackColor = System.Drawing.Color.Transparent;
            this.Title10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title10.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title10.Location = new System.Drawing.Point(391, 42);
            this.Title10.Name = "Title10";
            this.Title10.Size = new System.Drawing.Size(69, 58);
            this.Title10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title10.TabIndex = 13;
            this.Title10.TabStop = false;
            // 
            // Title9
            // 
            this.Title9.BackColor = System.Drawing.Color.Transparent;
            this.Title9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title9.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title9.Location = new System.Drawing.Point(322, 42);
            this.Title9.Name = "Title9";
            this.Title9.Size = new System.Drawing.Size(69, 58);
            this.Title9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title9.TabIndex = 12;
            this.Title9.TabStop = false;
            // 
            // Title8
            // 
            this.Title8.BackColor = System.Drawing.Color.Transparent;
            this.Title8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title8.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title8.Location = new System.Drawing.Point(253, 42);
            this.Title8.Name = "Title8";
            this.Title8.Size = new System.Drawing.Size(69, 58);
            this.Title8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title8.TabIndex = 11;
            this.Title8.TabStop = false;
            // 
            // Title7
            // 
            this.Title7.BackColor = System.Drawing.Color.Transparent;
            this.Title7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title7.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title7.Location = new System.Drawing.Point(185, 42);
            this.Title7.Name = "Title7";
            this.Title7.Size = new System.Drawing.Size(69, 58);
            this.Title7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title7.TabIndex = 10;
            this.Title7.TabStop = false;
            // 
            // Title6
            // 
            this.Title6.BackColor = System.Drawing.Color.Transparent;
            this.Title6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title6.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title6.Location = new System.Drawing.Point(116, 42);
            this.Title6.Name = "Title6";
            this.Title6.Size = new System.Drawing.Size(69, 58);
            this.Title6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title6.TabIndex = 9;
            this.Title6.TabStop = false;
            // 
            // Title5
            // 
            this.Title5.BackColor = System.Drawing.Color.Transparent;
            this.Title5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title5.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title5.Location = new System.Drawing.Point(47, 42);
            this.Title5.Name = "Title5";
            this.Title5.Size = new System.Drawing.Size(69, 58);
            this.Title5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title5.TabIndex = 8;
            this.Title5.TabStop = false;
            // 
            // Title4
            // 
            this.Title4.BackColor = System.Drawing.Color.Transparent;
            this.Title4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title4.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title4.Location = new System.Drawing.Point(47, 98);
            this.Title4.Name = "Title4";
            this.Title4.Size = new System.Drawing.Size(69, 58);
            this.Title4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title4.TabIndex = 7;
            this.Title4.TabStop = false;
            // 
            // Title3
            // 
            this.Title3.BackColor = System.Drawing.Color.Transparent;
            this.Title3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title3.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title3.Location = new System.Drawing.Point(47, 156);
            this.Title3.Name = "Title3";
            this.Title3.Size = new System.Drawing.Size(69, 58);
            this.Title3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title3.TabIndex = 6;
            this.Title3.TabStop = false;
            // 
            // Title2
            // 
            this.Title2.BackColor = System.Drawing.Color.Transparent;
            this.Title2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title2.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title2.Location = new System.Drawing.Point(47, 212);
            this.Title2.Name = "Title2";
            this.Title2.Size = new System.Drawing.Size(69, 58);
            this.Title2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title2.TabIndex = 5;
            this.Title2.TabStop = false;
            // 
            // Title1
            // 
            this.Title1.BackColor = System.Drawing.Color.Transparent;
            this.Title1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Title1.Image = global::LoSClient.Properties.Resources.Ground;
            this.Title1.Location = new System.Drawing.Point(47, 268);
            this.Title1.Name = "Title1";
            this.Title1.Size = new System.Drawing.Size(69, 58);
            this.Title1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Title1.TabIndex = 4;
            this.Title1.TabStop = false;
            // 
            // Dice
            // 
            this.Dice.BackColor = System.Drawing.Color.Transparent;
            this.Dice.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.Dice.Image = global::LoSClient.Properties.Resources.Dice1;
            this.Dice.Location = new System.Drawing.Point(641, 99);
            this.Dice.Name = "Dice";
            this.Dice.Size = new System.Drawing.Size(69, 58);
            this.Dice.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Dice.TabIndex = 26;
            this.Dice.TabStop = false;
            this.Dice.Click += new System.EventHandler(this.Dice_Click);
            // 
            // PlayerTurn
            // 
            this.PlayerTurn.BackColor = System.Drawing.Color.Transparent;
            this.PlayerTurn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.PlayerTurn.Image = global::LoSClient.Properties.Resources.YourTurn;
            this.PlayerTurn.Location = new System.Drawing.Point(587, 42);
            this.PlayerTurn.Name = "PlayerTurn";
            this.PlayerTurn.Size = new System.Drawing.Size(172, 40);
            this.PlayerTurn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PlayerTurn.TabIndex = 27;
            this.PlayerTurn.TabStop = false;
            // 
            // LeaveBtn
            // 
            this.LeaveBtn.BackColor = System.Drawing.Color.Transparent;
            this.LeaveBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.LeaveBtn.Image = global::LoSClient.Properties.Resources.LeaveBtn;
            this.LeaveBtn.Location = new System.Drawing.Point(608, 352);
            this.LeaveBtn.Name = "LeaveBtn";
            this.LeaveBtn.Size = new System.Drawing.Size(136, 36);
            this.LeaveBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.LeaveBtn.TabIndex = 28;
            this.LeaveBtn.TabStop = false;
            this.LeaveBtn.Click += new System.EventHandler(this.LeaveBtn_Click);
            // 
            // PlayerNameLabel
            // 
            this.PlayerNameLabel.AutoSize = true;
            this.PlayerNameLabel.BackColor = System.Drawing.Color.Transparent;
            this.PlayerNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.PlayerNameLabel.ForeColor = System.Drawing.Color.Maroon;
            this.PlayerNameLabel.Location = new System.Drawing.Point(495, 156);
            this.PlayerNameLabel.Name = "PlayerNameLabel";
            this.PlayerNameLabel.Size = new System.Drawing.Size(66, 17);
            this.PlayerNameLabel.TabIndex = 29;
            this.PlayerNameLabel.Text = "Unknown";
            // 
            // Loadinglabel
            // 
            this.Loadinglabel.AutoSize = true;
            this.Loadinglabel.BackColor = System.Drawing.Color.Transparent;
            this.Loadinglabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Loadinglabel.ForeColor = System.Drawing.Color.Maroon;
            this.Loadinglabel.Location = new System.Drawing.Point(394, 352);
            this.Loadinglabel.Name = "Loadinglabel";
            this.Loadinglabel.Size = new System.Drawing.Size(59, 17);
            this.Loadinglabel.TabIndex = 30;
            this.Loadinglabel.Text = "Loading";
            // 
            // Game_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LoSClient.Properties.Resources.GameBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.Loadinglabel);
            this.Controls.Add(this.PlayerNameLabel);
            this.Controls.Add(this.LeaveBtn);
            this.Controls.Add(this.PlayerTurn);
            this.Controls.Add(this.Dice);
            this.Controls.Add(this.Title20);
            this.Controls.Add(this.Title19);
            this.Controls.Add(this.Title18);
            this.Controls.Add(this.Title17);
            this.Controls.Add(this.Title16);
            this.Controls.Add(this.Title15);
            this.Controls.Add(this.Title14);
            this.Controls.Add(this.Title13);
            this.Controls.Add(this.ChatTxt);
            this.Controls.Add(this.ChatScreen);
            this.Controls.Add(this.Title12);
            this.Controls.Add(this.Title11);
            this.Controls.Add(this.Title10);
            this.Controls.Add(this.Title9);
            this.Controls.Add(this.Title8);
            this.Controls.Add(this.Title7);
            this.Controls.Add(this.Title6);
            this.Controls.Add(this.Title5);
            this.Controls.Add(this.Title4);
            this.Controls.Add(this.Title3);
            this.Controls.Add(this.Title2);
            this.Controls.Add(this.Title1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Game_Form";
            this.Text = "Game_Form";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Game_Form_FormClosed);
            this.Load += new System.EventHandler(this.Game_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Title20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Title1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Dice)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PlayerTurn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.LeaveBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox Title1;
        private System.Windows.Forms.PictureBox Title2;
        private System.Windows.Forms.PictureBox Title3;
        private System.Windows.Forms.PictureBox Title4;
        private System.Windows.Forms.PictureBox Title5;
        private System.Windows.Forms.PictureBox Title6;
        private System.Windows.Forms.PictureBox Title7;
        private System.Windows.Forms.PictureBox Title8;
        private System.Windows.Forms.PictureBox Title9;
        private System.Windows.Forms.PictureBox Title10;
        private System.Windows.Forms.PictureBox Title11;
        private System.Windows.Forms.PictureBox Title12;
        private System.Windows.Forms.RichTextBox ChatScreen;
        private System.Windows.Forms.TextBox ChatTxt;
        private System.Windows.Forms.PictureBox Title13;
        private System.Windows.Forms.PictureBox Title14;
        private System.Windows.Forms.PictureBox Title15;
        private System.Windows.Forms.PictureBox Title16;
        private System.Windows.Forms.PictureBox Title17;
        private System.Windows.Forms.PictureBox Title18;
        private System.Windows.Forms.PictureBox Title19;
        private System.Windows.Forms.PictureBox Title20;
        private System.Windows.Forms.PictureBox Dice;
        private System.Windows.Forms.PictureBox PlayerTurn;
        private System.Windows.Forms.PictureBox LeaveBtn;
        private System.Windows.Forms.Label PlayerNameLabel;
        private System.Windows.Forms.Label Loadinglabel;
    }
}