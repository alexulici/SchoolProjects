﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace LoSClient
{
    public class TCPFunctions
    {
        public Socket _clientSocket; //Creates a new socket for client
        public string _SERVERIP; //Server IP
        public int _SERVERPORT; //Server Port
        private Controller controller;

        public TCPFunctions(Controller controller)
        {
            this.controller = controller;
        }

        public string ConnectToServer()
        {
            //Tries to connect to the server with given parameters. Returns true if it succeedes else returns false
            _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            while (!_clientSocket.Connected)
            {
                try
                {
                    _clientSocket.Connect(_SERVERIP, _SERVERPORT);
                }
                catch (SocketException)
                {
                    return "Connection to server failed";
                }
            }
            return "0";
        }

        public void CloseConnection()
        {
            //_clientSocket.Shutdown(SocketShutdown.Both);
            _clientSocket.Close();
        }

        public void SendMessage(string message)
        {
            byte[] buffer = Encoding.ASCII.GetBytes(message + "|");
            try
            {
                IPEndPoint remoteIpEndPoint = _clientSocket.RemoteEndPoint as IPEndPoint;
                _clientSocket.Send(buffer, 0, buffer.Length, SocketFlags.None);
            }catch
            {
                controller.DisconnectedFromServer();
            }
        }

        public string GetMessage()
        {
            //Receives a response from server

            int received=0; //number of bytes received
            var buffer = new byte[2048]; //the buffer that gets the bytes first
            try
            {
                received = _clientSocket.Receive(buffer, SocketFlags.None);
            }catch
            {
                controller.DisconnectedFromServer();
                return "";
            }
            if (received == 0) return "";
            var data = new byte[received];
            Array.Copy(buffer, data, received);
            return Encoding.ASCII.GetString(data);
        }
    }
}
