﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSClient
{
    public class ErrorHandler
    {
        public string RegisterHandle(string param)
        {
            if (param.Equals("0"))
                return "OK";
            else if (param.Equals("1"))
                return "That username is already taken";
            else if (param.Equals("2"))
                return "Invalid password";
            else if (param.Equals("3"))
                return "Passwords not matching";
            else if (param.Equals("4"))
                return "Invalid characters in username";
            else if (param.Equals("5"))
                return "Invalid characters in nickname";
            else if (param.Equals("6"))
                return "Invalid mail format";
            else if (param.Equals("7"))
                return "Your password must have more than 3 characters";
            else if (param.Equals("8"))
                return "That nickname is already taken";
            else if (param.Equals("9"))
                return "Username and nickname must not match";
            return "An unexpected error ocurred";
        }

        public string LoginHandle(string param)
        {
            if (param.Equals("1"))
                return "Invalid username or password";
            else
                return param;
        }

        public string TokenHandle(string param)
        {
            if (param.Equals("0"))
                return "OK";
            else if (param.Equals("2"))
                return "2";
            else
                return "Invalid token or expired";
        }


        internal string RegisterValidate(string username, string nickname, string password, string confpassword, string email)
        {
            if (password.Length < 3)
                return "7";
            if (password != confpassword)
                return "3";
            if (username == nickname)
                return "9";
            if (!email.Contains('@'))
                return "6";
            if(ValidateInput(username)==false)
                return "4";
            if (ValidateInput(nickname) == false)
                return "5";
            if (ValidateInput(password) == false)
                return "2";
            return "0";
        }

        public Boolean ValidateInput(string input)
        {
            string allowedchars = "qwertyuiopasdfghjklzxcvbnm1234567890";
            for (int i = 0; i < input.Length; i++)
                if (!allowedchars.Contains(input[i]))
                    return false;
            return true;
        }
    }
}
