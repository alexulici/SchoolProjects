﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSClient.Domain
{
    public class Player
    {
        int ID;
        string nickname;
        int mmr;

        public void SetID(int id)
        {
            this.ID = id;
        }
        public void Setnickname(string nick)
        {
            nickname = nick;
        }
        public void Setmmr(int newmmr)
        {
            mmr = newmmr;
        }

        public int GetID()
        {
            return ID;
        }
        public string Getnickname()
        {
            return nickname;
        }
        public int Getmmr()
        {
            return mmr;
        }
    }
}
