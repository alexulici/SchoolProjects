﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoSClient.Domain
{
    public class Command
    {
        private int commandID;
        private string parameters;

        public Command(int id, string param)
        {
            this.commandID = id;
            this.parameters = param;
        }

        public Command()
        {

        }

        public override string ToString()
        {
            return commandID + " " + parameters;
        }

        public int GetCommandID()
        {
            return commandID;
        }
        public void SetCommandID(int commandID)
        {
            this.commandID = commandID;
        }
        public string GetParameters()
        {
            return parameters;
        }
        public void SetParameters(string parameters)
        {
            this.parameters = parameters;
        }
    }
}
