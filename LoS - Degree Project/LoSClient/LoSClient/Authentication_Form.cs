﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class Authentication_Form : Form
    {
        private Controller controller;

        public Authentication_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.SetAuthForm(this);
        }

        private void Authentication_Form_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
        }

        private void LoginBtn_Click(object sender, EventArgs e)
        {
            controller.Login(UsernameTxt.Text, PwdTxt.Text);
        }

        public void LogResponse(string message)
        {
            if (!message.Equals("Invalid username or password"))
            {
                controller.MyToken = message;
                Invoke((MethodInvoker)delegate ()
                {
                    Multiplayer_Form multiform = new Multiplayer_Form(controller);
                    multiform.Show();
                });
                this.Close();
            }
            else
                MessageBox.Show("Login failed: " + message);
        }

        private void RegisterBtn_Click(object sender, EventArgs e)
        {
            Register_Form registerform = new Register_Form(controller);
            registerform.Show();
            this.Close();
        }

        private void CancelBtn_Click(object sender, EventArgs e)
        {
            controller.TCP_Fct.CloseConnection();
            this.Close();
        }
    }
}
