﻿namespace LoSClient
{
    partial class Main_Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main_Form));
            this.SinglePlayerBtn = new System.Windows.Forms.PictureBox();
            this.MultiplayerBtn = new System.Windows.Forms.PictureBox();
            this.RulesBtn = new System.Windows.Forms.PictureBox();
            this.CreditsBtn = new System.Windows.Forms.PictureBox();
            this.QuitBtn = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.SinglePlayerBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.MultiplayerBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.RulesBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditsBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuitBtn)).BeginInit();
            this.SuspendLayout();
            // 
            // SinglePlayerBtn
            // 
            this.SinglePlayerBtn.BackColor = System.Drawing.Color.Transparent;
            this.SinglePlayerBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.SinglePlayerBtn.Image = ((System.Drawing.Image)(resources.GetObject("SinglePlayerBtn.Image")));
            this.SinglePlayerBtn.Location = new System.Drawing.Point(26, 102);
            this.SinglePlayerBtn.Name = "SinglePlayerBtn";
            this.SinglePlayerBtn.Size = new System.Drawing.Size(163, 36);
            this.SinglePlayerBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SinglePlayerBtn.TabIndex = 0;
            this.SinglePlayerBtn.TabStop = false;
            this.SinglePlayerBtn.Click += new System.EventHandler(this.SinglePlayerBtn_Click);
            // 
            // MultiplayerBtn
            // 
            this.MultiplayerBtn.BackColor = System.Drawing.Color.Transparent;
            this.MultiplayerBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.MultiplayerBtn.Image = global::LoSClient.Properties.Resources.MultiplayerBtn;
            this.MultiplayerBtn.Location = new System.Drawing.Point(26, 144);
            this.MultiplayerBtn.Name = "MultiplayerBtn";
            this.MultiplayerBtn.Size = new System.Drawing.Size(163, 36);
            this.MultiplayerBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.MultiplayerBtn.TabIndex = 1;
            this.MultiplayerBtn.TabStop = false;
            this.MultiplayerBtn.Click += new System.EventHandler(this.MultiplayerBtn_Click);
            // 
            // RulesBtn
            // 
            this.RulesBtn.BackColor = System.Drawing.Color.Transparent;
            this.RulesBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.RulesBtn.Image = global::LoSClient.Properties.Resources.RulesBtn;
            this.RulesBtn.Location = new System.Drawing.Point(617, 242);
            this.RulesBtn.Name = "RulesBtn";
            this.RulesBtn.Size = new System.Drawing.Size(144, 34);
            this.RulesBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.RulesBtn.TabIndex = 3;
            this.RulesBtn.TabStop = false;
            // 
            // CreditsBtn
            // 
            this.CreditsBtn.BackColor = System.Drawing.Color.Transparent;
            this.CreditsBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.CreditsBtn.Image = global::LoSClient.Properties.Resources.CreditsBtn;
            this.CreditsBtn.Location = new System.Drawing.Point(617, 282);
            this.CreditsBtn.Name = "CreditsBtn";
            this.CreditsBtn.Size = new System.Drawing.Size(144, 34);
            this.CreditsBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.CreditsBtn.TabIndex = 4;
            this.CreditsBtn.TabStop = false;
            // 
            // QuitBtn
            // 
            this.QuitBtn.BackColor = System.Drawing.Color.Transparent;
            this.QuitBtn.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.QuitBtn.Image = global::LoSClient.Properties.Resources.QuitBtn;
            this.QuitBtn.Location = new System.Drawing.Point(26, 320);
            this.QuitBtn.Name = "QuitBtn";
            this.QuitBtn.Size = new System.Drawing.Size(163, 34);
            this.QuitBtn.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.QuitBtn.TabIndex = 5;
            this.QuitBtn.TabStop = false;
            this.QuitBtn.Click += new System.EventHandler(this.QuitBtn_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.Maroon;
            this.label1.Location = new System.Drawing.Point(614, 374);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 17);
            this.label1.TabIndex = 10;
            this.label1.Text = "Client Version :";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Maroon;
            this.label2.Location = new System.Drawing.Point(723, 374);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 17);
            this.label2.TabIndex = 11;
            this.label2.Text = "x.x.x";
            // 
            // Main_Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::LoSClient.Properties.Resources.LoSBackground;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(800, 400);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.QuitBtn);
            this.Controls.Add(this.CreditsBtn);
            this.Controls.Add(this.RulesBtn);
            this.Controls.Add(this.MultiplayerBtn);
            this.Controls.Add(this.SinglePlayerBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Main_Form";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Main_Form_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SinglePlayerBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.MultiplayerBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.RulesBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.CreditsBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.QuitBtn)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox SinglePlayerBtn;
        private System.Windows.Forms.PictureBox MultiplayerBtn;
        private System.Windows.Forms.PictureBox RulesBtn;
        private System.Windows.Forms.PictureBox CreditsBtn;
        private System.Windows.Forms.PictureBox QuitBtn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}

