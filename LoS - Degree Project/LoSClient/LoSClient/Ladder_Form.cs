﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class Ladder_Form : Form
    {
        private Controller controller;

        public Ladder_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
            controller.SetLadderForm(this);
        }

        private void Ladder_Form_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("Nickname", "Nickname");
            dataGridView1.Columns.Add("Wins", "Wins");
            dataGridView1.Columns.Add("Losses", "Losses");
            dataGridView1.Columns.Add("Total Games", "Total Games");
            dataGridView1.Columns.Add("MMR", "MMR");
            controller.LoadLadder();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        public void AddPlayerToLadder(string message)
        {
            string[] split = message.Split(' ');
            DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row.Cells[0].Value = split[0];
            row.Cells[1].Value = split[1];
            row.Cells[2].Value = split[3];
            row.Cells[3].Value = Convert.ToInt16(split[4]) - Convert.ToInt16(split[3]);
            row.Cells[4].Value = split[4];
            row.Cells[5].Value = split[2];
            dataGridView1.Rows.Add(row);
        }

        private void BackBtn_Click(object sender, EventArgs e)
        {
            Multiplayer_Form multi_form = new Multiplayer_Form(controller);
            multi_form.Show();
            this.Close();
        }
    }
}
