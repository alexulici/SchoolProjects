﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class Main_Form : Form
    {
        Controller controller;
        public Main_Form()
        {
            InitializeComponent();
            controller = new Controller();
            controller.SetMainForm(this);
        }

        public Main_Form(Controller controller)
        {
            InitializeComponent();
            this.controller = controller;
        }

        private void SinglePlayerBtn_Click(object sender, EventArgs e)
        {
        }

        private void MultiplayerBtn_Click(object sender, EventArgs e)
        {
            controller.TCP_Fct._SERVERIP = controller.Dispatch_IP;
            controller.TCP_Fct._SERVERPORT = controller.Dispatch_Port;
            string result = controller.TCP_Fct.ConnectToServer();
            if (result.Equals("0"))
            {
                controller.StartListening();
                Authentication_Form authform = new Authentication_Form(controller);
                authform.Show();
                this.Hide();
            }
            else
                MessageBox.Show("Error: " + result);
        }

        private void Main_Form_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            string result=controller.LoadFromFile();
            if (!result.Equals("0"))
                MessageBox.Show("Error: " + result);
            label2.Text = Application.ProductVersion;
        }

        private void QuitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
