﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LoSClient
{
    public partial class GamesList_From : Form
    {
        private Controller controller;
        private int mode;

        public GamesList_From(Controller controller, int v)
        {
            InitializeComponent();
            this.controller = controller;
            this.mode = v;
            controller.SetGamesListForm(this);
        }

        private void GamesList_From_Load(object sender, EventArgs e)
        {
            Control.CheckForIllegalCrossThreadCalls = false;
            dataGridView1.Columns.Add("ID", "ID");
            dataGridView1.Columns.Add("Players", "Players");
            dataGridView1.Columns.Add("Type", "Type");
            dataGridView1.Columns.Add("Duration", "Duration");
            dataGridView1.Columns.Add("Winner", "Winner");
            dataGridView1.Columns.Add("MMR", "MMR");
            controller.LoadMatches(mode);
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
        }

        public void AddMatch(string message)
        {
            string[] split = message.Split(' ');
            string players="";
            for(int i=5;i<split.Length;i++)
            {
                players += split[i] + ",";
            }
            players.Substring(0, players.Length - 1);
            DataGridViewRow row = (DataGridViewRow)dataGridView1.Rows[0].Clone();
            row.Cells[0].Value = split[0]; //id
            row.Cells[1].Value = players; //players
            row.Cells[2].Value = split[1]; //matchtype
            row.Cells[3].Value = split[2]; //duration
            row.Cells[4].Value = split[3]; //winner
            row.Cells[5].Value = split[4]; //mmrdiff
            dataGridView1.Rows.Add(row);
        }

        private void pictureBox7_Click(object sender, EventArgs e)
        {
            Multiplayer_Form multi_form = new Multiplayer_Form(controller);
            multi_form.Show();
            this.Close();
        }
    }
}
