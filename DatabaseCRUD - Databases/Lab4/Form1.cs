﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace Lab4
{
    public partial class Form1 : Form
    {
        private Boolean ascendingbool=true;
        private string ascending="ASC";
        private string orderby = "nume";
        private string nrmatricol;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            dateTimePicker1.CustomFormat = "MM/dd/yyyy";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            con.Open();
            SqlCommand command = new SqlCommand("SELECT denumires FROM sectii",con);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    listBox1.Items.Add(reader.GetString(0));
                }
            }
            reader.Close();
            con.Close();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            listBox2.Items.Clear();
            int cods=0;
            con.Open();
            SqlCommand command1 = new SqlCommand("SELECT cods FROM sectii WHERE denumires='" + listBox1.SelectedItem.ToString() + "'", con);
            SqlDataReader reader1 = command1.ExecuteReader();
            
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    cods = Convert.ToInt32(reader1["cods"]);
                }
            }
            reader1.Close();
            con.Close();

            con.Open();
            SqlCommand command2 = new SqlCommand("SELECT nume,grupa FROM studenti WHERE cods=" + cods.ToString(), con);
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    listBox2.Items.Add(reader2.GetString(0) + "|" + reader2.GetString(1));
                }
            }
            reader2.Close();
            con.Close();
            RefreshList();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            con.Open();

            SqlCommand com = new SqlCommand("insert into studenti (cods, nrmatricol, nume, grupa, datan) values (@codSec, @nrMat, @nume, @grupa, convert(datetime, @dataText, 101))", con);

            SqlParameter p1 = new SqlParameter();
            SqlParameter p2 = new SqlParameter();
            SqlParameter p3 = new SqlParameter();
            SqlParameter p4 = new SqlParameter();
            SqlParameter p5 = new SqlParameter();

            p1.ParameterName = "codSec";
            p2.ParameterName = "nrMat";
            p3.ParameterName = "nume";
            p4.ParameterName = "grupa";
            p5.ParameterName = "dataText";

            p1.Value = textBox1.Text;
            p2.Value = textBox2.Text;
            p3.Value = textBox3.Text;
            p4.Value = textBox4.Text;
            string format = "MM/dd/yyyy";
            DateTime time = dateTimePicker1.Value;
            p5.Value = time.ToString(format);

            com.Parameters.Clear();

            com.Parameters.Add(p1);
            com.Parameters.Add(p2);
            com.Parameters.Add(p3);
            com.Parameters.Add(p4);
            com.Parameters.Add(p5);

            com.ExecuteNonQuery();

            con.Close();
            RefreshList();
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateTime data;
            con.Open();
            SqlCommand command1 = new SqlCommand("SELECT cods,nrmatricol,nume,grupa,datan FROM studenti WHERE nume='" + StudentName(listBox2.SelectedItem.ToString()) + "'", con);
            SqlDataReader reader1 = command1.ExecuteReader();
            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    try
                    {
                        textBox1.Text = Convert.ToInt32(reader1["cods"]).ToString();
                        textBox2.Text = Convert.ToInt32(reader1["nrmatricol"]).ToString();
                        textBox3.Text = reader1.GetString(2);
                        textBox4.Text = Convert.ToInt32(reader1["grupa"]).ToString();
                        data = reader1.GetDateTime(4);
                        if(data!=null)
                            dateTimePicker1.Value = data;
                    }
                    catch
                    {
                    }
                }
            }
            reader1.Close();
            con.Close();
            nrmatricol = textBox2.Text;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            con.Open();
            SqlCommand command = new SqlCommand("DELETE FROM studenti WHERE nume='" + textBox3.Text + "'" + " AND grupa='" + textBox4.Text + "';", con);
            command.ExecuteNonQuery();
            con.Close();
            RefreshList();
        }

        private string StudentName(string x)
        {
            int i=x.Length-1;
            while (x[i] != '|')
            {
                i--;
            }
            return x.Substring(0, i);
        }

        private void RefreshList()
        {
            int cods=0;
            listBox2.Items.Clear();
            con.Open();
            SqlCommand command1 = new SqlCommand("SELECT cods FROM sectii WHERE denumires='" + listBox1.SelectedItem.ToString() + "'", con);
            SqlDataReader reader1 = command1.ExecuteReader();

            if (reader1.HasRows)
            {
                while (reader1.Read())
                {
                    cods = Convert.ToInt32(reader1["cods"]);
                }
            }
            reader1.Close();
            con.Close();

            con.Open();
            SqlCommand command2 = new SqlCommand("SELECT nume,grupa FROM studenti " + "WHERE cods=" + cods.ToString() + " ORDER BY " + orderby + " " + ascending, con);
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                while (reader2.Read())
                {
                    listBox2.Items.Add(reader2.GetString(0) + "|" + reader2.GetString(1));
                }
            }
            reader2.Close();
            con.Close();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton1.Checked)
                orderby = "nume";
            RefreshList();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (ascendingbool == true)
            {
                pictureBox1.Image = Lab4.Properties.Resources.downarrow;
                ascendingbool = false;
                ascending = "DESC";
            }
            else
            {
                ascendingbool = true;
                pictureBox1.Image = Lab4.Properties.Resources.uparrow;
                ascending = "ASC";
            }
            RefreshList();
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if(radioButton2.Checked)
                orderby = "grupa";
            RefreshList();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            con.Open();

            SqlCommand com = new SqlCommand("UPDATE studenti SET cods=@codSec, nrmatricol=@nrMat, nume=@nume, grupa=@grupa, datan=convert(datetime, @dataText, 101) WHERE nrmatricol=@nrMat", con);

            SqlParameter p1 = new SqlParameter();
            SqlParameter p2 = new SqlParameter();
            SqlParameter p3 = new SqlParameter();
            SqlParameter p4 = new SqlParameter();
            SqlParameter p5 = new SqlParameter();

            p1.ParameterName = "codSec";
            p2.ParameterName = "nrMat";
            p3.ParameterName = "nume";
            p4.ParameterName = "grupa";
            p5.ParameterName = "dataText";

            p1.Value = textBox1.Text;
            p2.Value = nrmatricol;
            p3.Value = textBox3.Text;
            p4.Value = textBox4.Text;
            string format = "MM/dd/yyyy";
            DateTime time = dateTimePicker1.Value;
            p5.Value = time.ToString(format);

            com.Parameters.Clear();

            com.Parameters.Add(p1);
            com.Parameters.Add(p2);
            com.Parameters.Add(p3);
            com.Parameters.Add(p4);
            com.Parameters.Add(p5);

            com.ExecuteNonQuery();

            con.Close();
            RefreshList();
        }
    }
}
