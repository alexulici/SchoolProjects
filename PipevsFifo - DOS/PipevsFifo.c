#include<unistd.h>
#include<stdio.h>
#include<fcntl.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<time.h>
#include<signal.h>
#include<stdlib.h>
#include<string.h>
int fd1[2],p1,p2,count1,count2,fd2,i;
clock_t begin;
double time_spent,time_wait;
char mesaj;
main()
{
	time_wait=0.5;
	pipe(fd1);
	p1=fork();
	p2=fork();
	fd2=open("1",O_WRONLY);
	if(p1==0)
	{
		while(1)
		{
			read(fd1[0],&mesaj,1);
			count1=count1+1;
			printf("Pipe: %d\n",count1);
		}
	}
	begin = clock();
	while(1)
	{
		write(fd1[1],&mesaj,1);
		time_spent = (double)(clock() - begin) / CLOCKS_PER_SEC;
		if (time_spent>=time_wait)
		{
			kill(p1, SIGKILL);
			break;
		}
	}
	sleep(5);
	if(p2==0)
	{
		while(1)
		{
			read(fd2,&mesaj,1);
			count2=count2+1;
			printf("Fifo: %d\n",count2);
		}
	}
	begin = clock();
	while(1)
	{
		write(fd2,&mesaj,1);
		time_spent = (double)(clock() - begin) / CLOCKS_PER_SEC;
		if (time_spent>=time_wait)
		{
			kill(p2, SIGKILL);
			break;
		}
	}
	exit(0);
}
