#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <unistd.h>
#include <strings.h>
#include <stdint.h>
#include <stdint-gcc.h>
#include <signal.h>

socklen_t clilen;

int pid;

int sockfd,newsockfd;//file descriptorul soketului
struct sockaddr_in serv_addr,cli_addr;//adresa servarului si a clientului

void time_out(int semnal) {
  int32_t r = -1;
  r = htonl(r);
  printf("Time out.\n");
  send(newsockfd, &r, sizeof(int32_t), 0);
  close(newsockfd); // desi nu am primit nimic de la client in 10 secunde, inchidem civilizat conexiunea cu acesta
  exit(1);
}


void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void handelClient();

main() {
    
    sockfd=socket(AF_INET, SOCK_STREAM, 0);
    if(sockfd<0)
        error("Eroare la crearea soketului");

    serv_addr.sin_family=AF_INET;
    serv_addr.sin_addr.s_addr=INADDR_ANY;
    serv_addr.sin_port=htons(2300);

    if(bind(sockfd,(struct sockaddr *)&serv_addr, sizeof(serv_addr))<0)
        error("Error binding");

    listen(sockfd,5);

    clilen=sizeof(cli_addr);

    while(1){
        
        newsockfd = accept(sockfd,(struct sockaddr *)&cli_addr,&clilen);
        
        if(newsockfd<0)
            error("ERROR la aceptare");
        pid = fork();//cream un proces copil care se va ocupa de client
        if(pid==0){
            close(sockfd);
            handelClient();
            close(newsockfd);
            exit(0);
        }
        else
            close(newsockfd);
    }
    close(sockfd);

}
void handelClient(){

    int cod;
    int32_t r;
    int32_t aux=0;
    uint8_t buff;

    signal(SIGALRM, time_out);
    alarm(10);

    r=0;//rezultatul sumei

    do{
        cod = recv(newsockfd, &buff, 1, 0);

        if (cod == 1) // citire cu succes a unui caracter
            alarm(10);

        if (cod != 1) {
          r = -1;
          break;
        }
        
        if(buff !=' '){
            aux=aux*10+buff;
        }
        if (buff == ' ') {
          // Paragraful 5 din protocolul
          if (r == INT32_MAX) { // intregul maxim pozitiv pe 4 octetii cu semn sau 0x7FFFFFFF (a se vedea stdint.h)
            r = -2;
            break;
          }
          r+=aux;
          aux=0;
        }     

    }while(buff!=0);

    //printf("%d-->%s\n",getpid(),buf);
    alarm(0); // oprim timerul

    r = htonl(r);
    send(newsockfd, &r, sizeof(int32_t), 0);
    r = ntohl(r);
    
    close(newsockfd);

    if (r >= 0)
        printf("Am inchis cu succes conexiunea cu un client. I-am trimis suma: %d .\n", r);
    else {
        printf("Am inchis cu eroare conexiunea cu un client. Cod de eroare %d.\n", r);
        exit(1);
    }
    
  exit(0);
  // Terminam procesul fiu - foarte important!!! altfel numarul de procese creste exponential.
  // Fiul se termina dupa ce deserveste clientul.

}









