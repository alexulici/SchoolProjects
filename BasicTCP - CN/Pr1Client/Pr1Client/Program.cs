﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Pr1Client
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                TcpClient tcpclnt = new TcpClient();
                Console.WriteLine("Connecting.....");

                tcpclnt.Connect("127.0.0.1", 8080);

                Console.WriteLine("Connected");

                String[] str = new String[5];
                str[0] = "5 a.txt";
                str[1] = "3 asd";
                str[2] = "5 aicia";
                str[3] = "3 mek";
                str[4] = "6 finish";
                Stream stm = tcpclnt.GetStream();
                foreach (String s in str)
                {
                    System.Threading.Thread.Sleep(1000);
                    ASCIIEncoding asen = new ASCIIEncoding();
                    byte[] ba = asen.GetBytes(s);
                    Console.WriteLine("Transmitting.....");
                    stm.Write(ba, 0, ba.Length);
                }

                byte[] bb = new byte[100];
                int k = stm.Read(bb, 0, 100);

                for (int i = 0; i < k; i++)
                    Console.Write(Convert.ToChar(bb[i]));

                tcpclnt.Close();
                Console.ReadLine();
            }

            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
                Console.ReadLine();
            }
        }
    }
}
