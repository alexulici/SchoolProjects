﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Backtrack
{
    static class Util
    {
        static private Boolean Distinct (int[] v,int vsize)
        {
            for (int i = 1; i < vsize; i++)
            {
                for (int j = i + 1; j < vsize; j++)
                    if (v[i] == v[j] && v[i]!=0 && v[j] !=0)
                        return false;
            }
            return true;
        }

        static private Boolean DistinctGreedy(int[] v, int vsize)
        {
            for (int i = 0; i < vsize; i++)
            {
                for (int j = i + 1; j < vsize; j++)
                    if (v[i] == v[j] && v[i]!=-1 && v[j] !=-1)
                        return false;
            }
            return true;
        }

        static public Boolean CheckQueen(int[] v,int vsize)
        {

            if (Distinct(v,vsize) == true)
            {
                for (int i = 1; i <= vsize; i++)
                {
                    if (v[i] == v[i + 1] + 1 && i<vsize && v[i+1]!=0)
                        return false;
                    for (int j = 1; j <= vsize; j++)
                        if (Math.Abs(v[i] - i) == Math.Abs(v[j] - j) && i != j && v[j] != 0 && v[i] != 0)
                            return false;
                }
                return true;
            }
            else
                return false;
        }

        static public Boolean CheckQueenGreedy(int[] v, int vsize)
        {
            if (DistinctGreedy(v, vsize) == true)
            {
                int[,] mat = new int[vsize, vsize];
                for (int i = 0; i < vsize; i++)
                {
                    if (v[i] != -1)
                    {
                        try
                        {
                            if (mat[i, v[i]] == 2 || mat[i, v[i]] ==1)
                                return false;
                            else
                                mat[i, v[i]] = 1;
                        }
                        catch
                        {
                            return false;
                        }
                        for (int j = 1; j <= vsize; j++)
                        {
                            //dreapta jos
                            try
                            {
                                mat[i + j, v[i] + j] = 2;
                            }
                            catch
                            {
                            }
                            //stanga jos
                            try
                            {
                                mat[i - j, v[i] + j] = 2;
                            }
                            catch
                            {
                            }
                            //dreapta sus
                            try
                            {
                                mat[i + j, v[i] - j] = 2;
                            }
                            catch
                            {
                            }
                            //stanga sus
                            try
                            {
                                mat[i - j, v[i] - j] = 2;
                            }
                            catch
                            {
                            }
                        }
                    }
                }
                return true;
            }
            else
                return false;
        }

        static public int ZeroCount(int[] v, int vsize)
        {
            int j = 0;
            for (int i = 1; i <= vsize; i++)
                if (v[i] == 0)
                    j++;
            return j;
        }

        static public string VectorToString(int[] v, int vsize)
        {
            string output = "";
            for (int i = 1; i <= vsize; i++)
                output = output + v[i];
            return output;
        }

        static public void ClearVector(int[] v)
        {
            for (int i = 0; i < v.Length; i++)
                v[i] = 0;
        }

        static public int[] SolutionToArray(string sol,int n,int m)
        {
            int[] rez = new int[m+1];
            int c = 1;
            foreach (char s in sol)
            {
                rez[c] = Convert.ToInt32(s);
                rez[c] -= 48;
                c++;
            }
            for (int x = c; x < n + 1; x++)
                rez[x] = 0;
            return rez;
        }
    }
}
