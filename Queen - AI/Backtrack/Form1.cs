﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Backtrack
{
    public partial class Form1 : Form
    {
        List<PictureBox> pictureBoxList = new List<PictureBox>();
        int[] a = new int[20];
        int [] viz = new int[20];
        int[] max = new int[20];
        int zerocount = 9999;
        int n, m,solutions;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label1.Show();
            label4.Show();
            solutions = 0;
            zerocount = 99999;
            listBox1.Items.Clear();
            n = Convert.ToInt32(textBox1.Text);
            m = Convert.ToInt32(textBox2.Text);
            if(radioButton1.Checked == true)
                Back(1);
            if (radioButton2.Checked == true)
                Greedy(0);
            label4.Text = Util.VectorToString(max, n);

        }

        private void Greedy(int top)
        {
            int ok=0;
            for (int i = 0; i < n; i++)
                a[i] = -1;
            int poz=0;
            for (int i = top; i < n; i++)
            {
                ok = 0;
                for (int j = 0; j < n; j++)
                {
                    a[i] = j;
                    poz = j;
                    if (Util.CheckQueenGreedy(a, n) == true)
                    {
                        ok = 1;
                        break;
                    }
                }
                if (poz == n-1 && ok==0)
                    a[i] = -1;
            }
            for (int i = n; i >= 0; i--)
            {
                a[i]++;
                a[i + 1] = a[i];
            }
            PrintSol();
        }
        private void Back(int top)
        {
            int i;
            if (top == n + 1 && Util.CheckQueen(a, n) == true)
                PrintSol();
            else
                for(i=0;i<=n;i++)
                    if (viz[i] == 0)
                    {
                        viz[i] = 1;
                        a[top] = i;
                        Back(top + 1);
                        viz[i] = 0;
                    }
        }
        private void PrintSol()
        {
                string output;
                solutions = solutions + 1;
                label1.Text = solutions.ToString();
                output = Util.VectorToString(a, n);
                listBox1.Items.Add(output);
                int fret = Util.ZeroCount(a,n);
                if (fret < zerocount)
                {
                    a.CopyTo(max,0);
                    zerocount = fret;
                }
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GenerateGraphic(Util.SolutionToArray(listBox1.SelectedItem.ToString(),n,m));

        }
        private void GenerateGraphic(int[] r)
        {
            foreach (PictureBox pic in pictureBoxList)
            {
                Controls.Remove(pic);
            }
            pictureBoxList.Clear();
            Point p = new Point(0, 0);
            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= m; j++)
                {
                    p.X = j * 50 + 100;
                    PictureBox picture = new PictureBox
                    {
                        Name = "pictureBox" + i,
                        Size = new Size(50, 50),
                        Location = p,
                        BorderStyle = BorderStyle.None,
                        SizeMode = PictureBoxSizeMode.Zoom
                    };
                    if (r[j] == i)
                        picture.Image = Backtrack.Properties.Resources.BoxR;
                    else
                        picture.Image = Backtrack.Properties.Resources.Box;
                    pictureBoxList.Add(picture);
                    Controls.Add(picture);
                }
                p.X = 0;
                p.Y = i * 40;
            }
        }

        private void label4_Click(object sender, EventArgs e)
        {
            GenerateGraphic(Util.SolutionToArray(label4.Text, n, m));
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Hide();
            label4.Hide();
        }
    }
}
