/*
 * carList.c
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */
#include "carsList.h"
#include "assert.h"
CarList createList()
{
	CarList l;
	l.len=0;
	return l;
}

void add(CarList *l, Car s)
{
	l->caList[l->len]=s;
	l->len++;
}

int getSize(CarList *l)
{
	return l->len;
}

Car getAtIndex(CarList *l, int index)
{
	return l->caList[index];
}

void testList()
{
	CarList l=createList();
	assert(getSize(&l)==0);
	add(&l,createCar(123,11,"suv"));
	assert(getSize(&l)==1);
	add(&l,createCar(12,20,"mini"));
	assert(getSize(&l)==2);

}

