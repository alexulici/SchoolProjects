/*
 * carManagement.h
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */

#ifndef CARMANAGEMENT_H_
#define CARMANAGEMENT_H_

#include "CarsList.h"

void addCar(CarList* l, int numar, int model, char* categorie);
#endif
