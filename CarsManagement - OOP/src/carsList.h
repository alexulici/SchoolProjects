/*
 * carList.h
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */

#ifndef CARSLIST_H_
#define CARSLIST_H_

#include "car.h"
typedef struct {
	int len;
	Car caList[100];

} CarList;

CarList createList();

void add(CarList *l, Car s);

int getSize(CarList *l);

Car getAtIndex(CarList *l, int index);

void testList();


#endif
