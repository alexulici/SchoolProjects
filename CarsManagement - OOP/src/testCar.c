/*
 * testCar.c
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */



#include "testCar.h"
#include "Car.h"
#include "string.h"
#include "assert.h"

void testCreate()
{
	Car s=createCar(123,11,"suv");
	assert(getNumar(s)==123);
	assert(getModel(s)==11);
	assert(strcmp(getCategorie(s),"suv")==0);
}
void testCar()
{
	testCreate();
}
