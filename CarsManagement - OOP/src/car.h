/*
 * car.h
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */

#ifndef Car_H_
#define Car_H_

typedef struct{
	int numar,model;
	char categorie[20];
}Car;

Car createCar(int numar,int model,char* categorie);

int getNumar(const Car s);
void setNumar(Car s, int numar);

int getModel(Car s);
void setModel(Car s, int model);

char* getCategorie(const Car s);
void setCategorie(Car s, char* categorie);

#endif
