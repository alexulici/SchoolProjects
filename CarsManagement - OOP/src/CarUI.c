/*
 * CarUI.c
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */
//Problema 4
#include "car.h"
#include <stdio.h>
#include <conio.h>
#include "carManagement.h"
#include "carsList.h"
#include "testCar.h"

void readCar(CarList *l) {
	int numar,model;
	char categorie[20];
	printf("Numarul de inamtriculare:");
	scanf("%d", &numar);
	printf("Model:");
	scanf("%d", &model);
	printf("Categoria:");
	scanf("%s", &categorie);
	addCar(l,numar,model,categorie);
}
void printCars(CarList* l) {
	Car s;
	int i;
	char ch;
	for (i = 0; i < getSize(l); i++) {
		s = getAtIndex(l,i);
		printf("Numar de inmatriculare: %d Model: %d Categorie: %s\n", getNumar(s), getModel(s), getCategorie(s));
	}
	scanf("%s",&ch);
}

void printMenu() {
	printf("\n1.[Adauga Masina]\n");
	printf("2.[Tipareste masinile]\n");
	printf("0.[Iesire]\n");
}

int main() {
	setbuf(stdout,NULL);
	testList();
	testCar();
	CarList l = createList();
	while (1) {
		printMenu();
		int option;
		scanf("%d", &option);
		if (option == 0)
			return 0;
		if (option == 1)
			readCar(&l);
		if (option == 2)
			printCars(&l);
	}
	return 0;
}


