/*
 * car.c
 *
 *  Created on: 12.03.2014
 *      Author: Alex
 */

#include <string.h>
#include "car.h"
#include "util.h"

Car createCar(int numar, int model, char* categorie)
{
	Car s;
	s.numar=numar;
	s.model=model;
	strcpy(s.categorie,categorie);
	return s;
}

int getNumar(Car s)
{
	return s.numar;
}
void setNumar(Car s, int numar)
{
	s.numar=numar;
}

int getModel(Car s)
{
	return s.model;
}
void setModel(Car s, int model)
{
	s.model=model;
}

char* getCategorie(Car s)
{
	return copyString(s.categorie);
}

void setCategorie(Car s, char* categorie)
{
	strcpy(s.categorie,categorie);
}





