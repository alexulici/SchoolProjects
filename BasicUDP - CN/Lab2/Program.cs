﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Lab2
{
    class Program
    {
        const int port = 1234;

        static void Main(string[] args)
        {
            string data = "";
            SendMessage("Hello", IPAddress.Parse("127.0.0.1"));

            UdpClient client = new UdpClient(1235);
            IPEndPoint remoteIPEndPoint = new IPEndPoint(IPAddress.Any, 0);
            byte[] receivedBytes = client.Receive(ref remoteIPEndPoint);
            data = Encoding.ASCII.GetString(receivedBytes);
            Console.WriteLine(data);
            Console.Read();
        }

        static private void SendMessage(string data, IPAddress who)
        {
            byte[] sendBytes = new Byte[1024];

            UdpClient client = new UdpClient();
            client.Connect(who, port);
            sendBytes = Encoding.ASCII.GetBytes(data);
            client.Send(sendBytes, sendBytes.GetLength(0));
            client.Close();  //close connection
        }
    }
}
