﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Lab2Server
{
    class Program
    {
        const int port = 1234;
        static IPAddress ClientIp;

        static void Main(string[] args)
        {
            string data = "";

            UdpClient server = new UdpClient(port);
            IPEndPoint remoteIPEndPoint = new IPEndPoint(IPAddress.Any, 0);
            while (1==1)
            {
                byte[] receivedBytes = server.Receive(ref remoteIPEndPoint);
                ClientIp = IPAddress.Parse(remoteIPEndPoint.Address.ToString());
                data = Encoding.ASCII.GetString(receivedBytes);
                SendMessage(data.TrimEnd(),ClientIp);
            }
        }

        static private void SendMessage(string data,IPAddress who)
        {
            byte[] sendBytes = new Byte[1024];
            UdpClient client = new UdpClient();
            client.Connect(who, 1235);
            sendBytes = Encoding.ASCII.GetBytes(data);
            client.Send(sendBytes, sendBytes.GetLength(0));
            client.Close();  //close connection
        }
    }
}
