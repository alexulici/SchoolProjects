#include "Angajat.h"
#define SIZE 100
class ListaOrd: public Angajat
{
	friend class Iterator;
public:
	ListaOrd();
	~ListaOrd();
	void adauga(ListaOrd *l,int id,int ore);
	void sterge(ListaOrd *l,int id);
	int cauta(ListaOrd *l,int id);
	int element(ListaOrd *l,int i);
	void modifica(ListaOrd *l,int i);
	bool vida(ListaOrd *l);
	int dim(ListaOrd *l){return l->cp;}
	void iterator(ListaOrd *l);
	void primlibernou(ListaOrd *l);
	void afisare(ListaOrd *l);
	void afisaresalarii(ListaOrd *l,int &buget);
	int maxsize;
private:
	int *urm,*prec,primliber,*liber,cp,prim,*id,*ore,ultim;
	Angajat *listang;
};


class Iterator {
	friend class ListaOrd;
private:
	int curent=0;
	ListaOrd *l;
public:
	Iterator(ListaOrd *l);
	void prim(ListaOrd *l);
    void urmator(ListaOrd *l);
    bool valid(ListaOrd *l);
    int getCurent();
    int getId(ListaOrd *l, int i);
    int getOre(ListaOrd *l, int i);
    int getNext(ListaOrd *l, int i);

};
