/*Problema:
Sa se implementeze un program care distribuie salarii angajatilor in functie de bugetul firmei.
Salariul se calculeaza in felul urmator. Se ia numarul de ore lucrate si se inmulteste cu 10 la care se adauga
ca un bonus, numarul total de angajati din care se scade numarul angajatului pentru a rasplati angajatii cu vechime.
*/

#include "Firma.h"

int main()
{
	Firma *firm;
	firm->l=new ListaOrd();
	firm->salarii(firm->l);
	return 0;
}
