#include "ListaOrdonata.h"
#include <iostream>
using namespace std;
ListaOrd::ListaOrd()
{
	int i;
	urm=new int[SIZE];
	prec=new int[SIZE];
	liber=new int[SIZE];
	id=new int[SIZE];
	ore=new int[SIZE];
	maxsize=SIZE;
	prim=-1;
	ultim=-1;
	primliber=0;
	cp=0;
	for(i=0;i<=maxsize;i++)
	{
		urm[i]=-1;
		prec[i]=-1;
		liber[i]=0;
	}
}

void ListaOrd::adauga(ListaOrd *l,int xid,int xore)
{
	int p;
	l->id[l->primliber]=xid;
	l->ore[l->primliber]=xore;
	l->urm[l->primliber]=-1;
	l->prec[l->primliber]=-1;
	l->liber[l->primliber]=1;
	if(dim(l)==0)
	{
		l->prim=l->primliber;
		l->ultim=l->primliber;
	}
	else
	{
		p=l->prim;
		while(xore < l->ore[p] && l->urm[p]!=-1)
		{
			p=l->urm[p];
		}
		if(p==l->prim && xore >= l->ore[p])
		{
				l->urm[l->primliber]=p;
				l->prec[p]=l->primliber;
				l->prim=l->primliber;
		}
		else
			if(p==l->ultim && xore <= l->ore[p])
			{
				l->prec[l->primliber]=p;
				l->urm[p]=l->primliber;
				l->ultim=l->primliber;
			}
			else
				{
					l->urm[l->primliber]=p;
					l->prec[l->primliber]=l->prec[p];
					l->urm[l->prec[p]]=l->primliber;
					l->prec[p]=l->primliber;
				}
	}
	primlibernou(l);
	l->cp++;
}

void ListaOrd::sterge(ListaOrd *l,int xid)
{
	int p=l->prim;
	while(l->urm[p]!=-1 && l->id[p]!=xid)
		p=l->urm[p];
	if(p==l->prim)
	{
		l->prec[l->urm[p]]=-1;
		l->prim=l->urm[p];
	}
	else
	{
		if(p==l->ultim)
		{
			l->urm[l->prec[p]]=-1;
			l->ultim=l->prec[p];
		}
		else
		{
			l->urm[l->prec[p]]=l->urm[p];
			l->prec[l->urm[p]]=l->prec[p];
		}

	}
	l->liber[p]=0;
	primlibernou(l);
	cp--;
}

int ListaOrd::cauta(ListaOrd *l,int xid)
{
	int p=l->prim;
	while(l->urm[p]!=-1)
	{
		if(l->id[p]==xid)
			return p;
		p=l->urm[p];
	}
	if(l->id[p]==xid)
		return p;
	return -1;
}

int ListaOrd::element(ListaOrd *l,int i)
{
	int p=l->prim,j=0;
	while(l->urm[p]!=-1 && j<i)
	{
		j++;
		p=l->urm[p];
	}
	return p;
}

bool ListaOrd::vida(ListaOrd *l)
{
	if(dim(l)==0)
		return false;
	else
		return true;
}

void ListaOrd::afisare(ListaOrd *l)
{
	int p=l->prim;
	while(l->urm[p]!=-1)
	{
		cout<<"Angajatul "<<l->id[p]<<" a lucrat "<<l->ore[p]<<" ore"<<endl;
		p=l->urm[p];
	}
	cout<<"Angajatul "<<l->id[p]<<" a lucrat "<<l->ore[p]<<" ore"<<endl;

}

void ListaOrd::afisaresalarii(ListaOrd *l,int &buget)
{
	int p=l->prim,formula;
	while(l->urm[p]!=-1)
	{
		formula=l->ore[p]*10+l->cp-l->id[p];
		if(buget==0)
			cout<<"Nu mai avem buget pentru angajatul "<<l->id[p]<<endl;
		else
			if(buget-formula > 0)
			{
				cout<<"Angajatul "<<l->id[p]<<" a primit "<<formula<<" lei"<<endl;
				buget-=formula;
			}
			else
				if(buget-formula < 0)
				{
					cout<<"Angajatul "<<l->id[p]<<" a primit "<<buget<<" lei din "<<formula<<endl;
					buget=0;
				}
		p=l->urm[p];
	}
	formula=l->ore[p]*10+l->cp-l->id[p];
	if(buget==0)
		cout<<"Nu mai avem buget pentru angajatul "<<l->id[p]<<endl;
	else
		if(buget-formula > 0)
		{
			cout<<"Angajatul "<<l->id[p]<<" a primit "<<formula<<" lei"<<endl;
			buget-=formula;
		}
		else
			if(buget-formula < 0)
			{
				cout<<"Angajatul "<<l->id[p]<<" a primit "<<buget<<" lei din "<<formula<<endl;
				buget=0;
			}

}

void ListaOrd::primlibernou(ListaOrd *l)
{
	int i,ok=1;
	for(i=0;i<l->maxsize && ok;i++)
		if(l->liber[i]==0)
		{
			l->primliber=i;
			ok=0;
		}
}
ListaOrd::~ListaOrd()
{
	delete[] urm;
	delete[] prec;
	delete[] liber;
	delete[] id;
	delete[] ore;
}

void Iterator::prim(ListaOrd *l)
{
    curent=l->prim;
}

void Iterator::urmator(ListaOrd *l)
{
    curent=l->urm[curent];
}

bool Iterator::valid(ListaOrd *l)
{
    if(curent < l->maxsize && l->liber[curent] != -1)
    	return true;
    return false;
}

int Iterator::getCurent()
{
    return curent;
}

int Iterator::getId(ListaOrd *l, int i)
{
   	return l->id[i];
}

int Iterator::getOre(ListaOrd *l, int i)
{
    return l->ore[i];
}

int Iterator::getNext(ListaOrd *l, int i)
{
    return l->urm[i];
}
