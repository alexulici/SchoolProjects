/*
 * Firma.h
 *
 *  Created on: Jul 1, 2014
 *      Author: Noobsmoke
 */

#ifndef FIRMA_H_
#define FIRMA_H_

#include "ListaOrdonata.h"


class Firma{
private:
	Firma *firma;
public:
	ListaOrd *l;
	Iterator *iter;
	int buget;

	Firma(Firma *firma);
	~Firma();

	void salarii(ListaOrd *l);
};


#endif /* FIRMA_H_ */
