﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda
{
    class Domain
    {
        public class Task: IDObject, IComparable
        {
            private string title;
            private string description;
            private DateTime date;

            public Task(string title, string description, DateTime date): base(title)
            {
                this.title = title;
                this.description = description;
                this.date = date;
            }

            public void SetTitle(string title)
            {
                this.title = title;
            }
            public string GetTitle()
            {
                return this.title;
            }
            public void SetDescription(string description)
            {
                this.description = description;
            }
            public string GetDescription()
            {
                return this.description;
            }
            public void SetDate(DateTime date)
            {
                this.date = date;
            }
            public DateTime GetDate()
            {
                return this.date;
            }
            public int CompareTo(object obj)
            {
                if (obj == null) return 1;

                string title = obj as string;
                if (this.title == title)
                    return 1;
                else
                    return -1;
            }
        }
    }
}
