﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda
{

    class TaskValidator : IValidator<Domain.Task>
    {
        public bool validate(Domain.Task T)
        {
            StringBuilder Message = new StringBuilder("");
            if (T.GetTitle() == "")
                Message.Append("Title cannot be empty");
            if (T.GetDescription() == "")
                Message.Append("Description cannot be empty");
            if (T.GetTitle().Length > 10)
                Message.Append("Title cannot be longer that 10 characters");
            if (T.GetDescription().Length > 100)
                Message.Append("Description cannot contain more than 100 characters");
            if (Message.ToString() == "")
                return true;
            else
                throw new Exception(Message.ToString());
        }
    }
}
