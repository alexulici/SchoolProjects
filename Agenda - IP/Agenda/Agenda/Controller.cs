﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    class Controller
    {
        private Repository<Domain.Task> repository = new Repository<Domain.Task>();

        public void AddTask(string title,string description,DateTime date)
        {
            //adauga un nou task in memorie
            Domain.Task new_task = new Domain.Task(title, description, date);
            TaskValidator validator = new TaskValidator();
            try
            {
                validator.validate(new_task);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                return;
            }
            if (repository.FindElementbyAttribute(title) == null)
            {
                repository.AddElement(new_task);
            }
            else
                MessageBox.Show("An element with that title already exists");
        }

        public void RemoveTask(string title)
        {
            //sterge un task in functie de titlu
            if (repository.FindElementbyAttribute(title) != null)
            {
                repository.RemoveElement(repository.FindElementbyAttribute(title));
            }
            else
            {
                MessageBox.Show("The element " + title + " doesn't exist");
                return;
            }
            
        }

        public void EditTask(string title,string description,DateTime date)
        {
            //modifica atributele unui task in functie de parametrii
            Domain.Task task;
            task = repository.FindElementbyAttribute(title);
            task.SetDescription(description);
            task.SetDate(date);
        }

        public Domain.Task FindTask(string title)
        {
            //returneaza un task in functie de titlu
            return repository.FindElementbyAttribute(title);
        }

        public string[] GetAllTasksByDate(DateTime date)
        {
            //returneaza toate task-ruie la o anumita data
            string[] result = new string [repository.ListLength()];
            Domain.Task[] list = new Domain.Task[repository.ListLength()];
            Domain.Task T;
            for (int i = 0; i < repository.ListLength(); i++)
            {
                T = repository.GetElementAt(i);
                if (T.GetDate().Date == date.Date)
                    result[i] = T.GetTitle();
            }
            return result;
        }

        public Domain.Task[] GetAllTasks()
        {
            //returneaza toate task-urile din memorie
            Domain.Task[] list = new Domain.Task[repository.ListLength()];
            for (int i = 0; i < repository.ListLength(); i++)
            {
                list[i] = repository.GetElementAt(i);
            }
            return list;
        }

        public int CountTasks()
        {
            //returneaza cate task-uri avem
            return repository.ListLength();
        }

        public void LoadFromFile()
        {
            //incarca datele din fisier in memorie
            if (System.IO.File.Exists(Application.StartupPath + "/Tasks.txt"))
            {
                string[] buffer = System.IO.File.ReadAllLines(Application.StartupPath + "/Tasks.txt");
                foreach (string line in buffer)
                {
                    if (line != null)
                    {
                        string[] list = line.Split(';');
                        Domain.Task T = new Domain.Task(list[0], list[1], Convert.ToDateTime(list[2]));
                        repository.AddElement(T);
                    }
                }
            }
        }
        public void SaveToFile()
        {
            //salveaza in fisier
            Domain.Task[] list = GetAllTasks();
            string[] buffer = new string[repository.ListLength()];
            int i = 0;
            foreach (Domain.Task T in list)
            {
                buffer[i] = T.GetTitle() + ";" + T.GetDescription() + ";" + T.GetDate();
                i++;
            }
            System.IO.File.WriteAllLines(Application.StartupPath + "/Tasks.txt", buffer);
        }

        public string MostActiveDay()
        {
            //Determina ziua in care user-ul a fost mai activ
            int[] Days = new int[7];
            DayOfWeek[] days = new DayOfWeek[7];
            days[1] = DayOfWeek.Monday;
            days[2] = DayOfWeek.Tuesday;
            days[3] = DayOfWeek.Wednesday;
            days[4] = DayOfWeek.Thursday;
            days[5] = DayOfWeek.Friday;
            days[6] = DayOfWeek.Saturday;
            days[0] = DayOfWeek.Sunday;

            Domain.Task[] list = GetAllTasks();
            foreach (Domain.Task T in list)
            {
                Days[(int)T.GetDate().DayOfWeek]++;
            }
            return days[Max(Days)].ToString();
        }

        private int Max(int[] v)
        {
            //calucleaza maximul dintr-un vector de intregi
            int max=v[0];
            int pozmax=0;
            for (int i = 1; i < v.Length; i++)
            {
                if (v[i] > max)
                {
                    max = v[i];
                    pozmax = i;
                }
            }
            return pozmax;
        }
    }
}
