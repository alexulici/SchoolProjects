﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    public partial class Form1 : Form
    {
        private Controller controller = new Controller();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            controller.AddTask(textBox1.Text, textBox2.Text, dateTimePicker1.Value);
            textBox1.Text = "";
            textBox2.Text = "";
            RefreshListBox();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                controller.RemoveTask(listBox1.SelectedItem.ToString());
                RefreshListBox();
                textBox1.Text = "";
                textBox2.Text = "";
            }

        }

        void RefreshListBox()
        {
            string[] result = new string[controller.CountTasks()];
            listBox1.Items.Clear();
            result = controller.GetAllTasksByDate(dateTimePicker1.Value);
            foreach (string T in result)
            {
                if(T != null)
                    listBox1.Items.Add(T);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            controller.EditTask(textBox1.Text, textBox2.Text, dateTimePicker1.Value);
            RefreshListBox();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex > -1 && listBox1.SelectedIndex < listBox1.Items.Count)
            {
                Domain.Task T;
                T = controller.FindTask(listBox1.SelectedItem.ToString());
                textBox1.Text = T.GetTitle();
                textBox2.Text = T.GetDescription();
                dateTimePicker1.Value = T.GetDate();
                if (dateTimePicker1.Value.Date < DateTime.Today.Date)
                    label4.Show();
                else
                    label4.Hide();
            }
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            RefreshListBox();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //testing
            controller.AddTask("test", "test", DateTime.Today);
            Domain.Task T = controller.FindTask("test");
            Debug.Assert(T != null, "ERROR on finding a task");
            Debug.Assert(controller.CountTasks() == 1, "ERROR on add task");
            controller.RemoveTask("test");
            Debug.Assert(controller.CountTasks() == 0, "ERROR on remove task");
            //
            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd/MM/yyyy";
            controller.LoadFromFile();
            RefreshListBox();
            label4.Hide();
            label5.Text = "Most active day: " + controller.MostActiveDay();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            controller.SaveToFile();
        }

        private void button3_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Allows you to change description and date by specifying the title";
        }

        private void button1_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Adds a new task on the list";
        }

        private void button2_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Removes a new task from the list";
        }

        private void button4_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Saves your data in file";
        }

        private void textBox1_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Type your desired title. The title must be unique in your list";
        }

        private void textBox2_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Write a short description associated with your task";
        }
        
        private void dateTimePicker1_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "The task will take place on a specific day. After that it will expire.";
        }

        private void listBox1_MouseEnter(object sender, EventArgs e)
        {
            HelpLabel.Text = "Choose the task you want to see / modify / remove.";
        }
    }
}
