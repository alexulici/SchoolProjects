﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agenda
{
    public class IDObject
    {
        private IComparable _id;

        public IDObject(IComparable id) { this._id = id; }
        public IComparable ID
        {
            get { return _id; }
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }
    }
}
