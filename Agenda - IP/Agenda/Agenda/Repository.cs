﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Agenda
{
    class Repository<T> where T : IDObject, IComparable
    {
        private List<T> list = new List<T>();

        public void AddElement(T e)
        {
            list.Add(e);
        }
        public void RemoveElement(T e)
        {
            list.Remove(e);
        }
        public T GetElementAt(int position)
        {
            return list[position];
        }
        public int ListLength()
        {
            return list.Count;
        }
        public T FindElementbyAttribute(object attribute)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].CompareTo(attribute) > 0)
                    return list[i];
            }
            return null;
        }
    }
}
